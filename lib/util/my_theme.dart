import 'package:flutter/material.dart';
import 'package:zhibao_flutter/util/float/fram_size.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:get/get.dart';

class MyTheme {
  static Color textColor = const Color(0xff35A252);
  static Color recentlyActiveColor = const Color(0xffF6B548);
  static Color offlineColor = Colors.grey;

  static Rx<Color> mainColor = const Color(0xffF37135).obs;

  static List<BoxShadow> mainShadow = [
    BoxShadow(
      color: const Color(0xff000000).withOpacity(0.35),
      offset: Offset(0, 3.px),
      blurRadius: 5.px,
      spreadRadius: -3,
    )
  ];

  static List<Color> mainGradientColor() => <Color>[
        mainColor.value,
        recentlyActiveColor,
      ];

  static mainTextColor() {
    return const Color(0xffbdb2e5);
  }

  static List<BoxShadow> shadowGrey = [
    BoxShadow(
      color: const Color(0xff000000).withOpacity(0.08),
      offset: Offset(0, 2.px),
      blurRadius: 4.px,
      spreadRadius: 0,
    )
  ];

  // static Color mainColor() => Color(0xffAA0000);

  /// Third plugin use it.
  static ThemeData themeForPlugin() {
    return theme(
      /// 一定要dark，否则会导致黑色背景下的文本颜色为黑色
      /// 关联到设置页面和微信样式的拍照
      colorScheme: ColorScheme.dark(
        secondary: MyTheme.themeColor(),
      ),
    );
  }

  static ThemeData theme({ColorScheme? colorScheme}) {
    const bgColor = Color(0xffFAF9F8);
    return ThemeData(
      primaryColor: MyTheme.themeColor(),
      primarySwatch: MyTheme.themeColor(),
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      toggleButtonsTheme: const ToggleButtonsThemeData(
        splashColor: Colors.transparent,
      ),
      progressIndicatorTheme: ProgressIndicatorThemeData(
        color: MyTheme.mainColor.value,
        refreshBackgroundColor: MyTheme.mainColor.value.withOpacity(0.5),
        circularTrackColor: MyTheme.mainColor.value.withOpacity(0.5),
        linearTrackColor: MyTheme.mainColor.value.withOpacity(0.5),
      ),
      inputDecorationTheme: InputDecorationTheme(
        /// According login designer.
        labelStyle: TextStyle(color: const Color(0xff182046), fontSize: 16.px),

        /// According login designer.
        hintStyle: TextStyle(
          fontWeight: MyFontWeight.medium,
          color: const Color(0xffB3B3B3),
          fontSize: 16.px,
        ),
      ),
      textButtonTheme: TextButtonThemeData(
        style: ButtonStyle(
          /// TextButton的文本颜色。
          foregroundColor: MaterialStateProperty.all(MyTheme.mainColor.value),
          textStyle: MaterialStateProperty.all(
            TextStyle(
              color: MyTheme.mainColor.value,
              fontWeight: MyFontWeight.semBold,
            ),
          ),
        ),
      ),
      colorScheme: colorScheme,
      // Tower 任务: 【UI问题】光标颜色统一更改（详见图） ( https://tower.im/teams/995083/todos/770 )
      textSelectionTheme: TextSelectionThemeData(
        cursorColor: themeColor(),
        selectionColor: themeColor()[200],
        selectionHandleColor: themeColor(),
      ),
      tabBarTheme: TabBarTheme(
        labelColor: MyTheme.mainColor.value,
        indicatorColor: MyTheme.mainColor.value,
      ),
      appBarTheme: AppBarTheme(
        backgroundColor: bgColor,
      ),
      scaffoldBackgroundColor: bgColor,
      // fontFamily: 'PingFang-Medium',
    );
  }

  static MaterialColor themeColor([bool isTheme = false]) {
    final value = mainColor.value.value;
    MaterialColor themeColor = MaterialColor(
      value,
      <int, Color>{
        50: Color(value),
        100: Color(value),
        200: Color(value).withOpacity(0.2),
        300: Color(value).withOpacity(0.3),
        400: Color(value).withOpacity(0.4),
        500: Color(value).withOpacity(0.5),
        600: Color(value),
        700: Color(value),
        800: Color(value),
        900: Color(value),
      },
    );
    return themeColor;
  }

  static TextStyle labelOfUserBaseInfoStyle =
      TextStyle(color: Colors.white, fontSize: 11.px);

  /// Color use for reply message widget.
  static Color replyLineColor = Colors.blue.withOpacity(0.5);

  static TextStyle textRegular({Color? color, double? fontSize}) {
    return TextStyle(
      fontWeight: FontWeight.w500,
      fontFamily: 'PingFang-Medium',
      fontSize: fontSize,
      color: color,
    );
  }

  static TextStyle textMedium({Color? color, double? fontSize}) {
    return TextStyle(
      fontWeight: FontWeight.w500,
      fontFamily: 'PingFang-Bold',
      fontSize: fontSize,
      color: color,
    );
  }

  static TextStyle oneDayTextStyle =
      const TextStyle(color: Color(0xffA43D42), fontSize: 16);

  static TextStyle textBold({Color? color, double? fontSize}) {
    return TextStyle(
      fontWeight: FontWeight.w600,
      fontFamily: "PingFang-Bold",
      fontSize: fontSize,
      color: color,
    );
  }

  static Color unselectedItemColor() {
    return const Color(0xffB9B9B9);
  }

  static List<Color> gradientDisableColors = [
    const Color(0xffFFC889),
    const Color(0xffFFC189),
    const Color(0xffFFA47A),
  ];

  static List<Color> gradientColors = [
    const Color(0xffFF8800),
    const Color(0xffFF7F00),
    const Color(0xffFF5000),
  ];

  static LinearGradient btGradient(List<Color> colors) {
    return LinearGradient(
      begin: Alignment.centerLeft,
      end: Alignment.centerRight,
      colors: colors,
    );
  }

  static LinearGradient mainButtonGradient = LinearGradient(
    begin: Alignment.centerLeft,
    end: Alignment.centerRight,
    colors: gradientColors,
  );

  static LinearGradient mainButtonGradientReverse = LinearGradient(
    begin: Alignment.centerLeft,
    end: Alignment.centerRight,
    colors: gradientColors.reversed.toList(),
  );

  static mainHtmlStyle() {
    Style _style = Style(
      backgroundColor: Colors.white,
      fontSize: FontSize(16),
      padding: HtmlPaddings.symmetric(vertical: 0),
      margin: Margins.all(0),
      letterSpacing: 1.5, //行宽
    );
    return {'body': _style, 'p': _style};
  }

  static mainBorder([Color? color]) {
    return Border(bottom: mainBorderSide(color));
  }

  static mainBorderTop() {
    return Border(top: mainBorderSide());
  }

  static TextStyle wekakaStyle =
      const TextStyle(color: Color(0xff404040), fontSize: 15);

  static TextStyle appBarTitleStyle(Color? mainColorValue) {
    return TextStyle(
        fontSize: (18),
        fontWeight: FontWeight.w600,
        color: mainColorValue ?? const Color(0xff181716));
  }

  static mainBorderSide([Color? color]) {
    return BorderSide(color: color ?? const Color(0xffEFEFEF), width: 1);
  }

  static mainColors() {
    return [const Color(0xffFF2E18), const Color(0xffFF544C)];
  }
}

class MyFontWeight {
  static const FontWeight medium = FontWeight.w400;
  static const FontWeight semBold = FontWeight.w600;
  static const FontWeight bold = FontWeight.w600;
}
