import 'dart:math' as math;

class MockData {
  static List mock = [
    {
      "bgImg":
          "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Finews.gtimg.com%2Fnewsapp_bt%2F0%2F13573017746%2F1000&refer=http%3A%2F%2Finews.gtimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1669826282&t=c73ec40b3a700829aab77ebe8e9d266a",
      "pic": "2",
      "isFaceAuthentication": true,
      "isTalentShow": true,
      "isOnline": true,
      "name": "薇薇安",
      "age": "21",
      "job": "学生",
      "emotion": '单身',
      "height": "1.6km",
    },
    {
      "bgImg":
          "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fwww.ssfiction.com%2Fwp-content%2Fuploads%2F2020%2F08%2F20200805_5f2adfacdfc1e.jpg&refer=http%3A%2F%2Fwww.ssfiction.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1669826312&t=ab0172c9746c4b8d5ece145215fdb772",
      "pic": "0",
      "isFaceAuthentication": true,
      "isTalentShow": false,
      "isOnline": true,
      "name": "小甜甜",
      "age": "23",
      "job": "学生",
      "emotion": '单身',
      "height": "1.6km",
    },
    {
      "bgImg":
          "https://img2.baidu.com/it/u=3085622294,1635379708&fm=253&fmt=auto&app=138&f=JPEG?w=400&h=400",
      "pic": "2",
      "isFaceAuthentication": false,
      "isTalentShow": true,
      "isOnline": true,
      "name": "王晓晓",
      "age": "21",
      "job": "学生",
      "emotion": '单身',
      "height": "1.6km",
    },
    {
      "bgImg":
          "https://img2.baidu.com/it/u=1534125675,1977894405&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500",
      "pic": "0",
      "isFaceAuthentication": false,
      "isTalentShow": false,
      "isOnline": false,
      "name": "花花",
      "age": "21",
      "job": "学生",
      "emotion": '单身',
      "height": "1.6km",
    },
    {
      "bgImg":
          "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Finews.gtimg.com%2Fnewsapp_bt%2F0%2F13573017746%2F1000&refer=http%3A%2F%2Finews.gtimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1669826282&t=c73ec40b3a700829aab77ebe8e9d266a",
      "pic": "2",
      "isFaceAuthentication": true,
      "isTalentShow": true,
      "isOnline": true,
      "name": "薇薇安",
      "age": "21",
      "job": "学生",
      "emotion": '单身',
      "height": "1.6km",
    },
    {
      "bgImg":
          "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fwww.ssfiction.com%2Fwp-content%2Fuploads%2F2020%2F08%2F20200805_5f2adfacdfc1e.jpg&refer=http%3A%2F%2Fwww.ssfiction.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1669826312&t=ab0172c9746c4b8d5ece145215fdb772",
      "pic": "0",
      "isFaceAuthentication": true,
      "isTalentShow": false,
      "isOnline": true,
      "name": "小甜甜",
      "age": "23",
      "job": "学生",
      "emotion": '单身',
      "height": "1.6km",
    },
    {
      "bgImg":
          "https://img2.baidu.com/it/u=3085622294,1635379708&fm=253&fmt=auto&app=138&f=JPEG?w=400&h=400",
      "pic": "2",
      "isFaceAuthentication": false,
      "isTalentShow": true,
      "isOnline": true,
      "name": "王晓晓",
      "age": "21",
      "job": "学生",
      "emotion": '单身',
      "height": "1.6km",
    },
    {
      "bgImg":
          "https://img2.baidu.com/it/u=1534125675,1977894405&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500",
      "pic": "0",
      "isFaceAuthentication": false,
      "isTalentShow": false,
      "isOnline": false,
      "name": "花花",
      "age": "21",
      "job": "学生",
      "emotion": '单身',
      "height": "1.6km",
    },
    {
      "bgImg":
          "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Finews.gtimg.com%2Fnewsapp_bt%2F0%2F13573017746%2F1000&refer=http%3A%2F%2Finews.gtimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1669826282&t=c73ec40b3a700829aab77ebe8e9d266a",
      "pic": "2",
      "isFaceAuthentication": true,
      "isTalentShow": true,
      "isOnline": true,
      "name": "薇薇安",
      "age": "21",
      "job": "学生",
      "emotion": '单身',
      "height": "1.6km",
    },
    {
      "bgImg":
          "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fwww.ssfiction.com%2Fwp-content%2Fuploads%2F2020%2F08%2F20200805_5f2adfacdfc1e.jpg&refer=http%3A%2F%2Fwww.ssfiction.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1669826312&t=ab0172c9746c4b8d5ece145215fdb772",
      "pic": "0",
      "isFaceAuthentication": true,
      "isTalentShow": false,
      "isOnline": true,
      "name": "小甜甜",
      "age": "23",
      "job": "学生",
      "emotion": '单身',
      "height": "1.6km",
    },
    {
      "bgImg":
          "https://img2.baidu.com/it/u=3085622294,1635379708&fm=253&fmt=auto&app=138&f=JPEG?w=400&h=400",
      "pic": "2",
      "isFaceAuthentication": false,
      "isTalentShow": true,
      "isOnline": true,
      "name": "王晓晓",
      "age": "21",
      "job": "学生",
      "emotion": '单身',
      "height": "1.6km",
    },
    {
      "bgImg":
          "https://img2.baidu.com/it/u=1534125675,1977894405&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500",
      "pic": "0",
      "isFaceAuthentication": false,
      "isTalentShow": false,
      "isOnline": false,
      "name": "花花",
      "age": "21",
      "job": "学生",
      "emotion": '单身',
      "height": "1.6km",
    },
    {
      "bgImg":
          "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Finews.gtimg.com%2Fnewsapp_bt%2F0%2F13573017746%2F1000&refer=http%3A%2F%2Finews.gtimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1669826282&t=c73ec40b3a700829aab77ebe8e9d266a",
      "pic": "2",
      "isFaceAuthentication": true,
      "isTalentShow": true,
      "isOnline": true,
      "name": "薇薇安",
      "age": "21",
      "job": "学生",
      "emotion": '单身',
      "height": "1.6km",
    },
    {
      "bgImg":
          "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fwww.ssfiction.com%2Fwp-content%2Fuploads%2F2020%2F08%2F20200805_5f2adfacdfc1e.jpg&refer=http%3A%2F%2Fwww.ssfiction.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1669826312&t=ab0172c9746c4b8d5ece145215fdb772",
      "pic": "0",
      "isFaceAuthentication": true,
      "isTalentShow": false,
      "isOnline": true,
      "name": "小甜甜",
      "age": "23",
      "job": "学生",
      "emotion": '单身',
      "height": "1.6km",
    },
    {
      "bgImg":
          "https://img2.baidu.com/it/u=3085622294,1635379708&fm=253&fmt=auto&app=138&f=JPEG?w=400&h=400",
      "pic": "2",
      "isFaceAuthentication": false,
      "isTalentShow": true,
      "isOnline": true,
      "name": "王晓晓",
      "age": "21",
      "job": "学生",
      "emotion": '单身',
      "height": "1.6km",
    },
    {
      "bgImg":
          "https://img2.baidu.com/it/u=1534125675,1977894405&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500",
      "pic": "0",
      "isFaceAuthentication": false,
      "isTalentShow": false,
      "isOnline": false,
      "name": "花花",
      "age": "21",
      "job": "学生",
      "emotion": '单身',
      "height": "1.6km",
    },
  ];

  static String randomPic() {
    int randomInt = math.Random().nextInt(mock.length - 1);
    return mock[randomInt]['bgImg'];
  }
}
