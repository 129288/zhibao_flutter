import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:zhibao_flutter/widget/app/logo_widget.dart';
import 'package:zhibao_flutter/zone.dart';

class Q1SmartRefreshFooter extends StatelessWidget {
  const Q1SmartRefreshFooter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomFooter(
      builder: (BuildContext context, LoadStatus? mode) {
        Widget body;
        if (mode == LoadStatus.idle) {
          body = Text("PullUpToLoad".tr);
        } else if (mode == LoadStatus.loading) {
          body = const CupertinoActivityIndicator();
        } else if (mode == LoadStatus.failed) {
          body = Text("LoadFailed".tr);
        } else if (mode == LoadStatus.canLoading) {
          body = Text("ReleaseToLoad".tr);
        } else {
          body = Text("NoMoreData".tr);
        }
        return SizedBox(
          height: 60.0,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const LogoWidget(),
              Container(
                width: 100,
                alignment: Alignment.center,
                child: body,
              ),
            ],
          ),
        );
      },
    );
  }
}

class Q1SmartRefreshHeader extends StatelessWidget {
  const Q1SmartRefreshHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomHeader(
      builder: (context, mode) {
        Widget body;
        if (mode == RefreshStatus.idle) {
          body = Text("PullDownToRefresh".tr);
        } else if (mode == RefreshStatus.refreshing) {
          body = const CupertinoActivityIndicator();
        } else if (mode == RefreshStatus.canRefresh) {
          body = Text("ReleaseToRefresh".tr);
        } else if (mode == RefreshStatus.completed) {
          body = Text("RefreshCompleted".tr);
        } else {
          body = Container();
        }
        return SizedBox(
          height: 60.0,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const LogoWidget(),
              Container(
                width: 100,
                alignment: Alignment.center,
                padding: EdgeInsets.symmetric(horizontal: 6.px),
                child: body,
              ),
            ],
          ),
        );
      },
    );
  }
}

class MySmartRefresh extends StatelessWidget {
  final RefreshController controller;
  final VoidCallback? onRefresh;
  final VoidCallback? onLoading;
  final Widget? child;

  const MySmartRefresh({
    Key? key,
    required this.controller,
    this.onRefresh,
    this.onLoading,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SmartRefresher(
      controller: controller,
      header: const Q1SmartRefreshHeader(),
      footer: const Q1SmartRefreshFooter(),
      enablePullUp: onLoading != null,
      enablePullDown: onRefresh != null,
      onRefresh: onRefresh,
      onLoading: onLoading,
      child: child,
    );
  }
}
