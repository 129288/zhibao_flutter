import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:zhibao_flutter/util/func/log.dart';
import 'package:zhibao_flutter/util/func/tips_util.dart';
import 'package:zhibao_flutter/util/navibe/q1_android_util.dart';
import 'package:zhibao_flutter/util/ui/my_toast.dart';
import 'package:permission_handler/permission_handler.dart';

typedef FutureVoidCallback = Future Function();

class PermissionUtil {
  static Future picturePermission(FutureVoidCallback then) async {
    if (Platform.isAndroid) {
      List<Permission> permissions = [
        Permission.camera,
        Permission.storage,
      ];

      for (Permission pItem in permissions) {
        if (!(await pItem.isGranted)) {
          PermissionStatus status = await pItem.request();
          LogUtil.d("status::$status");
        }
      }

      // if (await Q1AndroidUtil().onlyCheckCameraDisable()) {
      //   myFailToast("缺少相机权限");
      //   return;
      // }
      // if (await await Q1AndroidUtil().onlyCheckStoreDisable()) {
      //   myFailToast("缺少相册权限");
      //   return;
      // }

      /// Check has permission not granted.
      if (!(await Permission.camera.isGranted)
          // ||
          // !(await Permission.storage.isGranted)
          ) {
        return;
      }

      return then();
    } else {
      List<Permission> permissions = [
        Permission.camera,
        Permission.storage,
        Permission.photos,
      ];

      for (Permission pItem in permissions) {
        if (!(await pItem.isGranted)) {
          PermissionStatus status = await pItem.request();
          LogUtil.d("status::pItem[${pItem.toString()}]::$status");
        }
      }

      /// Check has permission not granted.
      if (!(await Permission.camera.isGranted) &&
          !(await Permission.storage.isGranted) &&
          !(await Permission.photos.isGranted)) {
        return;
      }

      /// iOS暂时不校验，直接显示，插件会申请授权权限
      return then();
    }
  }

  static Future<bool> audioRecordPermission() async {
    await Permission.audio.request();
    await Permission.storage.request();
    await Permission.speech.request();
    await Permission.microphone.request();

    if (Platform.isAndroid) {
      return await Permission.speech.isGranted;
    } else {
      return await Permission.microphone.isGranted;
    }
  }
}
