import 'package:zhibao_flutter/util/ui/my_toast.dart';

class TipsUtil {
  /// network connection failed
  static String netError = "网络异常";

  /// 无权限且没有申请资格了
  static void notPermission() {
    myFailToast("缺少系统权限");
  }

  static Future<void> noData() async {
    myToast("no data.");
  }
}
