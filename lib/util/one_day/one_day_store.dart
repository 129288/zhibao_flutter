import 'package:zhibao_flutter/util/data/q1_data.dart';
import 'package:zhibao_flutter/util/func/sp_util.dart';

class OneDayStore {
  static final String oneDayStoreIsNotShowTip =
      "${Q1Data.id}oneDayStoreIsNotShowTip";

  static void setNotShowTip() {
    SpUtil().setBool(oneDayStoreIsNotShowTip, true);
  }

  static void setShowTip() {
    SpUtil().setBool(oneDayStoreIsNotShowTip, false);
  }

  static bool get isNotShowTip {
    return SpUtil().get(oneDayStoreIsNotShowTip) ?? false;
  }
}
