import 'package:event_bus/event_bus.dart';

EventBus eventBusOfNumBer = EventBus();

class EventBusOfNumBerModel {
  final String? value;

  /// 0 : input;
  /// 1 : delete;
  final int type;
  final int index;

  EventBusOfNumBerModel(this.value, this.type, this.index);
}

class RefreshHomePriceModel {}
