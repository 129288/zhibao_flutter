import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:zhibao_flutter/zone.dart';

/// 0到9
List<TextInputFormatter> numFormatter = [
  FilteringTextInputFormatter(RegExp(r'[0-9]'), allow: true),
];

/// 0到9最多11位
List<TextInputFormatter> numAndPhoneFormatter = [
  FilteringTextInputFormatter(RegExp(r'[0-9]'), allow: true),
  LengthLimitingTextInputFormatter(11)
];

/// 0到9最多6位
List<TextInputFormatter> textFormatterNumSmsCode = [
  FilteringTextInputFormatter(RegExp(r'[0-9]'), allow: true),
  LengthLimitingTextInputFormatter(6)
];

/// 0到9 和 空
List<TextInputFormatter> numFormatterAndNull = [
  FilteringTextInputFormatter(RegExp(r'[0-9 ]'), allow: true),
];

/// 0到9和点
List<TextInputFormatter> numAndDotFormatter = [
  FilteringTextInputFormatter(RegExp(r'[0-9.]'), allow: true),
];

List<TextInputFormatter> testFormatter = [
  FilteringTextInputFormatter(RegExp(r'[0-9¬]'), allow: true),
];

/// 邮箱
List<TextInputFormatter> emailFormatter = [
  FilteringTextInputFormatter(
      RegExp(
          r'[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]'),
      allow: true),
];

/// 价格
List<TextInputFormatter> priceFormatter = [
  FilteringTextInputFormatter(RegExp(r'^\d*\.{0,2}\d{0,2}'), allow: true),
];

//限制20长度，限制长度
List<TextInputFormatter> max20Formatter = [
  LengthLimitingTextInputFormatter(20)
];

/// 限制指定长度输入内容。
List<TextInputFormatter> maxLengthFormatter(int maxLength) {
  return [LengthLimitingTextInputFormatter(maxLength)];
}

///不允许中文
List<TextInputFormatter> notWordFormatter = [
  FilteringTextInputFormatter(RegExp(r"[^\x00-\xff]"), allow: false),
];

/// 分段显示手机号
///
/// 例子：
/// 手机号最高输入10位，前3位，中间3位，后4位之间需要分隔开
TextInputFormatter phoneInputFormatter() {
  return TextInputFormatter.withFunction((oldValue, newValue) {
    String text = newValue.text;
// Get the text to the left of the cursor
    final positionStr = (text.substring(0, newValue.selection.baseOffset))
        .replaceAll(RegExp(r"\s+\b|\b\s"), "");
// Calculate the cursor position after formatting
    int length = positionStr.length;
    var position = 0;
    if (length <= 3) {
      position = length;
    } else if (length <= 6) {
// Because a space is added to the previous string
      position = length + 1;
    } else if (length <= 11) {
// Because there are two spaces in the previous string
      position = length + 2;
    } else {
// The number itself is 11 Digit number , Because there are two more spaces , So it's 13
      position = 13;
    }
// Format the whole input text here
    text = text.replaceAll(RegExp(r"\s+\b|\b\s"), "");
    var string = "";
    for (int i = 0; i < text.length; i++) {
// Here I 4 position , With the first 8 position , Let's fill in... With spaces
      if (i == 3 || i == 6) {
        if (text[i] != " ") {
          string = "$string ";
        }
      }
      string += text[i];
    }
    return TextEditingValue(
      text: string,
      selection: TextSelection.fromPosition(
          TextPosition(offset: position, affinity: TextAffinity.upstream)),
    );
  });
}

String end4Phone(String? s) {
  if ((s?.length ?? 0) == 11) {
    return s!.substring(s.length - 4, s.length);
  }
  return s ?? '';
}

String hidePhoneUserNewMark(String? s) {
  return hidePhone(s, "*");
}

// [x] 内容太多如何去显示推荐人，换行还是隐藏中间【后端：隐藏中间】
String hideCenter(String? s, {int maxLength = 11}) {
  if (s == null) {
    return '';
  }
  return s.length > 11 ? s.replaceRange(3, s.length - 6, "***") : s;
}

String hidePhone(String? s, [String hideMark = "."]) {
  if ((s?.length ?? 0) == 11) {
    return s!.replaceRange(3, 7, hideMark * 4);
  }
  return s ?? '';
}

String formatNumberInDifLan(num number) {
  if (AppConfig.locale.languageCode == 'zh') {
    return '${(number / 10000).toStringAsFixed(2)}万';
  } else {
    return '${(number / 1000).toStringAsFixed(2)}k';
  }
}
