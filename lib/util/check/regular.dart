/// 手机号正则表达式->true匹配
bool isMobilePhoneNumber(String value) {
  RegExp mobile = RegExp(r"(0|86|17951)?(1[0-9][0-9])[0-9]{8}");

  return mobile.hasMatch(value);
}

///验证网页URl
bool isUrl(String value) {
  RegExp url = RegExp(r"^((https|http|ftp|rtsp|mms)?://)[^\s]+");

  return url.hasMatch(value);
}

///校验身份证
bool isIdCard(String value) {
  if (!strNoEmpty(value)) return false;
  RegExp identity = RegExp(r"\d{17}[\d|x]|\d{15}");

  return identity.hasMatch(value);
}

///正浮点数
bool isMoney(String value) {
  RegExp identity = RegExp(
      r"^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$");
  return identity.hasMatch(value);
}

///校验中文
bool isChinese(String value) {
  RegExp identity = RegExp(r"[\u4e00-\u9fa5]");

  return identity.hasMatch(value);
}

///校验猫表名称
bool isComoLlegarName(String value) {
  RegExp identity = RegExp(r"[\u4e00-\u9fa5_a-zA-Z]");

  return identity.hasMatch(value);
}

/// 字符串不为空
bool strNoEmpty(String? value) {
  if (value == null) return false;
  if (value == 'null') return false;

  return value.trim().isNotEmpty;
}

/// 字符串不为空
bool mapNoEmpty(Map? value) {
  if (value == null) return false;
  return value.isNotEmpty;
}

///判断List是否为空
bool listNoEmpty(List? list) {
  if (list == null) return false;

  if (list.isEmpty) return false;

  return true;
}

///判断验证码是否正确
bool isValidateCaptcha(String value) {
  RegExp mobile = RegExp(r"\d{6}$");
  return mobile.hasMatch(value);
}

///验证金额价格的正则表达式
bool isPrice(String value) {
  RegExp mobile = RegExp(r"(?!^0*(\.0{1,2})?$)^\d{1,13}(\.\d{1,2})?$");
  return mobile.hasMatch(value);
}

/// 判断是否网络
bool isNetWorkImg(String img) {
  return img.startsWith('http') || img.startsWith('https');
}

/// 判断是否资源图片
bool isAssetsImg(String img) {
  return img.startsWith('asset') || img.startsWith('assets');
}

/// 邮箱正则
const String regexEmail =
    "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*\$";

/// 检查是否是邮箱格式
bool isEmail(String input) {
  if (input.isEmpty) return false;
  return RegExp(regexEmail).hasMatch(input);
}
