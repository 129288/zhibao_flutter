import 'package:flutter/cupertino.dart';
import 'package:zhibao_flutter/api/user/user_view_model.dart';
import 'package:zhibao_flutter/widget/dialog/confirm_dialog.dart';
import 'package:get/get.dart';

class TipUtil {
  static void confirmPopTip(BuildContext context) {
    confirmDialog(context,
        text: "资料未完善，确定要退出登录吗？",
        cancelText: "退出",
        okText: "再考虑一下", onCancel: () {
      userViewModel.loginOut();
    });
  }
}
