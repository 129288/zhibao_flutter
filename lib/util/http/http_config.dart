import 'package:curl_logger_dio_interceptor/curl_logger_dio_interceptor.dart';
import 'package:dio/dio.dart';


class HttpConfig {
  Dio ?dio;
  static HttpConfig ?_instance;
  static HttpConfig? getInstance() {
    _instance ??= HttpConfig();
    return _instance;
  }

  static BaseOptions options = BaseOptions(
    connectTimeout: Duration(milliseconds: 50000),
    receiveTimeout: Duration(milliseconds: 30000),
    contentType: Headers.jsonContentType,
  );

  static Dio getDio(){
    var dio = Dio(HttpConfig.options);
    dio.interceptors.add(CurlLoggerDioInterceptor());
    return dio;
  }
}
