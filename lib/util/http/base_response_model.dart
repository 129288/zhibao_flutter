/// 等后端指定规范再统一封装
class ResponseModel {
  int? code;
  String? message;
  String? apiName;
  String? serverMessage;
  dynamic data;
  bool isHasNextPage = true;

  ResponseModel({this.code = 0, this.message = '请求成功', this.data});

  /*
  * 请求返回错误
  *
  * */
  factory ResponseModel.fromError(msg, code, [name = '']) {
    ResponseModel rep;

    rep = ResponseModel()
      ..code = code
      ..message = msg
      ..apiName = name ?? '';

    return rep;
  }

  /*
  * 请求成功
  *
  * */
  factory ResponseModel.fromSuccess(data, [bool? isHasNextPage]) {
    final rspModel = ResponseModel();
    if (isHasNextPage != null) {
      rspModel.isHasNextPage = isHasNextPage;
    }
    return rspModel..data = data;
  }

  /*
  * 请求单个数组成功
  *
  * */
  factory ResponseModel.fromSuccessList(data) =>
      ResponseModel()..data = data.data;

  /*
  * 参数校验
  *
  * */
  factory ResponseModel.fromParamError(String msg) => ResponseModel()
    ..code = 1
    ..message = '$msg';
}
