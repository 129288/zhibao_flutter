import 'package:zhibao_flutter/util/config/app_config.dart';

enum EnvironmentType { pro, devApp, devWeb }

class Environment {
  // 接口文档：
  // 接口文档地址（涉及到所有的业务流程的接口）：https://shimo.im/docs/Gx3KKJRcxhqqvcGk
  // 接口地址 http://${AppConfig.host}/
  // H5链接  http://${AppConfig.host}/#/
  // 注：H5链接直接复制

  /*
  * 生产环境
  *
  * */
  static String get pro => "http://${AppConfig.host}/";

  /*
  * 开发环境
  *
  * */
  static String devApp = pro;
  static String proApp = pro;
  static String devWeb = pro;

  /*
  * 当前环境
  *
  * @param pro production
  * @param dev development
  *
  * */
  static String current = pro;

  static void setCurrent(EnvironmentType environment) {
    if (environment == EnvironmentType.devApp) {
      current = devApp;
    } else if (environment == EnvironmentType.pro) {
      current = proApp;
    } else if (environment == EnvironmentType.devWeb) {
      current = devWeb;
    } else {
      current = pro;
    }
  }
}
