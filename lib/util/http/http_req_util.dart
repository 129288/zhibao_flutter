import 'dart:convert';
import 'dart:developer';

import 'package:zhibao_flutter/util/config/app_config.dart';
import 'package:zhibao_flutter/util/data/q1_data.dart';
import 'package:zhibao_flutter/util/func/log.dart';

class HttpReqUtil {
  /// 模拟设备id
  static String getSureDeviceId() {
    if (AppConfig.mockWeb) {
      return "16720260461562018304958";
    }
    return Q1DataRequest.deviceId;
  }

  /// 频道
  static String getCurrentAccessChannel() {
    return "flutter";
    // if(AppConfig.mockWeb){
    //   return "web";
    // }
    // return "app";
  }

  /// 平台
  static String countPlatformCode() {
    return "2";
  }

  static String apiUseVersion() {
    String versionStr = Q1DataRequest.versionStr;
    final int firstVersion = int.parse(versionStr.split('.')[0]);
    final result =

    /// Lin: +2不需要动，就工程设置1.5.3，默认传3.5.3就行了。不加不减的话你检查一下所有加的地方都要统一，
    /// 以前我们是图片上传和通用接口两个地方些了
        "${firstVersion + 2}.${versionStr.split('.')[1]}.${versionStr.split(
        '.')[2]}";
    // "${firstVersion}.${versionStr.split('.')[1]}.${versionStr.split('.')[2]}";

    LogUtil.d("HttpReqUtil::apiUseVersion::$result");
    return result;
  }

  /**
   * 网页版固定的参数
   */
  static Map<String, dynamic> get webClientFixedParams {
    return {
      "version": AppConfig.mockWeb ? "3.0.0" : apiUseVersion(),
      "app_name": AppConfig.appName
    };
  }

  /**
   * 生成请求用的特性的参数
   */
  static Map<String, dynamic> createRequestFeatureParams() {
    return {
      ...webClientFixedParams,
      "device_id": Q1DataRequest.deviceId,
      "channel": getCurrentAccessChannel(),
      "platform": countPlatformCode()
    };
  }

  static Map<String, dynamic> assignRequestParams(
      Map<String, dynamic> originParams) {
    // LogUtil.d('originParams', json.encode(originParams));
    // originParams = createWithoutUndefinedOfRecord(originParams);

    final Map<String, dynamic> targetParams = {
      ...createRequestFeatureParams(),
      ...originParams
    };
    log("assignRequestParams::targetParams::${json.encode(targetParams)}");
    //console.debug(createSecretParams(targetParams));
    // return HTTPSecret.requestSecretInterceptor(targetParams);
    return originParams;
  }
}
