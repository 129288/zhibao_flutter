import 'package:flutter/foundation.dart';
import 'package:flutter_cjadsdk_plugin/flutter_cjadsdk_plugin.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:zhibao_flutter/util/data/actions.dart';
import 'package:zhibao_flutter/util/data/notice.dart';
import 'package:zhibao_flutter/zone.dart';

class PageUtil {
  static toDefWebViewPage() {
    /// 首页暂时不跳转链接
    ///
    /// 【2024 Mar 2】
    /// 跳转到指定链接。
    ///
    // if (!kIsWeb) FlutterCjadsdkPlugin.getInterstitialAdMethod();
  }

  static Future toRechargeBalance() async {
    // if (!kIsWeb) FlutterCjadsdkPlugin.getInterstitialAdMethod();

    /// 【2024 Mar 3】
    /// Only `短剧` can jump to WebView.
    // Get.toNamed(RouteConfig.rechargePage, arguments: {});
    /// 【Mar 27 2024】
    // https://myxq8.kuaizhan.com/64/77/p926592549ee319?mid=45658&a=cc984bcc8eefb48c&uid=    充话费新方案，直接了解跳转web即可，uid填我们的用户id，充话费直接跳转页面
    final fullUrl =
        // [Mar 28 2024]
        // ToDo 我不知道是拼接手机号/邮箱还是用户
        // "https://myxq8.kuaizhan.com/64/77/p926592549ee319?mid=45658&a=cc984bcc8eefb48c&uid=${Q1Data.id}";
        "https://myxq8.kuaizhan.com/64/77/p926592549ee319?mid=45658&a=cc984bcc8eefb48c&uid=${Q1Data.phone}";
    if (kIsWeb) {
      openNewLinkWhenWeb(fullUrl);
    } else {
      Get.toNamed(RouteConfig.webViewPage, arguments: {
        "linkUrl": fullUrl,
      });
    }
  }

  static Future openNewLinkWhenWeb(String fullUrl) async {
    try {
      Uri uri = Uri.parse(fullUrl);
      if (await canLaunchUrl(uri)) {
        launchUrl(uri, mode: LaunchMode.platformDefault);
      } else {
        myToast("无法打开链接$fullUrl");
      }
    } catch (e) {
      LogUtil.d("toShortPlayDetail() face a error ${e.toString()}");
      myToast('出现错误');
    }
  }

  static Future toShortPlayDetail() async {
    // if (!kIsWeb) FlutterCjadsdkPlugin.getInterstitialAdMethod();
    final fullUrl =
        'https://playlet.mykshow.com/?appId=bac54b310f5e5c95f5&userId=${Q1Data.phone}';
    if (kIsWeb) {
      openNewLinkWhenWeb(fullUrl);
    } else {
      /// 【2024 Mar 3】
      /// "增加短剧 url 跳转webview功能， 放到后台可配置
      // https://playlet.mykshow.com/?appId=bac54b310f5e5c95f5&userId=我们平台的用户手机号/邮箱"
      Get.toNamed(RouteConfig.webViewPage, arguments: {
        "linkUrl": fullUrl,
      });
    }
  }

  static void toGame() {
    if (kIsWeb) {
      openNewLinkWhenWeb(AppConfig.gameLink);
    } else {
      Notice.send(Q1Actions.toTabBarIndex(), 2);
    }
  }
  /// 【Mar 28 2024】
  /// 下单【五大板块】
  static void toOrder() {
    // if (!kIsWeb) FlutterCjadsdkPlugin.getInterstitialAdMethod();
    final fullUrl =
        // [Mar 28 2024]
        // ToDo 我不知道是拼接手机号/邮箱还是用户
        //     'https://myxq8.kuaizhan.com/64/77/p926592549ee319?mid=1&a=b47c7f3bc4db0136&uid=${Q1Data.id}';
        'https://myxq8.kuaizhan.com/64/77/p926592549ee319?mid=1&a=b47c7f3bc4db0136&uid=${Q1Data.phone}';
    if (kIsWeb) {
      openNewLinkWhenWeb(fullUrl);
    } else {
      /// 【2024 Mar 3】
      /// "增加短剧 url 跳转webview功能， 放到后台可配置
      // https://playlet.mykshow.com/?appId=bac54b310f5e5c95f5&userId=我们平台的用户手机号/邮箱"
      Get.toNamed(RouteConfig.webViewPage, arguments: {
        "linkUrl": fullUrl,
      });
    }
  }
}
