import 'package:zhibao_flutter/pages/game/game_binding.dart';
import 'package:zhibao_flutter/pages/home/home_binding.dart';
import 'package:zhibao_flutter/pages/lian/lian_binding.dart';
import 'package:zhibao_flutter/pages/login/forget_pay_pw/forget_pay_pw_binding.dart';
import 'package:zhibao_flutter/pages/login/forget_pay_pw/forget_pay_pw_view.dart';
import 'package:zhibao_flutter/pages/login/forget_pw/forget_pw_binding.dart';
import 'package:zhibao_flutter/pages/login/forget_pw/forget_pw_view.dart';
import 'package:zhibao_flutter/pages/login/login_binding.dart';
import 'package:zhibao_flutter/pages/login/login_view.dart';
import 'package:zhibao_flutter/pages/login/privacy/privacy_binding.dart';
import 'package:zhibao_flutter/pages/login/privacy/privacy_view.dart';
import 'package:zhibao_flutter/pages/login/register/register_binding.dart';
import 'package:zhibao_flutter/pages/login/register/register_view.dart';
import 'package:zhibao_flutter/pages/mine/bill/bill_binding.dart';
import 'package:zhibao_flutter/pages/mine/bill/bill_view.dart';
import 'package:zhibao_flutter/pages/mine/charge/charge_binding.dart';
import 'package:zhibao_flutter/pages/mine/charge/charge_view.dart';
import 'package:zhibao_flutter/pages/mine/info/modify_nickname_page.dart';
import 'package:zhibao_flutter/pages/mine/invite/invite_binding.dart';
import 'package:zhibao_flutter/pages/mine/invite/invite_view.dart';
import 'package:zhibao_flutter/pages/mine/language_set/language_set_binding.dart';
import 'package:zhibao_flutter/pages/mine/language_set/language_set_view.dart';
import 'package:zhibao_flutter/pages/mine/mine_binding.dart';
import 'package:zhibao_flutter/pages/mine/team/team_binding.dart';
import 'package:zhibao_flutter/pages/mine/team/team_view.dart';
import 'package:zhibao_flutter/pages/mine/transfer/transfer_binding.dart';
import 'package:zhibao_flutter/pages/mine/transfer/transfer_view.dart';
import 'package:zhibao_flutter/pages/mine/upgrade/upgrade_binding.dart';
import 'package:zhibao_flutter/pages/mine/upgrade/upgrade_view.dart';
import 'package:zhibao_flutter/pages/mine/upgrade_det/upgrade_det_binding.dart';
import 'package:zhibao_flutter/pages/mine/upgrade_det/upgrade_det_view.dart';
import 'package:zhibao_flutter/pages/mine/withdraw/withdraw_binding.dart';
import 'package:zhibao_flutter/pages/mine/withdraw/withdraw_view.dart';
import 'package:zhibao_flutter/pages/other/main/main_binding.dart';
import 'package:zhibao_flutter/pages/other/main/main_view.dart';
import 'package:zhibao_flutter/pages/other/web_view_page.dart';
import 'package:zhibao_flutter/test/live_log_page.dart';
import 'package:get/get.dart';

class RouteConfig {
  // Name list.
  static const String mainPage = "/MainPage";
  static const String loginPage = "/LoginPage";
  static const String liveLogPage = "/LiveLogPage";
  static const String webViewPage = "/WebViewPage";
  static const String registerPage = "/RegisterPage";
  static const String forgetPwPage = "/ForgetPwPage";
  static const String teamPage = "/TeamPage";
  static const String invitePage = "/InvitePage";
  static const String chargePage = "/ChargePage";
  static const String withdrawPage = "/WithdrawPage";
  static const String billPage = "/BillPage";
  static const String transferPage = "/TransferPage";
  static const String upgradePage = "/UpgradePage";
  static const String upgradeDetPage = "/UpgradeDetPage";
  static const String forgetPayPwPage = "/ForgetPayPwPage";
  static const String modifyNickNamePage = "/ModifyNickNamePage";
  static const String languageSetPage = "/LanguageSetPage";

  // Add the privacy page route constant
  static const String privacyPage = "/PrivacyPage";

  /// Alias Mapping Page
  static final List<GetPage> getPages = [
    GetPage(name: mainPage, page: () => MainPage(), bindings: [
      MainBinding(),
      HomeBinding(),
      GameBinding(),
      LianBinding(),
      MineBinding(),
    ]),
    GetPage(name: loginPage, page: () => LoginPage(), binding: LoginBinding()),
    GetPage(name: liveLogPage, page: () => LiveLogPage()),
    GetPage(name: webViewPage, page: () => WebViewPage()),
    GetPage(
        name: registerPage,
        page: () => RegisterPage(),
        binding: RegisterBinding()),
    GetPage(
        name: forgetPwPage,
        page: () {
          if (Get.arguments is bool) {
            return ForgetPwPage(Get.arguments);
          }
          return ForgetPwPage();
        },
        binding: ForgetPwBinding()),
    GetPage(name: teamPage, page: () => TeamPage(), binding: TeamBinding()),
    GetPage(
        name: invitePage, page: () => InvitePage(), binding: InviteBinding()),
    GetPage(
        name: chargePage, page: () => ChargePage(), binding: ChargeBinding()),
    GetPage(
        name: withdrawPage,
        page: () => WithdrawPage(),
        binding: WithdrawBinding()),
    GetPage(name: billPage, page: () => BillPage(), binding: BillBinding()),
    GetPage(
        name: transferPage,
        page: () => TransferPage(),
        binding: TransferBinding()),
    GetPage(
        name: upgradePage,
        page: () => UpgradePage(),
        binding: UpgradeBinding()),
    GetPage(
        name: upgradeDetPage,
        page: () => UpgradeDetPage(),
        binding: UpgradeDetBinding()),
    GetPage(
        name: forgetPayPwPage,
        page: () => ForgetPayPwPage(),
        binding: ForgetPayPwBinding()),
    GetPage(
      name: modifyNickNamePage,
      page: () => ModifyNickNamePage(),
    ),
    GetPage(
      name: languageSetPage,
      page: () => LanguageSetPage(),
      binding: LanguageSetBinding(),
    ),
    // Add the privacy page route
    GetPage(
      name: privacyPage,
      page: () => PrivacyPage(),
      binding: PrivacyBinding(),
    ),
  ];
}
