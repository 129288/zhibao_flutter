import 'package:zhibao_flutter/util/data/q1_data.dart';

class SpKey {
  static String loginResult = "loginResult";
  static String userInfo = "userInfo";
  static String firstToWebView = "firstToWebView";
  static String loginPhone = "loginPhone";
  static String cityStoreKey = "${Q1Data.id}_cityStoreKey";
  static String cityStoreOfDynamicKey = "${Q1Data.id}_cityStoreOfDynamicKey";
  static String realAddressKey = "realAddressKey";
  static String homeTabData = "homeTabData";
  static String dynamicDataList = "dynamicDataList";
  static String realPersonTipDialog = "realPersonTipDialog";
  static String homeGuideShow = "homeGuideShow";
  static String checkLongTimeNoChangeAvatarKey =
      "checkLongTimeNoChangeAvatarKey";

  static String countryCodeForPhone = "countryCodeForPhone";
}
