import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:zhibao_flutter/main.dart';
import 'package:zhibao_flutter/util/config/sp_key.dart';
import 'package:zhibao_flutter/util/data/q1_data.dart';
import 'package:zhibao_flutter/util/http/http.dart';

const mainTextColor = Color.fromRGBO(115, 115, 115, 1.0);
const mainSpace = 10.0;
const chatBg = Color(0xffffffff);
const lineColor = Colors.grey;

class AppConfig {
  /// Is need to Agree the user privacy and if not will can't
  /// select [ProtocolView].
  static bool needAgreeTheUserPrivacy = false;

  // If use flutter page to auth phone login else use the native page make of
  // jVerify.
  static bool verifyUseFlutterPage = false;

  /// 支持网络请求抓包的代理url
  /// value=代理地址:8888
  static String proxyUrl = inProduction ? '' : "";

  static final defLan = Locale('en', 'US');

  static Rx<Locale> currentLan = defLan.obs;

  static String get countyCodeGet =>
      GetStorage().read(SpKey.countryCodeForPhone) ?? countyCodeGetDef;

  // 检测系统语言自动设置手机号码区号
  static String get countyCodeGetDef {
    final countryCode = Get.deviceLocale?.countryCode?.toLowerCase() ?? "cn";
    Map<String, String> countryToAreaCode = {
      "cn": "+86", // China
      "us": "+1", // United States
      "br": "+55", // Brazil
      "gb": "+44", // United Kingdom
    };
    if (countryToAreaCode.containsKey(countryCode)) {
      return countryToAreaCode[countryCode]!;
    } else {
      return "+1";
    }
  }

  /// App locale.
  ///
  /// Just app show language.
  static String get localeToHeader {
    return locale.toLanguageTag();
  }

  static Locale get locale {
    GetStorage box = GetStorage();
    final languageCode = box.read("languageCode");
    if (GetUtils.isNullOrBlank(languageCode)!) {
      return Get.deviceLocale ?? defLan;
    }
    final countryCode = box.read("countryCode");
    q1Logger.info(
        "lAppConfig::languageCode: $languageCode, countryCode: $countryCode");
    return Locale(languageCode, countryCode);
  }

  /// appName
  static String get appName => 'IDO168';

  // 每页加载数量
  static int pageLimit = 20;
  static int pageLimitInUserInfoDynamic = 2;

  /// Home page use it.
  static int pageLimitOfGrid = 21;

  /// 用户信息主页女生图片显示高度
  static double userInfoBannerHeight = 400.0;

  /// 键盘高度
  static double keyboardHeight = 300;

  /// If un required mock version set as null.
  static String? mockVersion = null; //"3.6.0"

  /// 可以撤回消息的秒，120秒就是2分钟
  static double recallSecond = 120;

  /// Current is can change the skin, if [true] will onPress to change the
  /// skin page in set page right top.
  static bool testChangeTheSkin = true;

  /// 是否测试模式
  static bool isTestMode = false;

  /// Hide [OneDayCp] function when [approvalMode] is ture.
  static bool approvalModeHideOnyDayCp = true;

  /// 是否显示测试菜单
  ///
  /// 在设置页面的【账号信息】点击10次也会出来。
  /// 在设置页面的【账号信息】点击10次出现【测试专用悬浮窗】，点击悬浮窗进入日志列表，可以看到整体打印日志。
  static bool showTestMenu = true;

  /// 是否模拟web参数
  static bool mockWeb = false;

  /// 是否生产环境
  static bool inProduction = const bool.fromEnvironment("dart.vm.product");

  /// 语言
  static String language = "zh";

  /// 主要访问地址
  // static String host = "api.lianbao360.com";
  static String host = "3.25.77.194:9999";

  static String get userAgreement => "https://$addressOld/user_agreement.html";

  static String get privacyPolicy => "https://$addressOld/privacy.html";

  static String addressOld = "https://api.lianbao360.com/";

  // static String address = "https://api.lianbao360.com/";
  // static String devWeb = "https://api.lianbao360.com/";
  // static String devApp = "https://api.lianbao360.com/";
  // static String proApp = "https://api.lianbao360.com/";

  static String get timeOutStr => 'ConnectionTimeout'.tr;

  static String get loginExpired => 'LoginExpired'.tr;

  /// 默认图片【当网络图片加载中或加载失败时展示】
  static String defPic = "assets/images/ic_launcher.png";

  /// 默认头像【当网络图片加载中或加载失败时展示或没有头像时】
  // static String defAvatar = "assets/images/ic_launcher.png";

  static bool allowAccountNotPhone = true;

  static int maxLevel = 12;

  static String tokenHead = "X-Token";

  static String inviterKey = "i";

  static String gameLink = "http://h5.kuwan8.com";

  static String get mockTarget {
    if (inProduction) return "";
    return "15681855446";
  }

  static String get mockAddress {
    if (inProduction) return "";
    return "0x85601d40bBcDF00F7129C190c58FBCeBCe5c6527";
  }

  static String get mockInviteCode {
    if (inProduction) return "";
    // return "ycSD0n";
    /// International.
    return "YVQpnu";
  }

  static String get mockPhone {
    if (inProduction) return "";

    /// # Test chat function.
    // return "";
    // return "18782255949";
    return "13244766725";
  }

  static String get mockEmail {
    if (inProduction) return "";

    /// # Test chat function.
    // return "";
    // return "18782255949";
    return "zonggeu@163.com";
  }

  static String get mockPw {
    if (inProduction) return "";
    return "123456";
  }
}
