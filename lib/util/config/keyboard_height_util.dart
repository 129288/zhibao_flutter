import 'package:zhibao_flutter/util/func/sp_util.dart';
import 'package:zhibao_flutter/zone.dart';

class KeyboardHeightUtil {
  /// 键盘高度
  static double keyHeight = 300;

  static final String _keyboardHeightUtilKey = "_keyboardHeightUtilKey";

  static Future initKeyboardHeight() async {
    final double? heightOfStore = SpUtil().getDouble(_keyboardHeightUtilKey);
    LogUtil.d("KeyboardHeightUtil::initKeyboardHeight::$heightOfStore");
    if (heightOfStore == null) {
      return;
    }
    keyHeight = heightOfStore;
  }

  static Future setKeyboardHeight(double heightValue) async {
    keyHeight = heightValue;
    SpUtil().setDouble(_keyboardHeightUtilKey, heightValue);
  }
}

class BottomHeightUtil {
  /// 底部安全区高度
  static double bottomPadding = 0;

  static final String _bottomHeightUtilKey = "_bottomHeightUtilKey";

  static Future initHeight() async {
    final double? heightOfStore = SpUtil().getDouble(_bottomHeightUtilKey);
    if (heightOfStore == null) {
      return;
    }
    bottomPadding = heightOfStore;
  }

  static Future setHeight(double heightValue) async {
    LogUtil.d("BottomHeightUtil::setHeight::$heightValue");
    bottomPadding = heightValue;
    SpUtil().setDouble(_bottomHeightUtilKey, heightValue);
  }
}
