import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:zhibao_flutter/util/func/log.dart';

const String q1AndroidUtilName = "com.q1android.util:plugin";

class Q1AndroidUtil {
  static Q1AndroidUtil? _instance;
  final MethodChannel _methodChannel;

  factory Q1AndroidUtil() {
    if (_instance == null) {
      MethodChannel methodChannel = const MethodChannel(q1AndroidUtilName);

      _instance = Q1AndroidUtil.private(
        methodChannel,
      );
    }
    return _instance!;
  }

  @visibleForTesting
  Q1AndroidUtil.private(
    this._methodChannel,
  );

  Future<void> chatInWhatsApp(String mobileNum) async {
    try {
      await _methodChannel
          .invokeMethod("chatInWhatsApp", {"mobileNum": mobileNum});
    } catch (e) {
      LogUtil.d("chatInWhatsApp Error : ${e.toString()}");
    }
  }

  Future<int?> getReleaseDate() async {
    try {
      return _methodChannel.invokeMethod<int>("getReleaseDate", {});
    } catch (e) {
      LogUtil.d("getReleaseDate Error : ${e.toString()}");
      return 0;
    }
  }

  Future<String> getAndroidId() async {
    try {
      return (await _methodChannel.invokeMethod<String?>("getAndroidId", {}) ??
          "");
    } catch (e) {
      LogUtil.d("getAndroidId Error : ${e.toString()}");
      return "";
    }
  }

  Future<bool> onlyCheckCameraDisable() async {
    final value =
        await _methodChannel.invokeMethod<bool?>("onlyCheckCameraDisable", {});
    LogUtil.d("onlyCheckCameraDisable::value::$value");
    return value ?? false;
  }

  Future<bool> onlyCheckStoreDisable() async {
    final value =
        await _methodChannel.invokeMethod<bool?>("onlyCheckStoreDisable", {});
    LogUtil.d("onlyCheckStoreDisable::value::$value");
    return value ?? false;
  }

  Future<bool> onlyCheckContactDisable() async {
    final value =
        await _methodChannel.invokeMethod<bool?>("onlyCheckContactDisable", {});
    LogUtil.d("onlyCheckContactDisable::value::$value");
    return value ?? false;
  }

  Future<String> getImei() async {
    return "";
    final value = await _methodChannel.invokeMethod<String?>("getImei", {});
    LogUtil.d("getImei::value::$value");
    return value ?? "";
  }
}
