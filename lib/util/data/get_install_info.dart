import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:zhibao_flutter/util/func/log.dart';

const String pluginChannelName = "com.install.info:plugin";

class GetInstallInfo {
  static GetInstallInfo? _instance;
  final MethodChannel _methodChannel;

  factory GetInstallInfo() {
    if (_instance == null) {
      MethodChannel methodChannel = const MethodChannel(pluginChannelName);

      _instance = GetInstallInfo.private(
        methodChannel,
      );
    }
    return _instance!;
  }

  @visibleForTesting
  GetInstallInfo.private(
    this._methodChannel,
  );

  Future<int> referrerClickTime() async {
    try {
      final int? value =
          await _methodChannel.invokeMethod<int>("referrerClickTime", {});
      if (value == null) {
        return 0;
      } else {
        return value;
      }
    } catch (e) {
      LogUtil.d("referrerClickTime Error : ${e.toString()}");
      return 0;
    }
  }

  Future<int> appInstallTime() async {
    try {
      final int? value =
          await _methodChannel.invokeMethod<int>("appInstallTime", {});
      if (value == null) {
        return 0;
      } else {
        return value;
      }
    } catch (e) {
      LogUtil.d("appInstallTime Error : ${e.toString()}");
      return 0;
    }
  }
}
