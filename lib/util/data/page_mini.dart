import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

/// 初始化示例
// initComment().then((value) {
// if (comments.length > 11) {
// initPage();
// }
// });
/// 数据处理示例
// if (!listNoEmpty(value.data)) {
// hasNextPage = false;
// }
// if (goPage == 1) {
// data = List.from(value.data);
// } else {
// data.addAll(List.from(value.data));
// }
mixin PageMiniAbs {
  void update([List<Object>? ids, bool condition = true]);
}
mixin PageMini on PageMiniAbs {
  ScrollController pageScrollController = ScrollController();
  bool isPageLoading = false;
  bool hasNextPage = true;
  bool isLoadOk = false;
  int goPage = 1;

  RefreshController refreshController =
      RefreshController(initialRefresh: false);

  /// 每页数据条数
  static int perPage = 10;

  initPage() {
    if (pageScrollController != null) {
      pageScrollController.addListener(() async {
        if (!hasNextPage) {
          return;
        }
        if (pageScrollController.position.pixels >=
                pageScrollController.position.maxScrollExtent &&
            !isPageLoading) {
          isPageLoading = true;
          await loadMoreData();
          await Future.delayed(const Duration(seconds: 2));
          isPageLoading = false;
        }
        if (pageScrollController.position.pixels ==
                pageScrollController.position.minScrollExtent &&
            !isPageLoading) {
          isPageLoading = true;
          await refreshData();
          await Future.delayed(const Duration(seconds: 2));
          isPageLoading = false;
        }
      });
    }
  }

  void setLoadOk() {
    isLoadOk = true;
    update();
  }

  @protected
  Future refreshData() {
    return Future.value();
  }

  @protected
  Future loadMoreData() {
    return Future.value();
  }
}
