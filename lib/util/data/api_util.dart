import 'package:zhibao_flutter/util/config/app_config.dart';

class ApiUtil {
  /// Get real page index.
  static int? getOffset(int? offset, int? limit) {
    if (offset == null) {
      return offset;
    }
    final int limitValue = limit ?? AppConfig.pageLimit;
    return offset + (offset * limitValue);
  }
}
