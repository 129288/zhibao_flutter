import 'package:flutter/material.dart';
import 'package:zhibao_flutter/widget/image/sw_image.dart';

/*
* 缓存加载图片
*
* */
Future cacheImgData(context) async {
  await precacheImage(
      const AssetImage('assets/images/bottom/bg_stay_toned_lian.png'), context);
  await precacheImage(
      const AssetImage('assets/images/bottom/bg_stay_toned_game.png'), context);
}
