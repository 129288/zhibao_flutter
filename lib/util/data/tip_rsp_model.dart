import 'dart:convert';
import 'dart:developer';

class TipGetRspModel {
  final dynamic data;
  final String msg;

  TipGetRspModel(this.data, this.msg);
}
