class Q1Actions {
  static String toTabBarIndex() => 'toTabBarIndex';

  static String toTabBarIndexLast() => 'toTabBarIndexLast';

  static String refreshComoLlegarAccount() => 'refreshCómoLlegarAccount';

  static String toTab(int index) => 'toTab$index';
}
