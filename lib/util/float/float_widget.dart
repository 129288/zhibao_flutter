import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zhibao_flutter/util/event_bus/refresh_event_bus.dart';
import 'package:zhibao_flutter/zone.dart';

class FloatWidget extends StatefulWidget {
  final VoidCallback onTap;

  FloatWidget(this.onTap);

  @override
  State<FloatWidget> createState() => _FloatWidgetState();
}

class _FloatWidgetState extends State<FloatWidget> {
  double _left = FrameSize.screenW() - (60 * 1.5);
  double _top = FrameSize.screenH() * 0.76;

  /// 尺寸
  double itemSize = 42.px;

  Widget _draggableView(BuildContext context) {
    return ClickEvent(
      onTap: () async {
        LogUtil.d("FloatUtil call refresh page");
        widget.onTap();
      },
      child: Container(
        width: itemSize,
        height: itemSize,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: MyTheme.mainColor.value,
          shape: BoxShape.circle,
        ),
        child: Icon(
          CupertinoIcons.home,
          size: 29.5.px,
          color: Colors.white,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Positioned(
      /// 【2021 11.25】优化小窗口位置（安卓苹果统一）
      /// 直播UI验收11.16 - 飞书云文档   【商品分享】安卓、IOS分享商品，都会出现挡住
      top: _top,
      left: _left,
      child: GestureDetector(
        // 移动中
        onPanUpdate: (details) {
          setState(() {
            _top = details.globalPosition.dy - (itemSize / 2);
            _left = details.globalPosition.dx - (itemSize / 2);
          });
          LogUtil.d("_top::$_top");
          LogUtil.d("_left::$_left");
        },
        // 移动结束
        onPanEnd: (details) {
          setState(() {
            if (_top <= (FrameSize.padTopH() * 1.5)) {
              _top = FrameSize.padTopH() * 1.5;
            }

            if (_top >= FrameSize.screenH() - 200.px) {
              _top = FrameSize.screenH() - 200.px;
            }

            if (_left > FrameSize.winWidth() - itemSize) {
              _left = FrameSize.screenW() - itemSize;
            }
            if (_left < 0) {
              _left = 0;
            }
          });
        },

        child: Material(
          type: MaterialType.transparency,
          child: _draggableView(context),
        ),
      ),
    );
  }
}
