import 'package:flutter/material.dart';

class UserLabelColor {
  static List<Color> textColors = [
    Color(0xffF46B0F),
    Color(0xff78C115),
    Color(0xffEE4D24),
    Color(0xff4C85FF),
    Color(0xff6E51C1),
    Color(0xffD8A511),
    Color(0xffF46B0F),
  ];
  static List<Color> bgColors = [
    Color(0xffFFF3E5),
    Color(0xffECF3E0),
    Color(0xffFFF3F1),
    Color(0xffECF2FF),
    Color(0xffF2EDFF),
    Color(0xffFFF7DC),
    Color(0xffFFF3E5),
  ];

  static Color getTextColor(int index) {
    return textColors[index % textColors.length];
  }

  static Color getBgColor(int index) {
    return bgColors[index % bgColors.length];
  }
}
