import 'package:flutter/material.dart';
import 'package:zhibao_flutter/util/float/fram_size.dart';

/// 横线
class HorizontalLine extends StatelessWidget {
  final double height;
  final Color? color;
  final EdgeInsetsGeometry? margin;

  const HorizontalLine({
    this.height = 1,
    this.color,
    this.margin,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      color: color ?? const Color(0xffDFE2E4),
      margin: margin,
    );
  }
}

/// 竖线
class VerticalLine extends StatelessWidget {
  final double width;
  final double height;
  final Color color;
  final double vertical;
  final EdgeInsetsGeometry? margin;

  const VerticalLine({
    this.width = 1.0,
    this.height = 25,
    this.color = const Color(0xff000000),
    this.vertical = 0.0,
    this.margin,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      color: color,
      margin: margin ?? EdgeInsets.symmetric(vertical: vertical),
      height: height,
    );
  }
}

/// 间隔组件
class Space extends StatelessWidget {
  final double? width;
  final double? height;

  const Space({this.width, this.height, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(width: width ?? 10.px, height: height ?? 10.0.px);
  }
}
