import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

/*
* num扩展方法
* */
// extension ScreenUtilNum on num {
//   /// 根据设计稿的设备高度适配【简单用法】
//   /// 例子：
//   /// fontSize: 10.f
//   double get f {
//     return ScreenUtil.f(this);
//   }
//
//   /// 根据设计稿的设备高度适配【简单用法】
//   /// 例子：
//   /// height: 10.h
//   double get h {
//     return ScreenUtil.h(this);
//   }
//
//   /// 根据设计稿的设备宽度适配【简单用法】
//   /// 例子：
//   /// width: 10.w
//   double get w {
//     return ScreenUtil.w(this);
//   }
// }

class ScreenUtil {
  //设计稿的设备尺寸修改
  static int width = 375;
  static int height = 812;
  static bool allowFontScaling = true;

  static MediaQueryData mediaQuery = MediaQueryData.fromWindow(window);

  static double _pixelRatio = mediaQuery.devicePixelRatio;
  static double _screenWidth = mediaQuery.size.width;
  static double _screenHeight = mediaQuery.size.height;
  static double _statusBarHeight = mediaQuery.padding.top;
  static double _bottomBarHeight = mediaQuery.padding.bottom;
  static final double _textScaleFactor = mediaQuery.textScaleFactor;

  ///每个逻辑像素的字体像素数，字体的缩放比例
  static double get textScaleFactory => _textScaleFactor;

  ///设备的像素密度
  static double get pixelRatio => _pixelRatio;

  ///当前设备宽度 dp
  static double get screenWidthDp => _screenWidth;

  ///当前设备高度 dp
  static double get screenHeightDp => _screenHeight;

  ///当前设备宽度 px
  static double get screenWidth => _screenWidth * _pixelRatio;

  ///当前设备高度 px
  static double get screenHeight => _screenHeight * _pixelRatio;

  ///状态栏高度 刘海屏会更高 px
  static double get statusBarHeight => _statusBarHeight * _pixelRatio;

  ///状态栏高度 刘海屏会更高 dp
  static double get statusBarHeightDp => _statusBarHeight;

  ///底部安全区距离
  static double get bottomBarHeight => _bottomBarHeight * _pixelRatio;

  ///实际的dp与设计稿px的比例
  static get scaleWidth => _screenWidth / width;

  static get scaleHeight => _screenHeight / height;

  static double winWidth() {
    return screenWidth;
  }

  static double winHeight() {
    return screenHeight;
  }

  ///根据设计稿的设备宽度适配
  ///高度也根据这个来做适配可以保证不变形
  static w(num? width) {
    if (width == null) return null;
    return width * scaleWidth;
  }

  /// 根据设计稿的设备高度适配
  /// 当发现设计稿中的一屏显示的与当前样式效果不符合时,
  /// 或者形状有差异时,高度适配建议使用此方法
  /// 高度适配主要针对想根据设计稿的一屏展示一样的效果
  static h(num? height) {
    if (height == null) return null;
    return height * scaleHeight;
  }

  ///字体大小适配方法
  ///@param fontSize 传入设计稿上字体的px ,
  ///@param allowFontScaling 控制字体是否要根据系统的“字体大小”辅助选项来进行缩放。默认值为true。
  ///@param allowFontScaling Specifies whether fonts should scale to respect Text Size accessibility settings. The default is true.
  static f(num fontSize) {
//    print('allowFontScaling == $allowFontScaling');
    return allowFontScaling ? w(fontSize) : w(fontSize) / _textScaleFactor;
  }

  static lineHeight(num fontSize) {
//    print('allowFontScaling == $allowFontScaling');
    return w(fontSize) / _textScaleFactor * 1.2;
  }

  /// 键盘高度
  /// 如果为0则是键盘未弹出
  static double winKeyHeight(BuildContext context) {
    return MediaQuery.of(context).viewInsets.bottom;
  }
}
