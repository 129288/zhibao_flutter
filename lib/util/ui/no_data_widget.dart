import 'package:flutter/material.dart';
import 'package:zhibao_flutter/util/float/fram_size.dart';
import 'package:zhibao_flutter/util/ui/ui.dart';

class NoDataWidget extends StatelessWidget {
  final bool miniHeight;

  const NoDataWidget({this.miniHeight = false, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NoDataWidgetOfQianbaozaie(miniHeight: miniHeight);
    //Center(child: Text('暂无数据'));
  }
}

class NoDataWidgetOfQianbaozaie extends StatelessWidget {
  final bool miniHeight;

  const NoDataWidgetOfQianbaozaie({this.miniHeight = false, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin:
          !miniHeight ? EdgeInsets.zero : EdgeInsets.symmetric(vertical: 27),
      height: !miniHeight
          ? ((FrameSize.winHeightDynamic(context) - FrameSize.topBarHeight()) *
              0.7)
          : null,
      width: FrameSize.winWidthDynamic(context),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            'assets/images/qianbaozai/ic_kby_zwsj.png',
            width: 191.65,
            height: 95,
          ),
          Space(height: 21),
          Text(
            '暂无数据',
            style: TextStyle(color: Color(0xff9E9E9E), fontSize: 16),
          ),
        ],
      ),
    );
  }
}
