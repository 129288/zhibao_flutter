import 'package:flutter/material.dart';
import 'package:zhibao_flutter/util/float/fram_size.dart';
import 'package:zhibao_flutter/widget/bt/Inkwell_without_color.dart';
import 'package:zhibao_flutter/zone.dart';

Future offsetTeamMenu(BuildContext context, Offset offset) {
  return showDialog(
    context: context,
    useSafeArea: false,
    builder: (context) {
      return OffsetTeamMenu(offset);
    },
  );
}

class OffsetTeamMenu extends StatefulWidget {
  final Offset offset;

  OffsetTeamMenu(this.offset);

  @override
  State<OffsetTeamMenu> createState() => _OffsetTeamMenuState();
}

class _OffsetTeamMenuState extends State<OffsetTeamMenu> {
  @override
  Widget build(BuildContext context) {

    // 【feb 29 2024】
    // 账号等级列表的下拉选项：字行距间距拉大一点；123级文字前面放个小钻石图标
    //
    final double itemHeight = (146 / 5).px + 10.px;

    // 【feb 29 2024】
    // 账号等级列表的下拉选项：字行距间距拉大一点；123级文字前面放个小钻石图标
    //
    final double itemWidth = 93.px;
    return Material(
      type: MaterialType.transparency,
      child: Stack(
        children: [
          SizedBox(
            width: FrameSize.winWidth(),
            height: FrameSize.winHeight(),
          ),
          Positioned(
            left: widget.offset.dx,
            top: widget.offset.dy + 26.px,
            child: Container(
              width: itemWidth,
              decoration: BoxDecoration(
                color: Color(0xff252324),
                borderRadius: BorderRadius.all(Radius.circular(10.px)),
              ),
              child: Column(
                children: List.generate(Q1Data.grade, (index) {
                  final levelItem = index + 1;
                  return InkwellWithOutColor(
                    child: Container(
                      height: itemHeight,

                      // 【feb 29 2024】
                      // 账号等级列表的下拉选项：字行距间距拉大一点；123级文字前面放个小钻石图标
                      //
                      alignment: Alignment.center,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          // 【feb 29 2024】
                          // 账号等级列表的下拉选项：字行距间距拉大一点；123级文字前面放个小钻石图标
                          //
                          Image.asset(
                            'assets/images/ic/ic_diamond.png',
                            width: 25.06.px,
                          ),
                          SizedBox(width: 10.px),
                          Text(
                            '$levelItem${'level'.tr}',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16.px,
                            ),
                          )
                        ],
                      ),
                    ),
                    onTap: () {
                      Get.back(result: levelItem);
                    },
                  );
                }),
              ),
            ),
          )
        ],
      ),
    );
  }
}
