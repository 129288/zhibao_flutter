import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:zhibao_flutter/api/mine/mine_entity.dart';
import 'package:zhibao_flutter/api/mine/mine_model.dart';
import 'package:zhibao_flutter/api/user/user_entity.dart';
import 'package:zhibao_flutter/util/config/app_config.dart';
import 'package:zhibao_flutter/util/config/sp_key.dart';
import 'package:zhibao_flutter/util/data/page_rsp_model.dart';
import 'package:zhibao_flutter/util/func/log.dart';
import 'package:zhibao_flutter/util/func/sp_util.dart';
import 'package:zhibao_flutter/util/http/base_view_model.dart';
import 'package:zhibao_flutter/util/http/http.dart';
import 'package:zhibao_flutter/zone.dart';

MineViewModel mineViewModel = MineViewModel();

class MineViewModel extends BaseViewModel {
  /*
  * GetSelfInfoRequestModel
  *
  **/
  Future<ResponseModel> getSelfInfo(BuildContext? context, String token,
      {OnData? onStore}) async {
    return GetSelfInfoRequestModel(token)
        .sendApiAction(
      context,
      reqType: ReqType.get,
      onStore: onStore == null
          ? null
          : (rep) {
              onStore(UserInfoEntity.fromJson(rep));
            },
    )
        .then((rep) {
      Q1Data.userInfoEntity = UserInfoEntity.fromJson(rep);
      Q1Data.userInfoEntity?.avatar =
          "${Q1Data.getFullAvatar(Q1Data.userInfoEntity?.avatar ?? "")}?${DateTime.now()}";
      SpUtil().setString(SpKey.userInfo, json.encode(Q1Data.userInfoEntity));
      return ResponseModel.fromSuccess(Q1Data.userInfoEntity);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * SetSelfInfoRequestModel
  *
  **/
  Future<ResponseModel> setSelfInfo(
    BuildContext context, {
    required final String username,
  }) async {
    return SetSelfInfoRequestModel(
      username,
    )
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      return ResponseModel.fromSuccess(rep ?? true);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * UserIndexRequestModel
  *
  **/
  Future<ResponseModel> userIndex(
    BuildContext? context, {
    OnData? onStore,
  }) async {
    return UserIndexRequestModel()
        .sendApiAction(
      context,
      reqType: ReqType.post,
      onStore: onStore == null
          ? null
          : (rep) {
              onStore(SelfInfoRspEntity.fromJson(rep['self_info']));
            },
    )
        .then((rep) {
      SelfInfoRspEntity rsp = SelfInfoRspEntity.fromJson(rep['self_info']);

      return ResponseModel.fromSuccess(rsp);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * AddActionRequestModel
  *
  **/
  Future<ResponseModel> addAction(
    BuildContext? context, {
    required final int action_type,
    required final String related_id,
  }) async {
    return AddActionRequestModel(
      action_type,
      related_id,
    )
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      return ResponseModel.fromSuccess(rep);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * GetRegisterDataRequestModel
  *
  **/
  Future<ResponseModel> getRegister(BuildContext? context) async {
    return GetRegisterDataRequestModel()
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      RegisterBaseData registerBaseData = RegisterBaseData.fromJson(rep);
      return ResponseModel.fromSuccess(registerBaseData);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * TimelinessDataRequestModel
  *
  **/
  Future<ResponseModel> timelinessData(
    BuildContext context,
    OnData onStore,
  ) async {
    return TimelinessDataRequestModel()
        .sendApiAction(context, reqType: ReqType.post, onStore: (rep) {
      onStore(rep);
    }).then((rep) {
      return ResponseModel.fromSuccess(rep);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * UserImgRequestModel
  *
  **/
  Future<ResponseModel> userImg(
    BuildContext? context, {
    final int? offset,
    final int? get_type,
    OnData? onStore,
  }) async {
    return UserImgRequestModel(
      offset,
      AppConfig.pageLimit,
      get_type,
    )
        .sendApiAction(context,
            reqType: ReqType.post,
            onStore: onStore == null
                ? null
                : (v) {
                    final repList =
                        List.from(v['data'] ?? []).map<MyPhotoEntity>((e) {
                      return MyPhotoEntity.fromJson(e);
                    }).toList();
                    onStore(repList);
                  })
        .then((rep) {
      final repList = List.from(rep['data'] ?? []).map<MyPhotoEntity>((e) {
        return MyPhotoEntity.fromJson(e);
      }).toList();

      PagingRspModel pagingRspModel = PagingRspModel.fromJson(rep['paging']);
      return ResponseModel.fromSuccess(
          PageGetRspModel<MyPhotoEntity>(repList, pagingRspModel));
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * UserVideoRequestModel
  *
  **/
  Future<ResponseModel> userVideo(
    BuildContext? context, {
    required final int offset,
    OnData? onStore,
  }) async {
    return UserVideoRequestModel(
      offset,
      AppConfig.pageLimit,
    )
        .sendApiAction(
      context,
      reqType: ReqType.post,
      onStore: onStore == null
          ? null
          : (v) {
              final repList =
                  List.from(v['data'] ?? []).map<VideoRspEntity>((e) {
                return VideoRspEntity.fromJson(e);
              }).toList();
              onStore(repList);
            },
    )
        .then((rep) {
      final repList = List.from(rep['data'] ?? []).map<VideoRspEntity>((e) {
        return VideoRspEntity.fromJson(e);
      }).toList();

      PagingRspModel pagingRspModel = PagingRspModel.fromJson(rep['paging']);
      return ResponseModel.fromSuccess(
          PageGetRspModel<VideoRspEntity>(repList, pagingRspModel));
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * GetSetPermissionNicknameContactRequestModel
  *
  **/
  Future<ResponseModel> getSetPermissionNicknameContact(
      BuildContext context) async {
    return GetSetPermissionNicknameContactRequestModel()
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      return ResponseModel.fromSuccess(rep);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * GetQuestionsAndAnswersRequestModel
  *
  **/
  @deprecated
  Future<ResponseModel> getQuestionsAndAnswers(BuildContext context) async {
    return GetQuestionsAndAnswersRequestModel()
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      return ResponseModel.fromSuccess(rep);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * GetQianbaozaieInfoRequestModel
  *
  **/
  Future<ResponseModel> getQianbaozaieInfo(
    BuildContext? context, {
    required final int offset,
    required final int limit,
    required final int get_type,
    OnData? onStore,
  }) async {
    return GetQianbaozaieInfoRequestModel(
      offset,
      limit,
      get_type,
    )
        .sendApiAction(
      context,
      reqType: ReqType.post,
      onStore: onStore == null
          ? null
          : (v) {
              QianbaozaieRspEntity qianbaozaiRspEntity =
                  QianbaozaieRspEntity.fromJson(v);
              onStore(qianbaozaiRspEntity);
            },
    )
        .then((rep) {
      QianbaozaieRspEntity qianbaozaiRspEntity =
          QianbaozaieRspEntity.fromJson(rep);
      return ResponseModel.fromSuccess(qianbaozaiRspEntity);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * SetComoLlegarAccountRequestModel
  *
  **/
  Future<ResponseModel> setComoLlegarAccount(
    BuildContext context, {
    required final String comoLlegar_account,
    required final String comoLlegar_name,
  }) async {
    return SetComoLlegarAccountRequestModel(
      comoLlegar_account,
      comoLlegar_name,
    )
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      return ResponseModel.fromSuccess(rep);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * RecoverSnapImgRequestModel
  *
  **/
  Future<ResponseModel> recoverSnapImg(BuildContext context) async {
    return RecoverSnapImgRequestModel()
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      return ResponseModel.fromSuccess(rep);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * ReceiveRedBagRequestModel
  *
  **/
  Future<ResponseModel> receiveRedBag(
    BuildContext context, {
    required final String order_id,
  }) async {
    return ReceiveRedBagRequestModel(order_id)
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      return ResponseModel.fromSuccess(rep);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * RedBagDetailRequestModel
  *
  **/
  Future<ResponseModel> redBagDetail(
    BuildContext context, {
    required String order_id,
  }) async {
    return RedBagDetailRequestModel(
      order_id,
    )
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      return ResponseModel.fromSuccess(rep);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * FetchDepositRequestModel
  *
  **/
  Future<ResponseModel> fetchDeposit(
    BuildContext context, {
    required final String amount,
  }) async {
    return FetchDepositRequestModel(amount)
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      return ResponseModel.fromSuccess(rep);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * ShowBlacklistRequestModel
  *
  **/
  Future<ResponseModel> showBlacklist(BuildContext context) async {
    return ShowBlacklistRequestModel()
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      return ResponseModel.fromSuccess(rep);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * RemoveBlacklistRequestModel
  *
  **/
  Future<ResponseModel> removeBlacklist(
    BuildContext context, {
    required final String target_uid,
  }) async {
    return RemoveBlacklistRequestModel(target_uid)
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      return ResponseModel.fromSuccess(rep);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * AddBlacklistRequestModel
  *
  **/
  Future<ResponseModel> addBlacklist(
    BuildContext context, {
    required final String target_uid,
  }) async {
    return AddBlacklistRequestModel(target_uid)
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      return ResponseModel.fromSuccess(rep);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * ConfirmRealityRequestModel
  *
  **/
  Future<ResponseModel> confirmReality(
    BuildContext context, {
    required final String img_url,
  }) async {
    return ConfirmRealityRequestModel(img_url)
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      return ResponseModel.fromSuccess(rep);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * GetConfirmRealityPermissionRequestModel
  *
  **/
  Future<ResponseModel> getConfirmRealityPermission(
      BuildContext context) async {
    return GetConfirmRealityPermissionRequestModel()
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      return ResponseModel.fromSuccess(rep['result']);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * CheckNicknameExistRequestModel
  *
  **/
  Future<ResponseModel> checkNicknameExist(
    BuildContext context, {
    required final String nickname,
    final Function(String msg)? callViolation,
  }) async {
    return CheckNicknameExistRequestModel(nickname)
        .sendApiAction(
      context,
      reqType: ReqType.post,
      isFetchResult: false,
    )
        .then((rep) {
      if (rep['code'] != 0) {
        if (callViolation != null) callViolation(rep['msg']);
        throw ResponseModel.fromError(
            rep['msg'], rep['code'], 'checkNicknameExist');
      }
      if (rep['status'] != 0) {
        if (callViolation != null) callViolation(rep['msg']);
        throw ResponseModel.fromError(
            rep['msg'], rep['code'], "checkNicknameExist");
      }

      if (rep['data']['nickname_exist'] == 1) {
        if (callViolation != null) callViolation(rep['msg']);
        throw ResponseModel.fromError(
            rep['msg'], rep['code'], "checkNicknameExist");
      }
      return ResponseModel.fromSuccess(rep['data']['nickname_exist']);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace, showToast: callViolation == null);
    });
  }

  /*
  * YaoqingrenUserLogRequestModel
  *
  **/
  Future<ResponseModel> yaoqingrenUserLog(
    BuildContext context, {
    required final int get_type,
    OnData? onStore,
  }) async {
    return YaoqingrenUserLogRequestModel(get_type)
        .sendApiAction(
      context,
      reqType: ReqType.post,
      onStore: onStore == null
          ? null
          : (rep) {
              final List<YaoqingrenRecordRspEntity> value =
                  List.from(rep['data'] ?? [])
                      .map<YaoqingrenRecordRspEntity>((e) {
                YaoqingrenRecordRspEntity entity =
                    YaoqingrenRecordRspEntity.fromJson(e);
                return entity;
              }).toList();

              onStore(value);
            },
    )
        .then((rep) {
      final List<YaoqingrenRecordRspEntity> value =
          List.from(rep['data'] ?? []).map<YaoqingrenRecordRspEntity>((e) {
        YaoqingrenRecordRspEntity entity =
            YaoqingrenRecordRspEntity.fromJson(e);
        return entity;
      }).toList();

      /// 模拟数据
      // if (!AppConfig.inProduction) {
      //   value.add(YaoqingrenRecordRspEntity(
      //       nickname: "$get_type-nickname",
      //       amount: "1122",
      //       icon: Q1Data.icon,
      //       isVip: 1,
      //       registerTimeStr: "1天前"));
      //   value.add(YaoqingrenRecordRspEntity(
      //       nickname: "$get_type-nickna22me",
      //       amount: "2",
      //       icon: Q1Data.icon,
      //       isVip: 2,
      //       registerTimeStr: "5天前"));
      //   value.add(YaoqingrenRecordRspEntity(
      //       nickname: "$get_type-22dd",
      //       amount: "112",
      //       icon: Q1Data.icon,
      //       isConfirmed: 1,
      //       registerTimeStr: "2天前"));
      // }
      return ResponseModel.fromSuccess(value);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * YaoqingrenUserShachaImgRequestModel
  *
  **/
  Future<ResponseModel> yaoqingrenUserShachaImg(BuildContext? context) async {
    return YaoqingrenUserShachaImgRequestModel()
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      ShachaPicOptionRsp repModel = ShachaPicOptionRsp.fromJson(rep);
      return ResponseModel.fromSuccess(repModel);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * CheckCoinOrderRequestModel
  *
  **/
  Future<ResponseModel> checkCoinOrder(
    BuildContext context, {
    required final int order_type,
    required final String file_url,
  }) async {
    return CheckCoinOrderRequestModel(
      order_type,
      file_url,
    )
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      return ResponseModel.fromSuccess(rep);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * MakeCoinOrderRequestModel
  *
  **/
  Future<ResponseModel> makeCoinOrder(
    BuildContext context, {
    required final String peer_uid,
    required final int order_type,
    required final String file_url,
  }) async {
    return MakeCoinOrderRequestModel(
      peer_uid,
      order_type,
      file_url,
    )
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      return ResponseModel.fromSuccess(rep);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * VisitLogRequestModel
  *
  **/
  Future<ResponseModel> visitLog(
    BuildContext context,
    OnData? onStore, {
    required final int offset,
  }) async {
    return VisitLogRequestModel(offset, AppConfig.pageLimit)
        .sendApiAction(
      context,
      reqType: ReqType.post,
      onStore: onStore == null
          ? null
          : (v) {
              final repList =
                  List.from(v['data'] ?? []).map<WhoLookMeEntity>((e) {
                return WhoLookMeEntity.fromJson(e);
              }).toList();
              onStore(repList);
            },
    )
        .then((rep) {
      final repList = List.from(rep['data'] ?? []).map<WhoLookMeEntity>((e) {
        return WhoLookMeEntity.fromJson(e);
      }).toList();

      PagingRspModel pagingRspModel = PagingRspModel.fromJson(rep['paging']);

      return ResponseModel.fromSuccess(
          PageGetRspModel(repList, pagingRspModel));
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * CoinFetchDepositRequestModel
  *
  **/
  Future<ResponseModel> coinFetchDeposit(
    BuildContext context, {
    required final int amount,
  }) async {
    return CoinFetchDepositRequestModel(amount)
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      return ResponseModel.fromSuccess(rep);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * AddDevicePermissionRequestModel
  *
  **/
  Future<ResponseModel> addDevicePermission(
    BuildContext context, {
    required final int permission_type,
    required final int result,
  }) async {
    return AddDevicePermissionRequestModel(
      permission_type,
      result,
    )
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      return ResponseModel.fromSuccess(rep);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * GetPrivacySetRequestModel
  *
  **/
  Future<ResponseModel> getPrivacySet(
      BuildContext? context, OnData onStore) async {
    return GetPrivacySetRequestModel()
        .sendApiAction(context, reqType: ReqType.post, onStore: onStore)
        .then((rep) {
      return ResponseModel.fromSuccess(rep);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * SetPrivacyRequestModel
  *
  **/
  Future<ResponseModel> setPrivacy(
    BuildContext context, {
    required final String privacy_type,
    required final int privacy_value,
  }) async {
    return SetPrivacyRequestModel(
      privacy_type,
      privacy_value,
    )
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      return ResponseModel.fromSuccess(rep);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * UserAudioRequestModel
  *
  **/
  Future<ResponseModel> userAudio(BuildContext context) async {
    return UserAudioRequestModel()
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      final value = List.from(rep ?? [])
          .map<AudioRspEntity>((e) => AudioRspEntity.fromJson(e))
          .toList();
      return ResponseModel.fromSuccess(value);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * CheckContactLegalRequestModel
  *
  **/
  Future<ResponseModel> checkContactLegal(
    BuildContext context, {
    final String? wekaka,
    final String? qq,
    required Function(String tip) callError,
  }) async {
    return CheckContactLegalRequestModel(
      wekaka,
      qq,
    )
        .sendApiAction(
      context,
      reqType: ReqType.post,
      isFetchResult: false,
    )
        .then((rep) {
      LogUtil.d("rep['status']::${rep['status']}");
      if (rep['code'] != 0 || rep['status'] != 0) {
        callError(rep['msg']);
        throw ResponseModel.fromError(rep['msg'], rep['code']);
      }
      return ResponseModel.fromSuccess(rep);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace, showToast: false);
    });
  }

  /*
  * ConfirmRealityAfterRequestModel
  *
  **/
  Future<ResponseModel> confirmRealityAfter(
    BuildContext context, {
    required final String img_url,
  }) async {
    return ConfirmRealityAfterRequestModel(
      img_url,
    )
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      return ResponseModel.fromSuccess(rep);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * GetPushSwitchRequestModel
  *
  **/
  Future<ResponseModel> getPushSwitch(BuildContext context) async {
    return GetPushSwitchRequestModel()
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      NotificationRspEntity rspEntity = NotificationRspEntity.fromJson(rep);
      return ResponseModel.fromSuccess(rspEntity);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * SetPushSwitchRequestModel
  *
  **/
  Future<ResponseModel> setPushSwitch(
    BuildContext context, {
    required final String switch_type,
    required final int switch_value,
  }) async {
    return SetPushSwitchRequestModel(
      switch_type,
      switch_value,
    )
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      return ResponseModel.fromSuccess(rep);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * MakeDepositOrderRequestModel
  *
  **/
  Future<ResponseModel> makeDepositOrder(
    BuildContext context, {
    required final String peer_uid,
    required final String order_type,
  }) async {
    return MakeDepositOrderRequestModel(
      peer_uid,
      order_type,
    )
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      return ResponseModel.fromSuccess(rep);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * CheckDepositOrderRequestModel
  *
  **/
  Future<ResponseModel> checkDepositOrder(
    BuildContext context, {
    required final String order_type,
  }) async {
    return CheckDepositOrderRequestModel(
      order_type,
    )
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      return ResponseModel.fromSuccess(rep);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * ConfirmRealityCountRequestModel
  *
  **/
  Future<ResponseModel> confirmRealityCount(
    BuildContext context, {
    required final String img_url,
  }) async {
    return ConfirmRealityCountRequestModel(img_url)
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      return ResponseModel.fromSuccess(rep);

      /// 模拟成功结果
      // return ResponseModel.fromSuccess({"result": 1});
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * ConfirmRealityAgainRequestModel
  *
  **/
  Future<ResponseModel> confirmRealityAgain(
    BuildContext context, {
    required final List<String> img_urls,
  }) async {
    return ConfirmRealityAgainRequestModel(img_urls)
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      return ResponseModel.fromSuccess(rep);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * JoinLogRequestModel
  *
  **/
  Future<ResponseModel> joinLog(
    BuildContext context, {
    required final int get_type,
    required final int offset,
    required final int limit,
  }) async {
    return JoinLogRequestModel(
      get_type,
      offset,
      limit,
    )
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      return ResponseModel.fromSuccess(rep);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * GetDunConfirmImgRequestModel
  *
  **/
  Future<ResponseModel> getDunConfirmImg(
    BuildContext context, {
    required final String dun_token,
  }) async {
    return GetDunConfirmImgRequestModel(dun_token)
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      return ResponseModel.fromSuccess(rep);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }
}
