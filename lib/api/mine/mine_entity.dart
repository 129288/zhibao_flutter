import 'dart:convert';
import 'dart:developer';

import 'package:zhibao_flutter/api/user/user_entity.dart';
import 'package:zhibao_flutter/util/check/check.dart';
import 'package:get/get.dart';

void tryCatch(Function? f) {
  try {
    f?.call();
  } catch (e, stack) {
    log('$e');
    log('$stack');
  }
}

T? asT<T>(dynamic value) {
  if (value is T) {
    return value;
  }
  return null;
}

class UserInfoEntity {
  UserInfoEntity({
    this.avatar,
    this.icon,
    this.balance,
    this.inviteCode,
    this.referrerPhone,
    this.rechargeAddress,
    this.nickname,
    this.grade,
    this.totalEarn,
    this.todayEarn,
    this.id,
    this.phone,
    this.activityLevel,
    this.name,
    this.sex,
    this.cityId,
    this.isSingle,
    this.work,
    this.vipOverdue,
    this.breast,
    this.chestCircumference,
    this.label,
    this.dateType,
    this.payType,
    this.cityName,
    this.qq,
    this.wekaka,
    this.provinceId,
    this.provinceName,
    this.accountInHide,
    this.inCheckMsg,
    this.dataInCheck,
    this.dataCompleted,
    this.isVip,
    this.lastCityId,
    this.imgNum,
    this.videoNum,
    this.lastLogTime,
    this.peerUid,
    this.distance,
    this.distanceStr,
    this.boughtTimes,
    this.lastLogTimeType,
    this.isConfirmed,
    this.audioDuration,
    this.showAudio,
    this.audioUrl,
    this.reportText,
    this.showAlbum,
    this.price,
    this.orderType,
    this.newUserRefuse,
    this.showWekakaButton,
    this.fileData = const [],
    this.deleted,
  });

  factory UserInfoEntity.def() {
    return UserInfoEntity.fromJson({});
  }

  factory UserInfoEntity.fromJson(Map<String, dynamic> json) {
    final List<Object>? label = json['label'] is List ? <Object>[] : null;
    if (label != null) {
      for (final dynamic item in json['label']!) {
        if (item != null) {
          tryCatch(() {
            label.add(asT<Object>(item)!);
          });
        }
      }
    }

    final List<Object>? dateType =
        json['date_type'] is List ? <Object>[] : null;
    if (dateType != null) {
      for (final dynamic item in json['date_type']!) {
        if (item != null) {
          tryCatch(() {
            dateType.add(asT<Object>(item)!);
          });
        }
      }
    }
    return UserInfoEntity(
      icon: asT<String?>(json['icon']),
      avatar: asT<String?>(json['avatar']),
      balance: asT<double?>(() {
        String strValue =
            "${strNoEmpty("${json['balance'] ?? ""}") ? json['balance'] : 0.0}";
        strValue = strValue.length > 10 ? strValue.substring(0, 10) : strValue;
        return double.parse(strValue);
      }()),
      id: asT<int?>(json['id']),
      inviteCode: asT<String?>(json['inviteCode']),
      referrerPhone: asT<String?>(json['referrerPhone']),
      rechargeAddress: asT<String?>(json['rechargeAddress']),
      totalEarn: asT<String?>(json['totalEarn'] ?? "0.00"),
      todayEarn: asT<String?>(json['todayEarn'] ?? "0.00"),
      grade: asT<int?>(json['grade']),
      nickname: asT<String?>(json['nickname']),
      phone: asT<String?>(json['phone']),
      activityLevel: asT<String?>("${json['activityLevel'] ?? 0}"),
      name: asT<String?>(json['name']),
      sex: asT<int?>(json['sex']),
      cityId: asT<int?>(json['city_id']),
      isSingle: asT<int?>(json['is_single']),
      work: asT<int?>(json['work']),
      vipOverdue: asT<String?>(json['vip_overdue']),
      breast: asT<String?>(json['breast']),
      chestCircumference: asT<int?>(json['chest_circumference']),
      label: listNoEmpty(label) ? label! : [],
      dateType: listNoEmpty(dateType) ? dateType! : [],
      payType: asT<String?>(json['pay_type']),
      cityName: asT<String?>(json['city_name']),
      qq: asT<String?>(json['qq']),
      wekaka: asT<String?>(json['wx']),
      provinceId: asT<int?>(json['province_id']),
      provinceName: asT<String?>(json['province_name']),
      accountInHide: asT<int?>(json['account_in_hide']),
      inCheckMsg: asT<String?>(json['in_check_msg']),
      dataInCheck: asT<int?>(json['data_in_check']),
      dataCompleted: asT<int?>(json['data_completed']),
      isVip: asT<int?>(json['is_vip']),
      lastCityId: asT<int?>(json['last_city_id']),
      imgNum: asT<int?>(json['img_num']),
      videoNum: asT<int?>(json['video_num']),
      lastLogTime: asT<String?>(json['last_log_time']),
      peerUid: asT<String?>(json['peer_uid']),
      distance: asT<int?>(json['distance']),
      distanceStr: asT<String?>(json['distance_str']),
      boughtTimes: asT<int?>(json['bought_times']),
      lastLogTimeType: asT<int?>(json['last_log_time_type']),
      isConfirmed: asT<int?>(json['is_confirmed']),

      /// 模拟语音数据
      // audioDuration: 4,
      // showAudio: 1,
      // audioUrl:
      //     "https://candy-circle-copy.oss-cn-qingdao.aliyuncs.com/image/20221116/MNzTLTppxRCsvLTrfIxcvCUTNjhDktwf.m4a",
      audioDuration: asT<int?>(json['audio_duration']),
      showAudio: asT<int?>(json['show_audio']),
      audioUrl: asT<String?>(json['audio_url']),
      reportText: asT<String?>(json['report_text']),
      showAlbum: asT<int?>(json['show_album']),
      orderType: asT<int?>(json['order_type']),
      newUserRefuse: asT<int?>(json['new_user_refuse']),
      showWekakaButton: asT<int?>(json['show_wx_button']),
      price: asT<double?>(json['price'] is int
          ? (json['price'] as int).toDouble()
          : json['price']),
      fileData:
          List.from(json['file_data'] == "[]" ? [] : json['file_data'] ?? [])
              .map<FileDataEntity>((e) {
        return FileDataEntity.fromJson(e);
      }).toList(),
      // deleted: kReleaseMode ? asT<int?>(json['deleted']) : 1,
      deleted: asT<int?>(json['deleted']),
    );
  }

  String? icon;
  String? avatar;
  double? balance;
  int? id;
  String? rechargeAddress;
  String? inviteCode;
  String? referrerPhone;
  String? totalEarn;
  String? todayEarn;
  int? grade;
  String? nickname;
  String? phone;
  String? activityLevel;
  String? name;
  int? sex;
  int? cityId;
  int? isSingle;
  int? work;
  String? vipOverdue;
  String? breast;
  int? chestCircumference;
  List<Object>? label;
  List<Object>? dateType;
  String? payType;
  String? cityName;
  String? qq;
  String? wekaka;
  int? provinceId;
  String? provinceName;
  int? accountInHide;
  String? inCheckMsg;
  int? dataInCheck;
  int? dataCompleted;
  int? isVip;
  int? lastCityId;
  int? imgNum;
  int? videoNum;
  String? lastLogTime;
  String? peerUid;
  int? distance;
  String? distanceStr;
  int? boughtTimes;
  int? lastLogTimeType;
  int? isConfirmed;
  int? audioDuration;
  int? showAudio;
  String? audioUrl;
  String? reportText;
  int? showAlbum;
  double? price;
  int? orderType;
  int? newUserRefuse;
  int? showWekakaButton;
  List<FileDataEntity> fileData;
  int? deleted;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'icon': icon,
        'avatar': avatar,
        'balance': balance,
        'id': id,
        'rechargeAddress': rechargeAddress,
        'inviteCode': inviteCode,
        'referrerPhone': referrerPhone,
        'totalEarn': totalEarn,
        'todayEarn': todayEarn,
        'grade': grade,
        'nickname': nickname,
        'phone': phone,
        'activityLevel': activityLevel,
        'name': name,
        'sex': sex,
        'city_id': cityId,
        'is_single': isSingle,
        'work': work,
        'vip_overdue': vipOverdue,
        'breast': breast,
        'chest_circumference': chestCircumference,
        'label': label,
        'date_type': dateType,
        'pay_type': payType,
        'city_name': cityName,
        'qq': qq,
        'wx': wekaka,
        'province_id': provinceId,
        'province_name': provinceName,
        'account_in_hide': accountInHide,
        'in_check_msg': inCheckMsg,
        'data_in_check': dataInCheck,
        'data_completed': dataCompleted,
        'is_vip': isVip,
        'last_city_id': lastCityId,
        'img_num': imgNum,
        'video_num': videoNum,
        'last_log_time': lastLogTime,
        'peer_uid': peerUid,
        'distance': distance,
        'distance_str': distanceStr,
        'bought_times': boughtTimes,
        'last_log_time_type': lastLogTimeType,
        'is_confirmed': isConfirmed,
        'audio_duration': audioDuration,
        'show_audio': showAudio,
        'audio_url': audioUrl,
        'report_text': reportText,
        'show_album': showAlbum,
        'price': price,
        'order_type': orderType,
        'new_user_refuse': newUserRefuse,
        'deleted': deleted,
        'show_wx_button': showWekakaButton,
        'file_data': json.encode(fileData),
      };
}

class RegisterBaseData {
  RegisterBaseData({
    required this.dateTypeData,
    required this.maleWorkData,
    required this.femaleWorkData,
    required this.femaleLabelData,
    required this.maleLabelData,
    required this.isSingleData,
    required this.reportData,
    required this.broadcastThemeMale,
    required this.broadcastThemeFemale,
    required this.broadcastLabelMale,
    required this.broadcastLabelFemale,
  });

  factory RegisterBaseData.fromJson(Map<String, dynamic> json) {
    final List<DateTypeData>? dateTypeData =
        json['date_type_data'] is List ? <DateTypeData>[] : null;
    if (dateTypeData != null) {
      for (final dynamic item in json['date_type_data']!) {
        if (item != null) {
          tryCatch(() {
            dateTypeData
                .add(DateTypeData.fromJson(asT<Map<String, dynamic>>(item)!));
          });
        }
      }
    }

    final List<WorkDataModel>? maleWorkData =
        json['male_work_data'] is List ? <WorkDataModel>[] : null;
    if (maleWorkData != null) {
      for (final dynamic item in json['male_work_data']!) {
        if (item != null) {
          tryCatch(() {
            maleWorkData
                .add(WorkDataModel.fromJson(asT<Map<String, dynamic>>(item)!));
          });
        }
      }
    }

    final List<WorkDataModel>? femaleWorkData =
        json['female_work_data'] is List ? <WorkDataModel>[] : null;
    if (femaleWorkData != null) {
      for (final dynamic item in json['female_work_data']!) {
        if (item != null) {
          tryCatch(() {
            femaleWorkData
                .add(WorkDataModel.fromJson(asT<Map<String, dynamic>>(item)!));
          });
        }
      }
    }

    final List<LabelData>? femaleLabelData =
        json['female_label_data'] is List ? <LabelData>[] : null;
    if (femaleLabelData != null) {
      for (final dynamic item in json['female_label_data']!) {
        if (item != null) {
          tryCatch(() {
            femaleLabelData
                .add(LabelData.fromJson(asT<Map<String, dynamic>>(item)!));
          });
        }
      }
    }

    final List<LabelData>? maleLabelData =
        json['male_label_data'] is List ? <LabelData>[] : null;
    if (maleLabelData != null) {
      for (final dynamic item in json['male_label_data']!) {
        if (item != null) {
          tryCatch(() {
            maleLabelData
                .add(LabelData.fromJson(asT<Map<String, dynamic>>(item)!));
          });
        }
      }
    }

    final List<IsSingleData>? isSingleData =
        json['is_single_data'] is List ? <IsSingleData>[] : null;
    if (isSingleData != null) {
      for (final dynamic item in json['is_single_data']!) {
        if (item != null) {
          tryCatch(() {
            isSingleData
                .add(IsSingleData.fromJson(asT<Map<String, dynamic>>(item)!));
          });
        }
      }
    }

    final List<ReportData>? reportData =
        json['report_data'] is List ? <ReportData>[] : null;
    if (reportData != null) {
      for (final dynamic item in json['report_data']!) {
        if (item != null) {
          tryCatch(() {
            reportData
                .add(ReportData.fromJson(asT<Map<String, dynamic>>(item)!));
          });
        }
      }
    }

    final List<String>? broadcastThemeMale =
        json['broadcast_theme_male'] is List ? <String>[] : null;
    if (broadcastThemeMale != null) {
      for (final dynamic item in json['broadcast_theme_male']!) {
        if (item != null) {
          tryCatch(() {
            broadcastThemeMale.add(asT<String>(item)!);
          });
        }
      }
    }

    final List<String>? broadcastThemeFemale =
        json['broadcast_theme_female'] is List ? <String>[] : null;
    if (broadcastThemeFemale != null) {
      for (final dynamic item in json['broadcast_theme_female']!) {
        if (item != null) {
          tryCatch(() {
            broadcastThemeFemale.add(asT<String>(item)!);
          });
        }
      }
    }

    final List<String>? broadcastLabelMale =
        json['broadcast_label_male'] is List ? <String>[] : null;
    if (broadcastLabelMale != null) {
      for (final dynamic item in json['broadcast_label_male']!) {
        if (item != null) {
          tryCatch(() {
            broadcastLabelMale.add(asT<String>(item)!);
          });
        }
      }
    }

    final List<String>? broadcastLabelFemale =
        json['broadcast_label_female'] is List ? <String>[] : null;
    if (broadcastLabelFemale != null) {
      for (final dynamic item in json['broadcast_label_female']!) {
        if (item != null) {
          tryCatch(() {
            broadcastLabelFemale.add(asT<String>(item)!);
          });
        }
      }
    }
    return RegisterBaseData(
      dateTypeData: dateTypeData!,
      maleWorkData: maleWorkData!,
      femaleWorkData: femaleWorkData!,
      femaleLabelData: femaleLabelData!,
      maleLabelData: maleLabelData!,
      isSingleData: isSingleData!,
      reportData: reportData!,
      broadcastThemeMale: broadcastThemeMale!,
      broadcastThemeFemale: broadcastThemeFemale!,
      broadcastLabelMale: broadcastLabelMale!,
      broadcastLabelFemale: broadcastLabelFemale!,
    );
  }

  List<DateTypeData> dateTypeData;
  List<WorkDataModel> maleWorkData;
  List<WorkDataModel> femaleWorkData;
  List<LabelData> femaleLabelData;
  List<LabelData> maleLabelData;
  List<IsSingleData> isSingleData;
  List<ReportData> reportData;
  List<String> broadcastThemeMale;
  List<String> broadcastThemeFemale;
  List<String> broadcastLabelMale;
  List<String> broadcastLabelFemale;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'date_type_data': dateTypeData,
        'male_work_data': maleWorkData,
        'female_work_data': femaleWorkData,
        'female_label_data': femaleLabelData,
        'male_label_data': maleLabelData,
        'is_single_data': isSingleData,
        'report_data': reportData,
        'broadcast_theme_male': broadcastThemeMale,
        'broadcast_theme_female': broadcastThemeFemale,
        'broadcast_label_male': broadcastLabelMale,
        'broadcast_label_female': broadcastLabelFemale,
      };
}

class DateTypeData {
  DateTypeData({
    required this.typeId,
    required this.typeName,
  });

  factory DateTypeData.fromJson(Map<String, dynamic> json) => DateTypeData(
        typeId: asT<int>(json['type_id'])!,
        typeName: asT<String>(json['type_name'])!,
      );

  int typeId;
  String typeName;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'type_id': typeId,
        'type_name': typeName,
      };
}

class WorkDataModel {
  WorkDataModel({
    required this.typeId,
    required this.typeName,
  });

  factory WorkDataModel.fromJson(Map<String, dynamic> json) => WorkDataModel(
        typeId: asT<int>(json['type_id'])!,
        typeName: asT<String>(json['type_name'])!,
      );

  int typeId;
  String typeName;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'type_id': typeId,
        'type_name': typeName,
      };
}

class LabelData {
  LabelData({
    required this.typeId,
    required this.typeName,
  });

  factory LabelData.fromJson(Map<String, dynamic> json) => LabelData(
        typeId: asT<int>(json['type_id'])!,
        typeName: asT<String>(json['type_name'])!,
      );

  int typeId;
  String typeName;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'type_id': typeId,
        'type_name': typeName,
      };
}

class IsSingleData {
  IsSingleData({
    required this.typeId,
    required this.typeName,
  });

  factory IsSingleData.fromJson(Map<String, dynamic> json) => IsSingleData(
        typeId: asT<int>(json['type_id'])!,
        typeName: asT<String>(json['type_name'])!,
      );

  int typeId;
  String typeName;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'type_id': typeId,
        'type_name': typeName,
      };
}

class ReportData {
  ReportData({
    required this.typeId,
    required this.typeName,
  });

  factory ReportData.fromJson(Map<String, dynamic> json) => ReportData(
        typeId: asT<int>(json['type_id'])!,
        typeName: asT<String>(json['type_name'])!,
      );

  int typeId;
  String typeName;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'type_id': typeId,
        'type_name': typeName,
      };
}

class UserDetInfoRsp {
  UserDetInfoRsp({
    this.selfInfo,
  });

  factory UserDetInfoRsp.fromJson(Map<String, dynamic> json) => UserDetInfoRsp(
        selfInfo: json['self_info'] == null
            ? null
            : SelfInfoRspEntity.fromJson(
                asT<Map<String, dynamic>>(json['self_info'])!),
      );

  SelfInfoRspEntity? selfInfo;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'self_info': selfInfo,
      };
}

class SelfInfoRspEntity {
  SelfInfoRspEntity({
    this.uid,
    this.icon,
    this.nickname,
    this.sex,
    this.age,
    this.cityId,
    this.cityName,
    this.isSingle,
    this.work,
    this.vipOverdue,
    this.isConfirmed,
    this.depositNum,
    this.iconChangeNeed,
    this.coinNum,
    this.isVip,
    this.depositNumStr,
    this.bannerData,
    this.historyVisitor,
    this.selfImage,
    this.serviceWekaka,
    this.isRealAccount,
    this.serviceData,
    this.boughtTimes,
    this.phoneNumber,
    this.iconUploaded,
  });

  factory SelfInfoRspEntity.fromJson(Map<String, dynamic> json) {
    final List<BannerData>? bannerData =
        json['banner_data'] is List ? <BannerData>[] : null;
    if (bannerData != null) {
      for (final dynamic item in json['banner_data']!) {
        if (item != null) {
          tryCatch(() {
            bannerData
                .add(BannerData.fromJson(asT<Map<String, dynamic>>(item)!));
          });
        }
      }
    }

    final List<MyPhotoEntity>? selfImage =
        json['self_image'] is List ? <MyPhotoEntity>[] : null;
    if (selfImage != null) {
      for (final dynamic item in json['self_image']!) {
        if (item != null) {
          tryCatch(() {
            selfImage.add(MyPhotoEntity.fromJson(item));
          });
        }
      }
    }
    return SelfInfoRspEntity(
      uid: asT<String?>(json['uid']),
      icon: asT<String?>(json['icon']),
      nickname: asT<String?>(json['nickname']),
      sex: asT<int?>(json['sex']),
      age: asT<int?>(json['age']),
      cityId: asT<int?>(json['city_id']),
      cityName: asT<String?>(json['city_name']),
      isSingle: asT<Object?>(json['is_single']),
      work: asT<int?>(json['work']),
      vipOverdue: asT<String?>(json['vip_overdue']),
      isConfirmed: asT<int?>(json['is_confirmed']),
      depositNum: asT<int?>(json['deposit_num']),
      iconChangeNeed: asT<int?>(json['icon_change_need']),
      coinNum: asT<int?>(json['coin_num']),
      isVip: asT<int?>(json['is_vip']),
      depositNumStr: asT<String?>(json['deposit_num_str']),
      bannerData: bannerData,
      historyVisitor: json['history_visitor'] == null
          ? null
          : HistoryVisitor.fromJson(
              asT<Map<String, dynamic>>(json['history_visitor'])!),
      selfImage: selfImage,
      serviceWekaka: asT<String?>(json['service_wx']),
      isRealAccount: asT<int?>(json['is_real_account']),
      serviceData: json['service_data'] == null
          ? null
          : ServiceData.fromJson(
              asT<Map<String, dynamic>>(json['service_data'])!),
      boughtTimes: asT<int?>(json['bought_times']),
      phoneNumber: asT<String?>(json['phone_number']),
      iconUploaded: asT<int?>(json['icon_uploaded']),
    );
  }

  String? uid;
  String? icon;
  String? nickname;
  int? sex;
  int? age;
  int? cityId;
  String? cityName;
  Object? isSingle;
  int? work;
  String? vipOverdue;
  int? isConfirmed;
  int? depositNum;
  int? iconChangeNeed;
  int? coinNum;
  int? isVip;
  String? depositNumStr;
  List<BannerData>? bannerData;
  HistoryVisitor? historyVisitor;
  List<MyPhotoEntity>? selfImage;
  String? serviceWekaka;
  int? isRealAccount;
  ServiceData? serviceData;
  int? boughtTimes;
  String? phoneNumber;
  int? iconUploaded;

  String get sexStr {
    if (sex == 0) {
      return "男";
    } else {
      return "女";
    }
  }

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'icon': icon,
        'nickname': nickname,
        'sex': sex,
        'age': age,
        'city_id': cityId,
        'city_name': cityName,
        'is_single': isSingle,
        'work': work,
        'vip_overdue': vipOverdue,
        'is_confirmed': isConfirmed,
        'deposit_num': depositNum,
        'icon_change_need': iconChangeNeed,
        'coin_num': coinNum,
        'is_vip': isVip,
        'deposit_num_str': depositNumStr,
        'banner_data': bannerData,
        'history_visitor': historyVisitor,
        'self_image': json.encode(selfImage),
        'service_wx': serviceWekaka,
        'is_real_account': isRealAccount,
        'service_data': serviceData,
        'bought_times': boughtTimes,
        'phone_number': phoneNumber,
        'icon_uploaded': iconUploaded,
      };
}

class BannerData {
  BannerData({
    this.imgUrl,
    this.linkUrl,
    this.linkType,
    this.title,
  });

  factory BannerData.fromJson(Map<String, dynamic> json) => BannerData(
        imgUrl: asT<String?>(json['img_url']),
        linkUrl: asT<String?>(json['link_url']),
        linkType: asT<String?>(json['link_type']),
        title: asT<String?>(json['title']),
      );

  String? imgUrl;
  String? linkUrl;
  String? linkType;
  String? title;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'img_url': imgUrl,
        'link_url': linkUrl,
        'link_type': linkType,
        'title': title,
      };
}

class HistoryVisitor {
  HistoryVisitor({
    this.icon,
    this.visitorNum,
  });

  factory HistoryVisitor.fromJson(Map<String, dynamic> json) {
    final List<String>? icon = json['icon'] is List ? <String>[] : null;
    if (icon != null) {
      for (final dynamic item in json['icon']!) {
        if (item != null) {
          tryCatch(() {
            icon.add(asT<String>(item)!);
          });
        }
      }
    }
    return HistoryVisitor(
      icon: icon,
      visitorNum: asT<String?>(json['visitor_num']),
    );
  }

  List<String>? icon;
  String? visitorNum;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'icon': icon,
        'visitor_num': visitorNum,
      };
}

class ServiceData {
  ServiceData({
    this.show,
    this.name,
    this.uid,
    this.message,
  });

  factory ServiceData.fromJson(Map<String, dynamic> json) => ServiceData(
        show: asT<int?>(json['show']),
        name: asT<String?>(json['name']),
        uid: asT<String?>(json['uid']),
        message: asT<String?>(json['message']),
      );

  int? show;
  String? name;
  String? uid;
  String? message;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'show': show,
        'name': name,
        'uid': uid,
        'message': message,
      };
}

class AudioRspEntity {
  AudioRspEntity({
    this.fileUrl,
    this.audioDuration,
    this.fileStatus,
  });

  factory AudioRspEntity.fromJson(Map<String, dynamic> json) => AudioRspEntity(
        fileUrl: asT<String?>(json['file_url']),
        audioDuration: asT<int?>(json['audio_duration']),
        fileStatus: asT<int?>(json['file_status']),
      );

  String? fileUrl;
  int? audioDuration;
  int? fileStatus;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'file_url': fileUrl,
        'audio_duration': audioDuration,
        'file_status': fileStatus,
      };
}

class NotificationRspEntity {
  NotificationRspEntity({
    this.msgPrivate,
    this.msgSameCity,
    this.msgSystem,
    this.msgQianbaozaie,
    this.msgPurse,
    this.msgStranger,
    this.msgSignUp,
  });

  factory NotificationRspEntity.fromJson(Map<String, dynamic> json) =>
      NotificationRspEntity(
        msgPrivate: (asT<int?>(json['msg_private']) ?? 0).obs,
        msgSameCity: (asT<int?>(json['msg_same_city']) ?? 0).obs,
        msgSystem: (asT<int?>(json['msg_system']) ?? 0).obs,
        msgQianbaozaie: (asT<int?>(json['msg_wl']) ?? 0).obs,
        msgPurse: (asT<int?>(json['msg_purse']) ?? 0).obs,
        msgStranger: (asT<int?>(json['msg_stranger']) ?? 0).obs,
        msgSignUp: (asT<int?>(json['msg_sign_up']) ?? 0).obs,
      );

  RxInt? msgPrivate;
  RxInt? msgSameCity;
  RxInt? msgSystem;
  RxInt? msgQianbaozaie;
  RxInt? msgPurse;
  RxInt? msgStranger;
  RxInt? msgSignUp;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'msg_private': msgPrivate?.value ?? 0,
        'msg_same_city': msgSameCity?.value ?? 0,
        'msg_system': msgSystem?.value ?? 0,
        'msg_wl': msgQianbaozaie?.value ?? 0,
        'msg_purse': msgPurse?.value ?? 0,
        'msg_stranger': msgStranger?.value ?? 0,
        'msg_sign_up': msgSignUp?.value ?? 0,
      };
}

class QianbaozaieRspEntity {
  QianbaozaieRspEntity({
    this.comoLlegarAccount,
    this.comoLlegarName,
    this.depositNum,
    this.realDepositNum,
    this.orderData,
    this.paging,
    this.explainMsg,
    this.yaoqingrenFetchData,
  });

  factory QianbaozaieRspEntity.fromJson(Map<String, dynamic> json) {
    final List<OrderData>? orderData =
        json['order_data'] is List ? <OrderData>[] : null;
    if (orderData != null) {
      for (final dynamic item in json['order_data']!) {
        if (item != null) {
          tryCatch(() {
            orderData.add(OrderData.fromJson(asT<Map<String, dynamic>>(item)!));
          });
        }
      }
    }
    return QianbaozaieRspEntity(
      comoLlegarAccount: asT<String?>(json['al_account']),
      comoLlegarName: asT<String?>(json['al_name']),
      depositNum: asT<String?>(json['deposit_num']),
      realDepositNum: asT<String?>(json['real_deposit_num']),
      orderData: orderData,
      paging: json['paging'] == null
          ? null
          : Paging.fromJson(asT<Map<String, dynamic>>(json['paging'])!),
      explainMsg: asT<String?>(json['explain_msg']),
      yaoqingrenFetchData: json['iv_fetch_data'] == null
          ? null
          : YaoqingrenFetchData.fromJson(json['iv_fetch_data']),
    );
  }

  String? comoLlegarAccount;
  String? comoLlegarName;
  String? depositNum;
  String? realDepositNum;
  List<OrderData>? orderData;
  Paging? paging;
  String? explainMsg;
  YaoqingrenFetchData? yaoqingrenFetchData;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'al_account': comoLlegarAccount,
        'al_name': comoLlegarName,
        'deposit_num': depositNum,
        'real_deposit_num': realDepositNum,
        'order_data': orderData,
        'paging': paging,
        'explain_msg': explainMsg,
        'iv_fetch_data': json.encode(yaoqingrenFetchData ?? ""),
      };
}

class YaoqingrenFetchData {
  YaoqingrenFetchData({
    this.fetchNum,
    this.fetchTotal,
  });

  factory YaoqingrenFetchData.fromJson(Map<String, dynamic> json) =>
      YaoqingrenFetchData(
        fetchNum: asT<int?>(json['fetch_num']),
        fetchTotal: asT<String?>(json['fetch_total']),
      );

  int? fetchNum;
  String? fetchTotal;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'fetch_num': fetchNum,
        'fetch_total': fetchTotal,
      };
}

class OrderData {
  OrderData({
    this.icon,
    this.nickname,
    this.sex,
    this.action,
    this.amount,
    this.createTime,
    this.peerUid,
    this.isIncrease,
  });

  factory OrderData.fromJson(Map<String, dynamic> json) => OrderData(
        icon: asT<String?>(json['icon']),
        nickname: asT<String?>(json['nickname']),
        sex: asT<int?>(json['sex']),
        action: asT<String?>(json['action']),
        amount: asT<String?>(json['amount']),
        peerUid: asT<String?>(json['peer_uid']),
        createTime: asT<String?>(json['create_time']),
        isIncrease: asT<int?>(json['is_increase']),
      );

  String? icon;
  String? nickname;
  int? sex;
  String? action;
  String? amount;
  String? createTime;
  String? peerUid;
  int? isIncrease;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'icon': icon,
        'nickname': nickname,
        'sex': sex,
        'action': action,
        'amount': amount,
        'create_time': createTime,
        'peer_uid': peerUid,
        'is_increase': isIncrease,
      };
}

class Paging {
  Paging({
    this.limit,
    this.offset,
    this.hasMore,
  });

  factory Paging.fromJson(Map<String, dynamic> json) => Paging(
        limit: asT<int?>(json['limit']),
        offset: asT<int?>(json['offset']),
        hasMore: asT<int?>(json['has_more']),
      );

  int? limit;
  int? offset;
  int? hasMore;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'limit': limit,
        'offset': offset,
        'has_more': hasMore,
      };
}

class VideoRspEntity {
  VideoRspEntity({
    this.price,
    this.fileUrl,
    this.readNum,
    this.fileStatus,
  });

  factory VideoRspEntity.fromJson(Map<String, dynamic> json) => VideoRspEntity(
        price: asT<int?>(json['price']),
        fileUrl: asT<String?>(json['file_url']),
        readNum: asT<int?>(json['read_num']),
        fileStatus: asT<int?>(json['file_status']),
      );

  int? price;
  String? fileUrl;

  String get fileCover {
    return (fileUrl ?? "") + "?x-oss-process=video/snapshot,t_0,m_fast,f_jpg";
  }

  int? readNum;
  int? fileStatus;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'price': price,
        'file_url': fileUrl,
        'read_num': readNum,
        'file_status': fileStatus,
      };
}

class MyPhotoEntity {
  MyPhotoEntity({
    this.isSnapChat,
    this.imgUrl,
    this.fileStatus,
    this.isSelf,
    this.isConfirmFile,
  });

  factory MyPhotoEntity.fromJson(Map<String, dynamic> json) => MyPhotoEntity(
        isSnapChat: asT<int?>(json['is_snap_chat']),
        imgUrl: asT<String?>(json['img_url']),
        fileStatus: asT<int?>(json['file_status']),
        isSelf: asT<int?>(json['is_self']),
        isConfirmFile: asT<int?>(json['is_confirm_file']),
      );

  int? isSnapChat;
  String? imgUrl;
  int? fileStatus;
  int? isSelf;
  int? isConfirmFile;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'is_snap_chat': isSnapChat,
        'img_url': imgUrl,
        'file_status': fileStatus,
        'is_self': isSelf,
        'is_confirm_file': isConfirmFile,
      };
}

class BlackListEntity {
  BlackListEntity({
    this.uid,
    this.icon,
    this.nickname,
  });

  factory BlackListEntity.fromJson(Map<String, dynamic> json) =>
      BlackListEntity(
        uid: asT<String?>(json['uid']),
        icon: asT<String?>(json['icon']),
        nickname: asT<String?>(json['nickname']),
      );

  String? uid;
  String? icon;
  String? nickname;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'uid': uid,
        'icon': icon,
        'nickname': nickname,
      };
}

class WhoLookMeEntity {
  WhoLookMeEntity({
    required this.uid,
    required this.createTime,
    required this.icon,
    required this.nickname,
    required this.isConfirmed,
    required this.isVip,
    required this.sex,
  });

  factory WhoLookMeEntity.fromJson(Map<String, dynamic> json) =>
      WhoLookMeEntity(
        uid: asT<String>(json['uid'])!,
        createTime: asT<String>(json['create_time'])!,
        icon: asT<String>(json['icon'])!,
        nickname: asT<String>(json['nickname'])!,
        isConfirmed: asT<int>(json['is_confirmed'])!,
        isVip: asT<int>(json['is_vip'])!,
        sex: asT<int>(json['sex'])!,
      );

  String uid;
  String createTime;
  String icon;
  String nickname;
  int isConfirmed;
  int isVip;
  int sex;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'uid': uid,
        'create_time': createTime,
        'icon': icon,
        'nickname': nickname,
        'is_confirmed': isConfirmed,
        'is_vip': isVip,
        'sex': sex,
      };

  WhoLookMeEntity copy() {
    return WhoLookMeEntity(
      uid: uid,
      createTime: createTime,
      icon: icon,
      nickname: nickname,
      isConfirmed: isConfirmed,
      isVip: isVip,
      sex: sex,
    );
  }
}
