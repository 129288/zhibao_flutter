import 'package:zhibao_flutter/util/data/api_util.dart';
import 'package:zhibao_flutter/util/data/q1_data.dart';
import 'package:zhibao_flutter/util/http/base_request.dart';

/*
* 获取个人信息
*
* @param test string 演示参数介绍
* 
* */
class GetSelfInfoRequestModel extends BaseRequest {
  final String token;

  GetSelfInfoRequestModel(this.token);

  @override
  Map<String, dynamic> toJson() {
    return {
      'token': token,
    };
  }

  @override
  String url() => 'user/info';
}

/*
* 设置个人信息
*
* @param test string 演示参数介绍
*
* */
class SetSelfInfoRequestModel extends BaseRequest {
  final String? username;

  @override
  Map<String, dynamic> toJson() {
    return {
      'username': this.username,
    };
  }

  SetSelfInfoRequestModel(
      this.username,);

  @override
  String url() => 'user/updateUsername';
}

/*
* “我的”主页
*
* @param test string 演示参数介绍
*
* */
class UserIndexRequestModel extends BaseRequest {
  @override
  String url() => 'api/v2/user/user_index';
}

/*
* 用户动作（点赞/报名广播，点赞个人信息）
*
* @param test string 演示参数介绍
*
* */
class AddActionRequestModel extends BaseRequest {
  final int action_type;
  final String related_id;

  @override
  Map<String, dynamic> toJson() {
    return {
      'action_type': this.action_type,
      'related_id': this.related_id,
    };
  }

  AddActionRequestModel(this.action_type, this.related_id);

  @override
  String url() => 'api/v2/user/add_action';
}

/*
* 获取“完善信息”相关数据
*
* @param test string 演示参数介绍
*
* */
class GetRegisterDataRequestModel extends BaseRequest {
  @override
  String url() => 'api/v2/user/get_register_data';
}

/*
* “我的”主页，实时数据
*
* @param test string 演示参数介绍
*
* */
class TimelinessDataRequestModel extends BaseRequest {
  @override
  String url() => 'api/v2/user/timeliness_data';
}

/*
* 我的相册
*
* @param test string 演示参数介绍
*
* */
class UserImgRequestModel extends BaseRequest {
  final int? offset;
  final int? limit;

  // 查询类型，1全部2普通图片3阅后即焚，默认全部
  final int? get_type;

  @override
  Map<String, dynamic> toJson() {
    return {
      'offset': ApiUtil.getOffset(this.offset, this.limit),
      'limit': this.limit,
      'get_type': this.get_type,
    };
  }

  UserImgRequestModel(this.offset, this.limit, this.get_type);

  @override
  String url() => 'api/v2/user/user_img';
}

/*
* 我的视频
*
* @param test string 演示参数介绍
*
* */
class UserVideoRequestModel extends BaseRequest {
  final int offset;
  final int limit;

  @override
  Map<String, dynamic> toJson() {
    return {
      'offset': ApiUtil.getOffset(this.offset, this.limit),
      'limit': this.limit,
    };
  }

  UserVideoRequestModel(this.offset, this.limit);

  @override
  String url() => 'api/v2/user/user_video';
}

/*
* 获取设置昵称与联系方式的权限
*
* @param test string 演示参数介绍
*
* */
class GetSetPermissionNicknameContactRequestModel extends BaseRequest {
  @override
  String url() => 'api/v2/user/get_set_permission_nickname_contact';
}

/*
* 常见问题
*
* @param test string 演示参数介绍
*
* */
class GetQuestionsAndAnswersRequestModel extends BaseRequest {
  @override
  String url() => 'api/v2/user/get_questions_and_answers';
}

/*
* 我的浅表
*
* @param test string 演示参数介绍
*
* */
class GetQianbaozaieInfoRequestModel extends BaseRequest {
  final int offset;
  final int limit;
  final int get_type;

  @override
  Map<String, dynamic> toJson() {
    return {
      'offset': ApiUtil.getOffset(this.offset, this.limit),
      'limit': this.limit,
      'get_type': this.get_type,
    };
  }

  GetQianbaozaieInfoRequestModel(this.offset, this.limit, this.get_type);

  @override
  String url() => 'api/v2/user/get_wl_info';
}

/*
* 设置猫表账号
*
* @param test string 演示参数介绍
*
* */
class SetComoLlegarAccountRequestModel extends BaseRequest {
  final String comoLlegar_account;
  final String comoLlegar_name;

  @override
  Map<String, dynamic> toJson() {
    return {
      'al_account': this.comoLlegar_account,
      'al_name': this.comoLlegar_name,
    };
  }

  SetComoLlegarAccountRequestModel(
      this.comoLlegar_account, this.comoLlegar_name);

  @override
  String url() => 'api/v2/user/set_al_account';
}

/*
* 恢复“阅后即焚”照片
*
* @param test string 演示参数介绍
*
* */
class RecoverSnapImgRequestModel extends BaseRequest {
  @override
  String url() => 'api/v2/homepage/recover_snap_img';
}

/*
* 接收红包
*
* @param test string 演示参数介绍
*
* */
class ReceiveRedBagRequestModel extends BaseRequest {
  final String order_id;

  @override
  Map<String, dynamic> toJson() {
    return {
      'order_id': this.order_id,
    };
  }

  ReceiveRedBagRequestModel(this.order_id);

  @override
  String url() => 'api/v2/user/receive_red_bag';
}

/*
* 红包详情
*
* @param test string 演示参数介绍
*
* */
class RedBagDetailRequestModel extends BaseRequest {
  final String order_id;

  @override
  Map<String, dynamic> toJson() {
    return {
      'order_id': this.order_id,
    };
  }

  RedBagDetailRequestModel(this.order_id);

  @override
  String url() => 'api/v2/user/red_bag_detail';
}

/*
* 申请提苗
*
* @param test string 演示参数介绍
*
* */
class FetchDepositRequestModel extends BaseRequest {
  final String amount;

  FetchDepositRequestModel(this.amount);

  @override
  String url() => 'api/v2/user/fetch_deposit';
}

/*
* 获取黑名单列表
*
* @param test string 演示参数介绍
*
* */
class ShowBlacklistRequestModel extends BaseRequest {
  @override
  String url() => 'api/v2/user/show_blacklist';
}

/*
* 移除黑名单
*
* @param test string 演示参数介绍
*
* */
class RemoveBlacklistRequestModel extends BaseRequest {
  final String target_uid;

  @override
  Map<String, dynamic> toJson() {
    return {
      'target_uid': this.target_uid,
    };
  }

  RemoveBlacklistRequestModel(this.target_uid);

  @override
  String url() => 'api/v2/user/remove_blacklist';
}

/*
* 添加黑名单
*
* @param test string 演示参数介绍
*
* */
class AddBlacklistRequestModel extends BaseRequest {
  final String target_uid;

  @override
  Map<String, dynamic> toJson() {
    return {
      'target_uid': this.target_uid,
    };
  }

  AddBlacklistRequestModel(this.target_uid);

  @override
  String url() => 'api/v2/user/add_blacklist';
}

/*
* 女性认证
*
* @param test string 演示参数介绍
*
* */
class ConfirmRealityRequestModel extends BaseRequest {
  final String img_url;

  ConfirmRealityRequestModel(this.img_url);

  @override
  String url() => 'api/v2/user/confirm_reality';
}

/*
* 获取认证权限
*
* @param test string 演示参数介绍
*
* */
class GetConfirmRealityPermissionRequestModel extends BaseRequest {
  @override
  String url() => 'api/v2/user/get_confirm_reality_permission';
}

/*
* 检查昵称是否已存在
*
* @param test string 演示参数介绍
*
* */
class CheckNicknameExistRequestModel extends BaseRequest {
  final String nickname;

  @override
  Map<String, dynamic> toJson() {
    return {
      'nickname': this.nickname,
    };
  }

  CheckNicknameExistRequestModel(this.nickname);

  @override
  String url() => 'api/v2/user/check_nickname_exist';
}

/*
* 다음에 초대됩니다好友记录
*
* @param test string 演示参数介绍
*
* */
class YaoqingrenUserLogRequestModel extends BaseRequest {
  // 请求类型，1다음에 초대됩니다注册2다음에 초대됩니다获得收有3会员增加，默认1
  final int get_type;

  @override
  Map<String, dynamic> toJson() {
    return {
      'get_type': this.get_type,
    };
  }

  YaoqingrenUserLogRequestModel(this.get_type);

  @override
  String url() => 'api/v2/user/iv_user_log';
}

/*
* 分享다음에 초대됩니다图
*
* @param test string 演示参数介绍
*
* */
class YaoqingrenUserShachaImgRequestModel extends BaseRequest {
  @override
  String url() => 'api/v2/user/iv_user_shacha_img';
}

/*
* 确认虚拟币订单
*
* @param test string 演示参数介绍
*
* */
class CheckCoinOrderRequestModel extends BaseRequest {
  final int order_type;
  final String file_url;

  @override
  Map<String, dynamic> toJson() {
    return {
      'order_type': this.order_type,
      'file_url': this.file_url,
    };
  }

  CheckCoinOrderRequestModel(this.order_type, this.file_url);

  @override
  String url() => 'api/v2/user/check_coin_order';
}

/*
* 虚拟币订单下单
*
* @param test string 演示参数介绍
*
* */
class MakeCoinOrderRequestModel extends BaseRequest {
  final String peer_uid;
  final int order_type;
  final String file_url;

  @override
  Map<String, dynamic> toJson() {
    return {
      'peer_uid': this.peer_uid,
      'order_type': this.order_type,
      'file_url': this.file_url,
    };
  }

  MakeCoinOrderRequestModel(this.peer_uid, this.order_type, this.file_url);

  @override
  String url() => 'api/v2/user/make_coin_order';
}

/*
* 获取访客记录
*
* @param test string 演示参数介绍
*
* */
class VisitLogRequestModel extends BaseRequest {
  final int offset;
  final int limit;

  @override
  Map<String, dynamic> toJson() {
    return {
      'offset': ApiUtil.getOffset(this.offset, this.limit),
      'limit': this.limit,
    };
  }

  VisitLogRequestModel(this.offset, this.limit);

  @override
  String url() => 'api/v2/user/visit_log';
}

/*
* 虚拟币提苗
*
* @param test string 演示参数介绍
*
* */
class CoinFetchDepositRequestModel extends BaseRequest {
  final int amount;

  @override
  Map<String, dynamic> toJson() {
    return {
      'amount': this.amount,
    };
  }

  CoinFetchDepositRequestModel(this.amount);

  @override
  String url() => 'api/v2/user/coin_fetch_deposit';
}

/*
* 获取用户设备权限记录
*
* @param test string 演示参数介绍
*
* */
class AddDevicePermissionRequestModel extends BaseRequest {
  final int permission_type;
  final int result;

  @override
  Map<String, dynamic> toJson() {
    return {
      'permission_type': this.permission_type,
      'result': this.result,
    };
  }

  AddDevicePermissionRequestModel(this.permission_type, this.result);

  @override
  String url() => 'api/v2/user/add_device_permission';
}

/*
* 获取隐私设置
*
* @param test string 演示参数介绍
*
* */
class GetPrivacySetRequestModel extends BaseRequest {
  @override
  String url() => 'api/v2/user/get_privacy_set';
}

/*
* 设置隐私设置
*
* @param test string 演示参数介绍
*
* */
class SetPrivacyRequestModel extends BaseRequest {
  final String privacy_type;
  final int privacy_value;

  @override
  Map<String, dynamic> toJson() {
    return {
      'privacy_type': this.privacy_type,
      'privacy_value': this.privacy_value,
    };
  }

  SetPrivacyRequestModel(this.privacy_type, this.privacy_value);

  @override
  String url() => 'api/v2/user/set_privacy';
}

/*
* 我的语音
*
* @param test string 演示参数介绍
*
* */
class UserAudioRequestModel extends BaseRequest {
  @override
  String url() => 'api/v2/user/user_audio';
}

/*
* 检查联系方式合理性
*
* @param test string 演示参数介绍
*
* */
class CheckContactLegalRequestModel extends BaseRequest {
  final String? wekaka;
  final String? qq;

  @override
  Map<String, dynamic> toJson() {
    return {
      'wx': this.wekaka,
      'qq': this.qq,
    };
  }

  CheckContactLegalRequestModel(this.wekaka, this.qq);

  @override
  String url() => 'api/v2/user/check_contact_legal';
}

/*
* 女性登录认证
*
* @param test string 演示参数介绍
*
* */
class ConfirmRealityAfterRequestModel extends BaseRequest {
  final String img_url;

  @override
  Map<String, dynamic> toJson() {
    return {
      'img_url': this.img_url,
    };
  }

  ConfirmRealityAfterRequestModel(this.img_url);

  @override
  String url() => 'api/v2/user/confirm_reality_after';
}

/*
* 获取推送开关
*
* @param test string 演示参数介绍
*
* */
class GetPushSwitchRequestModel extends BaseRequest {
  @override
  String url() => 'api/v2/user/get_push_switch';
}

/*
* 设置推送开关
*
* @param test string 演示参数介绍
*
* */
class SetPushSwitchRequestModel extends BaseRequest {
  final String switch_type;
  final int switch_value;

  @override
  Map<String, dynamic> toJson() {
    return {
      'switch_type': this.switch_type,
      'switch_value': this.switch_value,
    };
  }

  SetPushSwitchRequestModel(this.switch_type, this.switch_value);

  @override
  String url() => 'api/v2/user/set_push_switch';
}

/*
* 잔액은 여기에 있습니다订单下单
*
* @param test string 演示参数介绍
*
* */
class MakeDepositOrderRequestModel extends BaseRequest {
  final String peer_uid;
  final String order_type;

  @override
  Map<String, dynamic> toJson() {
    return {
      'peer_uid': this.peer_uid,
      'order_type': this.order_type,
    };
  }

  MakeDepositOrderRequestModel(this.peer_uid, this.order_type);

  @override
  String url() => 'api/v2/user/make_deposit_order';
}

/*
* 确认잔액은 여기에 있습니다订单
*
* @param test string 演示参数介绍
*
* */
class CheckDepositOrderRequestModel extends BaseRequest {
  final String order_type;

  @override
  Map<String, dynamic> toJson() {
    return {
      'order_type': this.order_type,
    };
  }

  CheckDepositOrderRequestModel(this.order_type);

  @override
  String url() => 'api/v2/user/check_deposit_order';
}

/*
* 新版女性认证
*
* @param test string 演示参数介绍
*
* */
class ConfirmRealityCountRequestModel extends BaseRequest {
  final String img_url;

  @override
  Map<String, dynamic> toJson() {
    return {
      'img_url': this.img_url,
    };
  }

  ConfirmRealityCountRequestModel(this.img_url);

  @override
  String url() => 'api/v2/user/confirm_reality_count';
}

/*
* 新版女性后续认证
*
* @param test string 演示参数介绍
*
* */
class ConfirmRealityAgainRequestModel extends BaseRequest {
  final List<String> img_urls;

  @override
  Map<String, dynamic> toJson() {
    return {
      'img_urls': this.img_urls,
    };
  }

  ConfirmRealityAgainRequestModel(this.img_urls);

  @override
  String url() => 'api/v2/user/confirm_reality_again';
}

/*
* 同城动态记录
*
* @param test string 演示参数介绍
*
* */
class JoinLogRequestModel extends BaseRequest {
  final int get_type;
  final int offset;
  final int limit;

  @override
  Map<String, dynamic> toJson() {
    return {
      'get_type': this.get_type,
      'offset': ApiUtil.getOffset(this.offset, this.limit),
      'limit': this.limit,
    };
  }

  JoinLogRequestModel(this.get_type, this.offset, this.limit);

  @override
  String url() => 'api/v2/homepage/user/join_log';
}

/*
* 获取易盾活体识别图片
*
* @param test string 演示参数介绍
* 
* */
class GetDunConfirmImgRequestModel extends BaseRequest {
  final String dun_token;

  @override
  Map<String, dynamic> toJson() {
    return {
      'dun_token': this.dun_token,
    };
  }

  GetDunConfirmImgRequestModel(this.dun_token);

  @override
  String url() => 'api/v2/user/get_dun_confirm_img';
}
