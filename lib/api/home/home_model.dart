import 'package:zhibao_flutter/util/http/base_request.dart';

class NoticeTextRequestModel extends BaseRequest {
  @override
  String url() => 'notice/text';
}

class AdListRequestModel extends BaseRequest {
  @override
  String url() => 'adpic/adList';
}
