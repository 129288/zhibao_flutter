
import 'dart:convert';

import '../commom/common_entity.dart';

class HomeBannerEntity {
  HomeBannerEntity({
    this.name,
    this.picUrl,
  });

  factory HomeBannerEntity.fromJson(Map<String, dynamic> json) => HomeBannerEntity(
    name: asT<String?>(json['name']),
    picUrl: asT<String?>(json['picUrl']),
  );

  String? name;
  String? picUrl;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
    'name': name,
    'picUrl': picUrl,
  };

  HomeBannerEntity copy() {
    return HomeBannerEntity(
      name: name,
      picUrl: picUrl,
    );
  }
}