import 'package:flutter/material.dart';
import 'package:zhibao_flutter/api/home/home_entity.dart';
import 'package:zhibao_flutter/api/home/home_model.dart';
import 'package:zhibao_flutter/util/http/http.dart';

HomeViewModel homeViewModel = HomeViewModel();

class HomeViewModel extends BaseViewModel {
  Future<ResponseModel> noticeText(BuildContext? context) async {
    return NoticeTextRequestModel()
        .sendApiAction(
      context,
      reqType: ReqType.get,
    )
        .then((rep) {
      return ResponseModel.fromSuccess(rep);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  Future<ResponseModel> getAdList(
    BuildContext? context,
    OnData? onStore,
  ) async {
    return AdListRequestModel()
        .sendApiAction(
      context,
      reqType: ReqType.get,
      onStore: onStore != null
          ? (v) {
              onStore(List.from(v ?? [])
                  .map<HomeBannerEntity>((e) => HomeBannerEntity.fromJson(e))
                  .toList());
            }
          : null, // Pass the function here
    )
        .then((rep) {
      return ResponseModel.fromSuccess(List.from(rep ?? [])
          .map<HomeBannerEntity>((e) => HomeBannerEntity.fromJson(e))
          .toList());
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }
}
