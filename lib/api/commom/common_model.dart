import 'dart:io';

import 'package:zhibao_flutter/util/http/http.dart';
/*
* 获取城市名称及id
*
* @param test string 演示参数介绍
*
* */
class CityListRequestModel extends BaseRequest {
  @override
  String url() => 'api/v2/common/city_list';
}

/*
* 检查是否为会员/已确认
*
* @param test string 演示参数介绍
*
* */
class ConfirmVipRequestModel extends BaseRequest {
  final int check_type;

  @override
  Map<String, dynamic> toJson() {
    return {
      'check_type': this.check_type,
    };
  }

  ConfirmVipRequestModel(this.check_type);

  @override
  String url() => 'api/v2/common/confirm_vip';
}

/*
* 获取敏感信息开关
*
* @param test string 演示参数介绍
*
* */
class GetShowPrivacyRequestModel extends BaseRequest {
  @override
  String url() => 'api/v2/common/get_show_privacy';
}

/*
* 删除图片/视频
*
* @param test string 演示参数介绍
*
* */
class PictureDeleteRequestModel extends BaseRequest {
  final String file_url;

  @override
  Map<String, dynamic> toJson() {
    return {
      'file_url': this.file_url,
    };
  }

  PictureDeleteRequestModel(this.file_url);

  @override
  String url() => 'api/v2/common/delete';
}

/*
* 获取会员价格表
*
* @param test string 演示参数介绍
*
* */
class GetGoodsListVipRequestModel extends BaseRequest {
  @override
  String url() => 'api/v2/common/get_goods_list_vip';
}

/*
* 获取系统时间
*
* @param test string 演示参数介绍
*
* */
class GetDatetimeRequestModel extends BaseRequest {
  @override
  String url() => 'api/v2/common/get_datetime';
}

/*
* 检查是否需要更新
*
* @param test string 演示参数介绍
*
* */
// /dict/getUpgrade/upgrade_param  升级的url
class CheckUpdateRequestModel extends BaseRequest {
  @override
  String url() => 'dict/getUpgrade/upgrade_param';
}

/*
* 首次激活app
*
* @param test string 演示参数介绍
*
* */
class AppActiveRequestModel extends BaseRequest {
  final String idfa;
  final String openudid;

  @override
  Map<String, dynamic> toJson() {
    return {
      'idfa': this.idfa,
      'openudid': this.openudid,
    };
  }

  AppActiveRequestModel(this.idfa, this.openudid);

  @override
  String url() => 'api/v2/common/app_active';
}

/*
* 获取女王榜上榜商品价格及虚拟币잔액은 여기에 있습니다
*
* @param test string 演示参数介绍
*
* */
class GetGoodsListRankingRequestModel extends BaseRequest {
  @override
  String url() => 'api/v2/common/get_goods_list_ranking';
}

/*
* 获取虚拟币充值商品列表
*
* @param test string 演示参数介绍
*
* */
class GetGoodsListCoinRequestModel extends BaseRequest {
  @override
  String url() => 'api/v2/common/get_goods_list_coin';
}

/*
* url上传图片/视频
*
* @param test string 演示参数介绍
*
* */
class UploadUrlRequestModel extends BaseRequest {
  final String file_url;
  // 1相册2视频3认证4头像5语音6动态7举报8新认证
  final int type;
  final int description;
  final int to_album;

  @override
  Map<String, dynamic> toJson() {
    return {
      'file_url': this.file_url,
      'type': this.type,
      'description': this.description,
      'to_album': this.to_album,
    };
  }

  UploadUrlRequestModel(
      this.file_url, this.type, this.description, this.to_album);

  @override
  String url() => 'api/v2/common/upload_url';
}

/*
* 获取加次包商品列表
*
* @param test string 演示参数介绍
*
* */
class GetGoodsListTimesRequestModel extends BaseRequest {
  @override
  String url() => 'api/v2/common/get_goods_list_times';
}

/*
* 获取应用内客服数据
*
* @param test string 演示参数介绍
*
* */
class GetServiceDataRequestModel extends BaseRequest {
  @override
  String url() => 'api/v2/common/get_service_data';
}

/*
* 经纬度获取城市id
*
* @param test string 演示参数介绍
*
* */
class GetCityIdLocationRequestModel extends BaseRequest {
  final double lon;
  final double lat;

  GetCityIdLocationRequestModel(this.lon, this.lat);

  @override
  Map<String, dynamic> toJson() {
    return {
      'lon': this.lon,
      'lat': this.lat,
    };
  }

  @override
  String url() => 'api/v2/common/get_city_id_location';
}

/*
* web获取上传文件key
*
* @param test string 演示参数介绍
*
* */
class GetWebUploadKeyRequestModel extends BaseRequest {
  final String file_url;

  @override
  Map<String, dynamic> toJson() {
    return {
      'file_url': this.file_url,
    };
  }

  GetWebUploadKeyRequestModel(this.file_url);

  @override
  String url() => 'api/v2/common/get_web_upload_key';
}

/*
* 发送消息检测文案
* 检测消息是否违规
*
* @param test string 演示参数介绍
* 
* */
class StrCheckWordRequestModel extends BaseRequest {
  final String check_word;

  @override
  Map<String, dynamic> toJson() {
    return {
      'check_word': this.check_word,
    };
  }

  StrCheckWordRequestModel(this.check_word);

  @override
  String url() => 'api/v2/common/str_check_word';
}
/*
* demo
*
* @param test string 演示参数介绍
* 
* */
class UploadAvatarRequestModel extends BaseRequest {
  final File file;

  @override
  Map<String, dynamic> toJson() {
    return {
      'file': file,
    };
  }


  UploadAvatarRequestModel(this.file);

  @override
  String url() => 'user/uploadAvatar';
}