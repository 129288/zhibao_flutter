// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'common_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AreaCode _$AreaCodeFromJson(Map<String, dynamic> json) => AreaCode(
      json['english_name'] as String,
      json['chinese_name'] as String,
      json['phone_code'] as String,
    );

Map<String, dynamic> _$AreaCodeToJson(AreaCode instance) => <String, dynamic>{
      'english_name': instance.englishName,
      'chinese_name': instance.chineseName,
      'phone_code': instance.phoneCode,
    };
