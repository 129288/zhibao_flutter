import 'package:zhibao_flutter/util/data/page_mini.dart';
import 'package:zhibao_flutter/util/http/http.dart';

/*
* demo
*
* @param test string 演示参数介绍
*
* */
class WalletLogRequestModel extends BaseRequest {
  final int? page;
  final int? type;

  @override
  Map<String, dynamic> toJson() {
    return {
      'page': this.page,
      'pageSize': PageMini.perPage,
      'type': this.type,
    };
  }

  WalletLogRequestModel(this.page, this.type);

  @override
  String url() => 'wallet/log';
}

/*
* demo
*
* @param test string 演示参数介绍
*
* */
class WalletWithdrawApplyRequestModel extends BaseRequest {
  final double amount;
  final String address;
  final String sms;

  @override
  Map<String, dynamic> toJson() {
    return {
      'amount': this.amount,
      'address': this.address,
      'sms': this.sms,
    };
  }

  WalletWithdrawApplyRequestModel(this.amount, this.address, this.sms);

  @override
  String url() => 'wallet/withdrawApply';
}

/*
* demo
*
* @param test string 演示参数介绍
* 
* */
class UserUpgradeRequestModel extends BaseRequest {
  @override
  String url() => 'user/upgrade';
}

/*
* demo
*
* @param test string 演示参数介绍
*
* */
class WalletTransferRequestModel extends BaseRequest {
  final double amount;
  final String distPhone;
  final String password;
  final String sms;

  @override
  Map<String, dynamic> toJson() {
    return {
      'amount': this.amount,
      'distPhone': this.distPhone,
      'password': this.password,
      'sms': this.sms,
    };
  }

  WalletTransferRequestModel(
      this.amount, this.distPhone, this.password, this.sms);

  @override
  String url() => 'wallet/transfer';
}

/*
* demo
*
* @param test string 演示参数介绍
*
* */
class UserMyTeamRequestModel extends BaseRequest {
  final int? active;
  final int page;

  @override
  Map<String, dynamic> toJson() {
    return {
      'active': this.active,
      'page': this.page,
      'pageSize': PageMini.perPage,
    };
  }

  UserMyTeamRequestModel(this.active, this.page);

  @override
  String url() => 'user/myTeam';
}

/*
* demo
*
* @param test string 演示参数介绍
*
* */
class UserMyTeamStructureRequestModel extends BaseRequest {
  final int grade;

  @override
  Map<String, dynamic> toJson() {
    return {
      'grade': this.grade,
    };
  }

  UserMyTeamStructureRequestModel(this.grade);

  @override
  String url() => 'user/myTeamStructure';
}
