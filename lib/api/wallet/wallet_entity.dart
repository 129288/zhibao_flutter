import 'dart:convert';
import 'dart:developer';

import 'package:zhibao_flutter/util/check/check.dart';

void tryCatch(Function? f) {
  try {
    f?.call();
  } catch (e, stack) {
    log('$e');
    log('$stack');
  }
}

class FFConvert {
  FFConvert._();

  static T? Function<T extends Object?>(dynamic value) convert =
      <T>(dynamic value) {
    if (value == null) {
      return null;
    }
    return json.decode(value.toString()) as T?;
  };
}

T? asT<T extends Object?>(dynamic value, [T? defaultValue]) {
  if (value is T) {
    return value;
  }
  try {
    if (value != null) {
      final String valueS = value.toString();
      if ('' is T) {
        return valueS as T;
      } else if (0 is T) {
        return int.parse(valueS) as T;
      } else if (0.0 is T) {
        return double.parse(valueS) as T;
      } else if (false is T) {
        if (valueS == '0' || valueS == '1') {
          return (valueS == '1') as T;
        }
        return (valueS == 'true') as T;
      } else {
        return FFConvert.convert<T>(value);
      }
    }
  } catch (e, stackTrace) {
    log('asT<$T>', error: e, stackTrace: stackTrace);
    return defaultValue;
  }

  return defaultValue;
}

class WalletRspEntity {
  WalletRspEntity({
    this.balance,
    this.count,
    this.createTime,
    this.fromUserId,
    this.id,
    this.remark,
    this.type,
    this.userId,
    this.phone,
  });

  factory WalletRspEntity.fromJson(Map<String, dynamic> json) =>
      WalletRspEntity(
        balance: asT<double?>(() {
          String strValue =
              "${strNoEmpty("${json['balance'] ?? ""}") ? json['balance'] : 0.0}";
          strValue =
              strValue.length > 10 ? strValue.substring(0, 10) : strValue;
          return double.parse(strValue);
        }()),
        count: asT<String?>("${json['count'] ?? 0}"),
        createTime: asT<String?>(json['createTime']),
        fromUserId: asT<int?>(json['fromUserId']),
        id: asT<int?>(json['id']),
        remark: asT<String?>(json['remark']),
        type: asT<int?>(json['type']),
        phone: asT<String?>(json['phone']),
        userId: asT<int?>(json['userId']),
      );

  double? balance;
  String? count;
  String? createTime;
  int? fromUserId;
  int? id;
  String? remark;
  int? type;
  String? phone;
  int? userId;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'balance': balance,
        'count': count,
        'createTime': createTime,
        'fromUserId': fromUserId,
        'id': id,
        'remark': remark,
        'type': type,
        'phone': phone,
        'userId': userId,
      };

  WalletRspEntity copy() {
    return WalletRspEntity(
      balance: balance,
      count: count,
      createTime: createTime,
      fromUserId: fromUserId,
      id: id,
      remark: remark,
      type: type,
      phone: phone,
      userId: userId,
    );
  }
}

// extra是团队总人数，total是直推总人数
class TeamEntityTotalData {
  final List<TeamEntity> dataList;
  final int total;
  final int extra;

  TeamEntityTotalData(this.dataList,
      {required this.total, required this.extra});
}

class TeamEntity {
  TeamEntity({
    this.createTime,
    this.grade,
    this.hasActive,
    this.phone,
    this.username,
  });

  factory TeamEntity.fromJson(Map<String, dynamic> json) => TeamEntity(
        createTime: asT<String?>(json['createTime']),
        grade: asT<int?>(json['grade']),
        hasActive: asT<int?>(json['hasActive']),
        phone: asT<String?>(json['phone']),
        username: asT<String?>(json['username']),
      );

  String? createTime;
  int? grade;
  int? hasActive;
  String? phone;
  String? username;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'createTime': createTime,
        'grade': grade,
        'hasActive': hasActive,
        'phone': phone,
        'username': username,
      };

  TeamEntity copy() {
    return TeamEntity(
      createTime: createTime,
      grade: grade,
      hasActive: hasActive,
      phone: phone,
      username: username,
    );
  }
}

class TeamStructureEntity {
  TeamStructureEntity({
    this.grade,
    this.id,
    this.num,
    this.teamStructureCount,
    this.referrerPhone,
    this.phone,
    this.userId,
  });

  factory TeamStructureEntity.fromJson(Map<String, dynamic> json) =>
      TeamStructureEntity(
        grade: asT<int?>(json['grade']),
        id: asT<int?>(json['id']),
        num: asT<int?>(json['num']),
        teamStructureCount: asT<int?>(json['teamStructureCount']),
        referrerPhone: asT<String?>(json['referrerPhone']),
        phone: asT<String?>(json['phone']),
        userId: asT<int?>(json['userId']),
      );

  int? grade;
  int? id;
  int? num;
  int? teamStructureCount;
  String? phone;
  String? referrerPhone;
  int? userId;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'grade': grade,
        'id': id,
        'num': num,
        'teamStructureCount': teamStructureCount,
        'referrerPhone': referrerPhone,
        'phone': phone,
        'userId': userId,
      };

  TeamStructureEntity copy() {
    return TeamStructureEntity(
      grade: grade,
      id: id,
      num: num,
      teamStructureCount: teamStructureCount,
      referrerPhone: referrerPhone,
      phone: phone,
      userId: userId,
    );
  }
}
