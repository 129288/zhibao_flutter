import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:zhibao_flutter/api/mine/mine_entity.dart';
import 'package:zhibao_flutter/api/mine/mine_view_model.dart';
import 'package:zhibao_flutter/api/user/user_entity.dart';
import 'package:zhibao_flutter/api/user/user_model.dart';
import 'package:zhibao_flutter/api/wallet/wallet_entity.dart';
import 'package:zhibao_flutter/api/wallet/wallet_model.dart';
import 'package:zhibao_flutter/util/check/check.dart';
import 'package:zhibao_flutter/util/config/app_config.dart';
import 'package:zhibao_flutter/util/config/config_route.dart';
import 'package:zhibao_flutter/util/config/sp_key.dart';
import 'package:zhibao_flutter/util/data/q1_data.dart';
import 'package:zhibao_flutter/util/func/log.dart';
import 'package:zhibao_flutter/util/func/sp_util.dart';
import 'package:zhibao_flutter/util/http/http.dart';
import 'package:zhibao_flutter/util/ui/my_toast.dart';
import 'package:zhibao_flutter/widget/dialog/confirm_dialog.dart';
import 'package:zhibao_flutter/widget/view/hud_view.dart';
import 'package:get/get.dart';
// import 'package:rongcloud_im_plugin/rongcloud_im_plugin.dart';

WalletViewModel walletViewModel = WalletViewModel();

class WalletViewModel extends BaseViewModel {
  static bool isTemporaryTapProcessingFormLogin = false;

  static void restoreTemporaryProcessFormLogin([int milliseconds = 500]) {
    Future.delayed(Duration(milliseconds: milliseconds)).then((value) {
      isTemporaryTapProcessingFormLogin = false;
    });
  }

  /*
  * WalletLogRequestModel
  *
  **/
  Future<ResponseModel> walletLog(
    BuildContext? context, {
    required final int? page,
    required final int? type,
  }) async {
    // if (!kReleaseMode) {
    //   return ResponseModel.fromSuccess([
    //     WalletRspEntity.fromJson({
    //       "address": "string",
    //       "balance": 0,
    //       "count": 0,
    //       "createTime": "2024-02-28T15:37:51.405Z",
    //       "fromUserId": 0,
    //       "id": 0,
    //       "phone": "string",
    //       "remark": "string",
    //       "type": 0,
    //       "userId": 0
    //     })
    //   ]);
    // }
    return WalletLogRequestModel(
      page,
      type,
    )
        .sendApiAction(
      context,
      reqType: ReqType.get,
    )
        .then((rep) {
      final repList = List.from(rep['data'] ?? []).map<WalletRspEntity>((e) {
        return WalletRspEntity.fromJson(e);
      }).toList();
      return ResponseModel.fromSuccess(repList);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * WalletWithdrawApplyRequestModel
  *
  **/
  Future<ResponseModel> walletWithdrawApply(
    BuildContext context, {
    required final double amount,
    required final String address,
    required final String sms,
  }) async {
    return WalletWithdrawApplyRequestModel(
      amount,
      address,
      sms,
    )
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      return ResponseModel.fromSuccess(rep ?? true);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * UserUpgradeRequestModel
  *
  **/
  Future<ResponseModel> userUpgrade(BuildContext context) async {
    return UserUpgradeRequestModel()
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      return ResponseModel.fromSuccess(rep ?? true);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * WalletTransferRequestModel
  *
  **/
  Future<ResponseModel> walletTransfer(
    BuildContext context, {
    required final double amount,
    required final String distPhone,
    required final String password,
    required final String sms,
  }) async {
    return WalletTransferRequestModel(
      amount,
      distPhone,
      password,
      sms,
    )
        .sendApiAction(
      context,
      reqType: ReqType.post,
    )
        .then((rep) {
      return ResponseModel.fromSuccess(rep ?? true);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  /*
  * UserMyTeamRequestModel
  *
  **/
  Future<ResponseModel> userMyTeam(
    BuildContext? context, {
    required final int? active,
    required final int page,
  }) async {
    if (kDebugMode && AppConfig.isTestMode) {
      return ResponseModel.fromSuccess(
        TeamEntityTotalData(
          List.generate(
            10,
            (index) => TeamEntity(
              createTime: DateTime.now().toString(),
              grade: 6,
              username: "User $index",
              phone: "1888888888$index",
            ),
          ),
          total: 10,
          extra: 10,
        ),
      );
    }
    return UserMyTeamRequestModel(
      active,
      page,
    )
        .sendApiAction(
      context,
      reqType: ReqType.get,
    )
        .then((rep) {
      final repList = List.from(rep['data'] ?? []).map<TeamEntity>((e) {
        return TeamEntity.fromJson(e);
      }).toList();
      TeamEntityTotalData totalData = TeamEntityTotalData(repList,
          total: rep['total'], extra: rep['extra']);
      return ResponseModel.fromSuccess(totalData);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }

  Future<ResponseModel> userMyTeamStructure(
      BuildContext? context, final int grade) async {
    // if (!kReleaseMode) {
    //   return ResponseModel.fromSuccess(List.generate(
    //     1 + grade,
    //     (index) => TeamStructureEntity.fromJson({
    //       "grade": grade,
    //       "id": 2 + index,
    //       "num": 2 + index,
    //       "phone": "2331323${grade + index}",
    //       "referrerPhone": "1231213${grade + index}",
    //       "teamStructureCount": 2 + grade,
    //       "userId": 2313 + index
    //     }),
    //   )..add(TeamStructureEntity.fromJson({
    //       "grade": Q1Data.grade,
    //       "id": 234,
    //       "num": 1,
    //       "phone": Q1Data.phone,
    //       "referrerPhone": "${Q1Data.userInfoEntity?.referrerPhone ?? 0}",
    //       "teamStructureCount": 2,
    //       "userId": Q1Data.id
    //     })));
    // }
    return UserMyTeamStructureRequestModel(grade)
        .sendApiAction(
      context,
      reqType: ReqType.get,
    )
        .then((rep) {
      final repList = List.from(rep ?? []).map<TeamStructureEntity>((e) {
        return TeamStructureEntity.fromJson(e);
      }).toList();
      return ResponseModel.fromSuccess(repList);
    }).catchError((e) {
      return throwError(e);
    }).onError((error, stackTrace) {
      return onError(error, stackTrace);
    });
  }
}
