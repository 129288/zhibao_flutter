import 'package:zhibao_flutter/util/http/http.dart';

/*
* 登录
* Login request model.
*
* @param test string 演示参数介绍
*
* */
class SecurityLoginRequestModel extends BaseRequest {
  final String username;
  final String password;

  @override
  Map<String, dynamic> toJson() {
    return {
      'username': this.username,
      'password': this.password,
    };
  }

  SecurityLoginRequestModel(this.username, this.password);

  @override
  String url() => 'user/login';
}

/*
* 注册账号
*
* @param test string 演示参数介绍
*
* */
class RegisterRequestModel extends BaseRequest {
  final String checkCode;
  final String inviteCode;
  final String password;
  final String account;

  @override
  Map<String, dynamic> toJson() {
    return {
      'checkCode': this.checkCode,
      'inviteCode': this.inviteCode,
      'password': this.password,
      'account': this.account,
    };
  }

  RegisterRequestModel(
      this.checkCode, this.inviteCode, this.password, this.account);

  @override
  String url() => 'user/register';
}

/*
* 修改密码
*
* @param test string 演示参数介绍
*
* */
class ChangePswRequestModel extends BaseRequest {
  final String checkCode;
  final String inviteCode;
  final String password;
  final String account;

  @override
  Map<String, dynamic> toJson() {
    return {
      'checkCode': this.checkCode,
      'inviteCode': this.inviteCode,
      'password': this.password,
      'account': this.account,
    };
  }

  ChangePswRequestModel(
      this.checkCode, this.inviteCode, this.password, this.account);

  @override
  String url() => 'user/updatePassword';
}

/*
* demo
*
* @param test string 演示参数介绍
*
* */
class UserUpdatePayPasswordRequestModel extends BaseRequest {
  final String checkCode;
  final String password;
  final String account;

  @override
  Map<String, dynamic> toJson() {
    return {
      'checkCode': this.checkCode,
      'password': this.password,
      'account': this.account,
    };
  }

  UserUpdatePayPasswordRequestModel(this.checkCode, this.password, this.account);

  @override
  String url() => 'user/updatePayPassword';
}

/*
* 注销账号
*
* @param test string 演示参数介绍
*
* */
class LogoFFRequestModel extends BaseRequest {
  final String reason;
  final String check_code;

  @override
  Map<String, dynamic> toJson() {
    return {
      ' reason': this.reason,
      'check_code': this.check_code,
    };
  }

  LogoFFRequestModel(this.reason, this.check_code);

  @override
  String url() => 'api/v2/account/logoff';
}

/*
* 设置推送token
*
* @param test string 演示参数介绍
* 
* */
class SetPushIdRequestModel extends BaseRequest {
  final String push_id;

  @override
  Map<String, dynamic> toJson() {
    return {
      'push_id': this.push_id,
    };
  }

  SetPushIdRequestModel(this.push_id);

  @override
  String url() => 'api/v2/account/set_push_id';
}

/*
* 发送验证码（登录）
*
* @param test string 演示参数介绍
* 
* */
class SendCodeLoginRequestModel extends BaseRequest {
  final String account;

  // 0: 注册/找回 1: 修改支付密码
  final int type;

  @override
  Map<String, dynamic> toJson() {
    return {
      'account': this.account,
      'type': this.type,
    };
  }

  SendCodeLoginRequestModel(this.account, this.type);

  @override
  String url() => 'user/sendSms';
}

/*
* 刷新IM token
*
* @param test string 演示参数介绍
*
* */
class RefreshImTokenRequestModel extends BaseRequest {
  @override
  String url() => 'api/v2/account/refresh_im_token';
}

/*
* 获取注销原因
*
* @param test string 演示参数介绍
*
* */
class GetLogoFFReasonRequestModel extends BaseRequest {
  @override
  String url() => 'api/v2/account/get_logoff_reason';
}

/*
* 意见反馈
*
* @param test string 演示参数介绍
*
* */
class AddAdviseRequestModel extends BaseRequest {
  final String content;

  @override
  Map<String, dynamic> toJson() {
    return {
      'content': this.content,
    };
  }

  AddAdviseRequestModel(this.content);

  @override
  String url() => 'api/v2/user/add_advise';
}

/*
* 다음에 초대됩니다好友首页
*
* @param test string 演示参数介绍
*
* */
class YaoqingrenUserIndexRequestModel extends BaseRequest {
  @override
  String url() => 'api/v2/user/iv_user_index';
}

/*
* 提苗首页数据
*
* @param test string 演示参数介绍
*
* */
class CoinToCashIndexRequestModel extends BaseRequest {
  final int get_type;

  @override
  Map<String, dynamic> toJson() {
    return {
      'get_type': this.get_type,
    };
  }

  CoinToCashIndexRequestModel(this.get_type);

  @override
  String url() => 'api/v2/user/coin_to_cash_index';
}

/*
* 兑换虚拟币到잔액은 여기에 있습니다
*
* @param test string 演示参数介绍
*
* */
class CoinToCashRequestModel extends BaseRequest {
  final int amount;

  @override
  Map<String, dynamic> toJson() {
    return {
      'amount': this.amount,
    };
  }

  CoinToCashRequestModel(this.amount);

  @override
  String url() => 'api/v2/user/coin_to_cash';
}

/*
* 确认认证结果（H5）
*
* @param test string 演示参数介绍
*
* */
class CheckConfirmH5RequestModel extends BaseRequest {
  final String verify_token;

  @override
  Map<String, dynamic> toJson() {
    return {
      'verify_token': this.verify_token,
    };
  }

  CheckConfirmH5RequestModel(this.verify_token);

  @override
  String url() => 'api/web1/user/check_confirm_h5';
// String url() => 'api/v2/user/check_confirm_h5';
}
