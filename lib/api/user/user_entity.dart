import 'dart:convert';
import 'dart:developer';

void tryCatch(Function? f) {
  try {
    f?.call();
  } catch (e, stack) {
    log('$e');
    log('$stack');
  }
}

T? asT<T>(dynamic value) {
  if (value is T) {
    return value;
  }
  return null;
}
class LoginRspEntity {
  LoginRspEntity({
    required this.token,
    required this.dataCompleted,
    required this.imToken,
    required this.sex,
    required this.cityName,
    this.cityId,
    this.nickname,
    required this.icon,
    required this.uid,
    required this.iosInCheck,
    required this.androidInCheck,
    required this.accountStatus,
    required this.accountType,
    required this.isVip,
    required this.isConfirmed,
    required this.iosWords,
    required this.needConfirm,
  });

  factory LoginRspEntity.fromJson(Map<String, dynamic> json) => LoginRspEntity(
    token: asT<String?>(json['token']),
    dataCompleted: asT<int?>(json['data_completed']),
    imToken: asT<String?>(json['im_token']),
    sex: asT<int?>(json['sex']),
    cityName: asT<String?>(json['city_name']),
    cityId: asT<int?>(json['city_id']),
    nickname: asT<Object?>(json['nickname']),
    icon: asT<String?>(json['icon']),
    uid: asT<String?>(json['uid']),
    iosInCheck: asT<int?>(json['ios_in_check']),
    androidInCheck: asT<int?>(json['android_in_check']),
    accountStatus: asT<int?>(json['account_status']),
    accountType: asT<int?>(json['account_type']),
    isVip: asT<int?>(json['is_vip']),
    isConfirmed: asT<int?>(json['is_confirmed']),
    iosWords: asT<String?>(json['ios_words']),
    needConfirm: asT<int?>(json['need_confirm']),
  );

  String? token;
  int? dataCompleted;
  String? imToken;
  int? sex;
  String? cityName;
  int? cityId;
  Object? nickname;
  String? icon;
  String? uid;
  int? iosInCheck;
  int? androidInCheck;
  int? accountStatus;
  int? accountType;
  int? isVip;
  int? isConfirmed;
  String? iosWords;
  int? needConfirm;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
    'token': token,
    'data_completed': dataCompleted,
    'im_token': imToken,
    'sex': sex,
    'city_name': cityName,
    'city_id': cityId,
    'nickname': nickname,
    'icon': icon,
    'uid': uid,
    'ios_in_check': iosInCheck,
    'android_in_check': androidInCheck,
    'account_status': accountStatus,
    'account_type': accountType,
    'is_vip': isVip,
    'is_confirmed': isConfirmed,
    'ios_words': iosWords,
    'need_confirm': needConfirm,
  };
}


class YaoqingrenRspEntity {
  YaoqingrenRspEntity({
    this.yaoqingrenNumStr,
    this.yaoqingrenYicameStr,
    this.vipDaysStr,
    this.yaoqingrenRule,
    this.lightWords,
    this.explainMsg,
    this.yaoqingrenRank,
    this.fakeYaoqingrenData,
  });

  factory YaoqingrenRspEntity.fromJson(Map<String, dynamic> json) {
    final List<String>? lightWords =
        json['light_words'] is List ? <String>[] : null;
    if (lightWords != null) {
      for (final dynamic item in json['light_words']!) {
        if (item != null) {
          tryCatch(() {
            lightWords.add(asT<String>(item)!);
          });
        }
      }
    }

    // final List<String>? explainMsg =
    //     json['explain_msg'] is List ? <String>[] : null;
    // if (explainMsg != null) {
    //   for (final dynamic item in json['explain_msg']!) {
    //     if (item != null) {
    //       tryCatch(() {
    //         explainMsg.add(asT<String>(item)!);
    //       });
    //     }
    //   }
    // }

    final List<YaoqingrenRank>? yaoqingrenRank =
        json['iv_rank'] is List ? <YaoqingrenRank>[] : null;
    if (yaoqingrenRank != null) {
      for (final dynamic item in json['iv_rank']!) {
        if (item != null) {
          tryCatch(() {
            yaoqingrenRank
                .add(YaoqingrenRank.fromJson(asT<Map<String, dynamic>>(item)!));
          });
        }
      }
    }

    final List<FakeYaoqingrenData> fakeYaoqingrenData =
        json['fake_iv_data'] is List ? <FakeYaoqingrenData>[] : [];
    for (final dynamic item in json['fake_iv_data']!) {
      if (item != null) {
        tryCatch(() {
          fakeYaoqingrenData
              .add(FakeYaoqingrenData.fromJson(asT<Map<String, dynamic>>(item)!));
        });
      }
    }
    return YaoqingrenRspEntity(
      yaoqingrenNumStr: asT<String?>(json['iv_num_str']),
      yaoqingrenYicameStr: asT<String?>(json['iv_rise_str']),
      vipDaysStr: asT<String?>(json['vip_days_str']),
      yaoqingrenRule: asT<String?>(json['iv_rule']),
      lightWords: lightWords,
      explainMsg: json['explain_msg'] == null
          ? ""
          : json['explain_msg'] == "[]"
              ? ""
              : json['explain_msg'],
      yaoqingrenRank: yaoqingrenRank,
      fakeYaoqingrenData: fakeYaoqingrenData,
    );
  }

  String? yaoqingrenNumStr;
  String? yaoqingrenYicameStr;
  String? vipDaysStr;
  String? yaoqingrenRule;
  List<String>? lightWords;
  Object? explainMsg;
  List<YaoqingrenRank>? yaoqingrenRank;
  List<FakeYaoqingrenData>? fakeYaoqingrenData;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'iv_num_str': yaoqingrenNumStr,
        'iv_rise_str': yaoqingrenYicameStr,
        'vip_days_str': vipDaysStr,
        'iv_rule': yaoqingrenRule,
        'light_words': lightWords,
        'explain_msg': explainMsg,
        'iv_rank': yaoqingrenRank,
        'fake_iv_data': fakeYaoqingrenData,
      };
}

class YaoqingrenRank {
  YaoqingrenRank({
    this.icon,
    this.nickname,
    this.yaoqingrenNum,
    this.yaoqingrenYicame,
    this.peerUid,
  });

  factory YaoqingrenRank.fromJson(Map<String, dynamic> json) => YaoqingrenRank(
        icon: asT<String?>(json['icon']),
        nickname: asT<String?>(json['nickname']),
        yaoqingrenNum: asT<int?>(json['iv_num'] is String
            ? int.parse(json['iv_num'])
            : json['iv_num']),
        yaoqingrenYicame: asT<String?>(json['iv_rise']),
        peerUid: asT<String?>(json['peer_uid']),
      );

  String? icon;
  String? nickname;
  int? yaoqingrenNum;
  String? yaoqingrenYicame;
  String? peerUid;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'icon': icon,
        'nickname': nickname,
        'iv_num': yaoqingrenNum,
        'iv_rise': yaoqingrenYicame,
        'peer_uid': peerUid,
      };
}

class FakeYaoqingrenData {
  FakeYaoqingrenData({
    this.nickname,
    this.operateType,
    this.uid,
    this.yicame,
  });

  factory FakeYaoqingrenData.fromJson(Map<String, dynamic> json) => FakeYaoqingrenData(
        nickname: asT<String?>(json['nickname']),
        operateType: asT<String?>(json['operate_type']),
        uid: asT<String?>(json['uid']),
        yicame: asT<String?>(json['rise']),
      );

  String? nickname;
  String? operateType;
  String? uid;
  String? yicame;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'nickname': nickname,
        'operate_type': operateType,
        'uid': uid,
        'rise': yicame,
      };
}

class YaoqingrenRecordRspEntity {
  YaoqingrenRecordRspEntity({
    this.registerTimeStr,
    this.peerUid,
    this.icon,
    this.sex,
    this.nickname,
    this.description,
    this.amount,
    this.isVip,
    this.isConfirmed,
  });

  factory YaoqingrenRecordRspEntity.fromJson(Map<String, dynamic> json) =>
      YaoqingrenRecordRspEntity(
        registerTimeStr: asT<String?>(json['register_time_str']),
        peerUid: asT<String?>(json['peer_uid']),
        icon: asT<String?>(json['icon']),
        sex: asT<int?>(json['sex']),
        nickname: asT<String?>(json['nickname']),
        description: asT<String?>(json['description']),
        amount: "${json['amount'] ?? "0"}",
        isVip: asT<int?>(json['is_vip']),
        isConfirmed: asT<int?>(json['is_confirmed']),
      );

  String? registerTimeStr;
  String? peerUid;
  String? icon;
  int? sex;
  String? nickname;
  String? description;
  String? amount;
  int? isVip;
  int? isConfirmed;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'register_time_str': registerTimeStr,
        'peer_uid': peerUid,
        'icon': icon,
        'sex': sex,
        'nickname': nickname,
        'description': description,
        'amount': amount,
        'is_vip': isVip,
        'is_confirmed': isConfirmed,
      };
}

class ShachaPicOptionRsp {
  ShachaPicOptionRsp({
    this.imgUrl,
    this.shachaUrl,
    this.shachaPermissionNewUser,
    this.shachaContentUrlNewUser,
    this.shachaTitle,
    this.shachaContent,
  });

  factory ShachaPicOptionRsp.fromJson(Map<String, dynamic> json) {
    final List<String>? imgUrl = json['img_url'] is List ? <String>[] : null;
    if (imgUrl != null) {
      for (final dynamic item in json['img_url']!) {
        if (item != null) {
          tryCatch(() {
            imgUrl.add(asT<String>(item)!);
          });
        }
      }
    }
    return ShachaPicOptionRsp(
      imgUrl: imgUrl,
      shachaUrl: asT<String?>(json['shacha_url']),
      shachaPermissionNewUser: asT<int?>(json['shacha_permission_new_user']),
      shachaContentUrlNewUser: asT<String?>(json['shacha_content_url_new_user']),
      shachaTitle: asT<String?>(json['shacha_title']),
      shachaContent: asT<String?>(json['shacha_content']),
    );
  }

  List<String>? imgUrl;
  String? shachaUrl;
  int? shachaPermissionNewUser;
  String? shachaContentUrlNewUser;
  String? shachaTitle;
  String? shachaContent;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'img_url': imgUrl,
        'shacha_url': shachaUrl,
        'shacha_permission_new_user': shachaPermissionNewUser,
        'shacha_content_url_new_user': shachaContentUrlNewUser,
        'shacha_title': shachaTitle,
        'shacha_content': shachaContent,
      };
}

class RetiradaEntity {
  RetiradaEntity({
    this.coinNum,
    this.explainMsg,
  });

  factory RetiradaEntity.fromJson(Map<String, dynamic> json) =>
      RetiradaEntity(
        coinNum: asT<int?>(json['coin_num']),
        explainMsg: asT<String?>(json['explain_msg']),
      );

  int? coinNum;
  String? explainMsg;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'coin_num': coinNum,
        'explain_msg': explainMsg,
      };
}

class FileDataEntity {
  FileDataEntity({
    this.fileType,
    this.fileUrl,
    this.isSelf,
    this.price,
    this.more = false,
  });

  factory FileDataEntity.fromJson(Map<String, dynamic> json) => FileDataEntity(
        fileType: asT<int?>(json['file_type']),
        fileUrl: asT<String?>(json['file_url']),
        isSelf: asT<int?>(json['is_self']),
        price: asT<int?>(json['price']),
        more: false,
      );

  // 1普通图片 2图片阅后即焚未焚烧 3图片阅后即焚已焚烧 4免费视频无蒙板 5收费视频未付费 6收费视频已付费
  int? fileType;
  String? fileUrl;

  String get fileCover {
    return (fileUrl ?? "") + "?x-oss-process=video/snapshot,t_0,m_fast,f_jpg";
  }

  // 是否为本人照片
  int? isSelf;

  // 视频价格，默认为0
  int? price;

  bool more;

  int get isSnapChat {
    return fileType == 2 ? 1 : 0;
  }

  String get imgUrl {
    return fileUrl ?? "";
  }

  bool isPic() {
    return (fileType ?? 0) <= 3;
  }

  /// 是否隐藏图片内容
  bool get hiddenPic {
    return fileType == 2 || fileType == 3;
  }

  /// 是否是图片
  bool get video {
    return fileType == 4 || fileType == 5 || fileType == 6;
  }

  String fileTypeStr() {
    // 1普通图片 2图片阅后即焚未焚烧 3图片阅后即焚已焚烧 4免费视频无蒙板 5收费视频未付费 6收费视频已付费
    if (fileType == 1) {
      return "普通图片";
    } else if (fileType == 2) {
      return "图片阅后即焚未焚烧";
    } else if (fileType == 3) {
      return "图片阅后即焚已焚烧";
    } else if (fileType == 4) {
      return "免费视频无蒙板";
    } else if (fileType == 5) {
      return "收费视频未付费";
    } else if (fileType == 6) {
      return "收费视频已付费";
    } else {
      return "未知图片";
    }
  }

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'file_type': fileType,
        'file_url': fileUrl,
        'is_self': isSelf,
        'price': price,
      };
}

class NoPermissionRsp {
  NoPermissionRsp({
    this.permissionTitle,
    this.permissionMsg,
    this.permission,
    this.topIcon,
    this.price,
    this.orderType,
    this.contents,
    this.payByCoin,
    this.realDepositNum,
    this.realCoinNum,
    this.inBlacklist,
    this.reportText,
    this.serviceWekaka,
    this.correctRequestUserId,
    this.targetUserId,
  });

  factory NoPermissionRsp.fromJson(
      Map<String, dynamic> json, String targetUserId) {
    final List<List<String>>? contents =
        json['contents'] is List ? <List<String>>[] : null;
    if (contents != null) {
      for (final dynamic item0 in asT<List<dynamic>>(json['contents'])!) {
        if (item0 != null) {
          final List<String> items1 = <String>[];
          for (final dynamic item1 in asT<List<dynamic>>(item0)!) {
            if (item1 != null) {
              tryCatch(() {
                items1.add(asT<String>(item1)!);
              });
            }
          }
          contents.add(items1);
        }
      }
    }
    return NoPermissionRsp(
      permissionTitle: asT<String?>(json['permission_title']),
      permissionMsg: asT<String?>(json['permission_msg']),
      permission: asT<int?>(json['permission']),
      topIcon: asT<String?>(json['top_icon']),
      price: asT<double?>(json['price'] is int
          ? (json['price'] as int).toDouble()
          : json['price']),
      orderType: asT<int?>(json['order_type']),
      contents: contents,
      payByCoin: asT<int?>(json['pay_by_coin']),
      realDepositNum: asT<String?>(json['real_deposit_num']),
      realCoinNum: asT<int?>(json['real_coin_num']),
      inBlacklist: asT<int?>(json['in_blacklist']),
      reportText: asT<String?>(json['report_text']),
      serviceWekaka: asT<String?>(json['service_wx']),
      correctRequestUserId: asT<String?>(json['correct_request_userId']),
      targetUserId: targetUserId,
    );
  }

  String? permissionTitle;
  String? permissionMsg;
  int? permission;
  String? topIcon;
  double? price;
  int? orderType;
  List<List<String>>? contents;
  int? payByCoin;
  String? realDepositNum;
  int? realCoinNum;
  int? inBlacklist;
  String? reportText;
  String? serviceWekaka;
  String? correctRequestUserId;
  String? targetUserId;

  @override
  String toString() {
    return jsonEncode(this);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'permission_title': permissionTitle,
        'permission_msg': permissionMsg,
        'permission': permission,
        'top_icon': topIcon,
        'price': price,
        'order_type': orderType,
        'contents': contents,
        'pay_by_coin': payByCoin,
        'real_deposit_num': realDepositNum,
        'real_coin_num': realCoinNum,
        'in_blacklist': inBlacklist,
        'report_text': reportText,
        'service_wx': serviceWekaka,
        'correct_request_userId': correctRequestUserId,
      };

  NoPermissionRsp copy() {
    return NoPermissionRsp(
      permissionTitle: permissionTitle,
      permissionMsg: permissionMsg,
      permission: permission,
      topIcon: topIcon,
      price: price,
      orderType: orderType,
      contents: contents
          ?.map((List<String> e) => e.map((String e) => e).toList())
          .toList(),
      payByCoin: payByCoin,
      realDepositNum: realDepositNum,
      realCoinNum: realCoinNum,
      inBlacklist: inBlacklist,
      reportText: reportText,
    );
  }
}
