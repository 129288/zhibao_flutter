import 'dart:async';

import 'package:flutter/material.dart';
import 'package:zhibao_flutter/util/check/click_event.dart';
import 'package:zhibao_flutter/util/config/config_route.dart';
import 'package:zhibao_flutter/util/float/fram_size.dart';
import 'package:get/get.dart';

class TestMenu {
  static OverlayEntry? overlayEntry;

  static void showOverlayEntry(BuildContext context) {
    removeOverlayEntry();

    overlayEntry = OverlayEntry(builder: (context) {
      return TestMenuStatePage();
    });

    //往Overlay中插入插入OverlayEntry
    Overlay.of(context, rootOverlay: true)!.insert(overlayEntry!);
  }

  static void removeOverlayEntry() {
    if (overlayEntry != null) {
      if (overlayEntry?.mounted ?? false) overlayEntry!.remove();
      overlayEntry = null;
    }
  }
}

class TestMenuStatePage extends StatefulWidget {
  @override
  _TestMenuStatePageState createState() => _TestMenuStatePageState();
}

class _TestMenuStatePageState extends State<TestMenuStatePage> {
  double _left = FrameSize.screenW() - 232;
  double _top = 139;

  TextStyle style = TextStyle(
    color: Colors.redAccent.withOpacity(0.8),
    fontSize: 12,
  );

  bool get isShowHomeIcon {
    return true;
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: _top, //moveOffset.dy,
      left: _left, //moveOffset.dx,
      child: GestureDetector(
        // 移动中
        onPanUpdate: (details) {
          setState(() {
            _top = details.globalPosition.dy;
            _left = details.globalPosition.dx;
          });
        },
        // 移动结束
        onPanEnd: (details) {
          setState(() {
            if (_top <= (FrameSize.padTopH() + 5.5)) {
              _top = FrameSize.padTopH() + 5.5;
            } else if (_top >=
                FrameSize.screenH() - (FrameSize.padBotH() * 2)) {
              _top = FrameSize.screenH() - (FrameSize.padBotH() * 2);
            }
          });
        },

        child: Material(
          type: MaterialType.transparency,
          child: ClickEvent(
            onTap: () async {
              Get.toNamed(RouteConfig.liveLogPage);
            },
            child: Container(
              width: 132,
              height: 102,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: const Color(0xff000000).withOpacity(0.2),
                borderRadius: const BorderRadius.all(Radius.circular(5)),
              ),
              padding: EdgeInsets.symmetric(horizontal: 5),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                      '屏幕宽度:${FrameSize.winWidthDynamic(context).toStringAsFixed(2)}',
                      style: style),
                  Text(
                      '屏幕高度:${FrameSize.winHeightDynamic(context).toStringAsFixed(2)}',
                      style: style),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
