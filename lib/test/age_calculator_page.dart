import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:zhibao_flutter/zone.dart';

class AgeCalculatorPage extends StatefulWidget {
  @override
  _AgeCalculatorPageState createState() => _AgeCalculatorPageState();
}

class _AgeCalculatorPageState extends State<AgeCalculatorPage> {
  DateTime? dob;
  String result = '';

  void calculateAge() {
    if (dob != null) {
      final currentDate = DateTime.now();
      final age = currentDate.year - dob!.year;
      final months = currentDate.month - dob!.month;
      final days = currentDate.day - dob!.day;

      setState(() {
        result = '年龄: $age 年\n月份: $months 月\n天数: $days 天';
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('年龄计算器'),
      ),
      body: Container(
        width: Get.width,
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: 20.px),
            ElevatedButton(
              onPressed: () async {
                final date = await showDatePicker(
                  context: context,
                  initialDate: DateTime.now(),
                  firstDate: DateTime(1900),
                  lastDate: DateTime.now(),
                  locale: const Locale('zh', 'CN'),
                );
                if (date != null) {
                  setState(() {
                    dob = date;
                  });
                  calculateAge();
                }
              },
              child: Text('选择出生日期'),
            ),
            SizedBox(height: 20.px),
            if (dob != null)
              Text('出生日期: ${DateFormat('yyyy年MM月dd日', 'zh_CN').format(dob!)}'),
            SizedBox(height: 20.px),
            Text(result),
          ],
        ),
      ),
    );
  }
}
