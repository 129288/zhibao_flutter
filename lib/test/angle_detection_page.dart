import 'dart:io';
import 'dart:math' as math;

import 'package:compassx/compassx.dart';
import 'package:flutter/material.dart';
import 'package:oktoast/oktoast.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:zhibao_flutter/zone.dart';

class AngleDetectionPage extends StatefulWidget {
  const AngleDetectionPage({super.key});

  @override
  State<AngleDetectionPage> createState() => _AngleDetectionPageState();
}

class _AngleDetectionPageState extends State<AngleDetectionPage> {
  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      appBar: CommomBar(title: "角度检测"),
      body: Center(
        child: StreamBuilder<CompassXEvent>(
          stream: CompassX.events,
          builder: (context, snapshot) {
            if (snapshot.hasError) return Text(snapshot.error.toString());
            if (!snapshot.hasData) return const CircularProgressIndicator();
            final compass = snapshot.data!;
            return Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text('北航向: ${compass.heading}'),
                Text('准确性: ${compass.accuracy}'),
                Text('应校准: ${compass.shouldCalibrate}'),
                Transform.rotate(
                  angle: compass.heading * math.pi / 180,
                  child: Icon(
                    Icons.arrow_upward_rounded,
                    size: MediaQuery.of(context).size.width - 80,
                  ),
                ),
                FilledButton(
                  onPressed: () async {
                    if (!Platform.isAndroid) return;
                    await Permission.location.request();
                    showToast("操作成功");
                  },
                  child: const Text('请求权限【如果无效则点击】'),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
