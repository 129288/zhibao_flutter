import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:zhibao_flutter/test/feedback_page.dart';
import 'package:zhibao_flutter/widget/test/label_tile.dart';
import 'package:zhibao_flutter/zone.dart';

class AboutPage extends StatefulWidget {
  const AboutPage({Key? key}) : super(key: key);

  @override
  _AboutPageState createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  PackageInfo? packageInfo;

  List data = [
    // '用户协议',
    '隐私政策',
    // "联系我们",
    "意见反馈",
    // "详情介绍【广告招租】",
  ];

  @override
  void initState() {
    super.initState();
    init();
  }

  void init() async {
    packageInfo = await PackageInfo.fromPlatform();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      backgroundColor: Colors.white,
      appBar: const CommomBar(title: '', backgroundColor: Colors.white),
      body: SizedBox(
        width: Get.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Space(height: 10),
            Image.asset(
              'assets/images/ic_launcher.png',
              width: 80,
            ),
            Padding(
              padding: EdgeInsets.only(top: 15.px, bottom: 5.px),
              child: Text(
                '${AppConfig.appName}APP',
                style: TextStyle(fontSize: 22.px, fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 10.px),
              child: Text(
                'V${packageInfo?.version ?? '1.0.0'}',
                style: TextStyle(fontSize: 17.px),
              ),
            ),
            Expanded(
              child: ListView.builder(
                itemBuilder: (context, index) {
                  return LabelTile(
                    title: '${data[index]}',
                    onPressed: () {
                      if (data[index] == "意见反馈") {
                        Get.to(const FeedbackPage());
                      } else if (data[index] == "详情介绍") {
                        // Get.to(const DesPage());
                      } else if (data[index] == "联系我们") {
                        // Get.to(ContactMePage());
                      } else {
                        Get.toNamed(RouteConfig.privacyPage);
                      }
                    },
                  );
                },
                itemCount: data.length,
              ),
            ),
            Center(
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                child: Text(
                  'Copyright ©2019-2023 ${AppConfig.appName}.All Rights Reserved.',
                  style: TextStyle(color: Color(0xffAEAEAE), fontSize: 13),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
