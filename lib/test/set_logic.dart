import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:zhibao_flutter/api/commom/common_view_model.dart';
import 'package:zhibao_flutter/api/user/user_view_model.dart';
import 'package:zhibao_flutter/test/feedback_page.dart';
import 'package:zhibao_flutter/widget/dialog/confirm_dialog.dart';
import 'package:zhibao_flutter/zone.dart';

class SetLogic extends GetxController {
  List<String> item = [
    '检测版本',
    '意见反馈',
    // '用户协议',
    '隐私政策',
    '修改密码',
    // '【密码666】下载新版',
    // '【尝试】打开官网',
    // '【备用1】打开官网',
    // '【备用2】打开官网',
    // '退出账号',
  ]; // , '夜间模式'

  late BuildContext context;

  Future action(String e) async {
    switch (e) {
      case "检测版本":
        await commonViewModel.checkVersion(context, true);
        break;
      // case "【密码666】下载新版":
      //   launchURL('https://wwpj.lanzoul.com/b019apzwd');
      //   break;
      // case "【尝试】打开官网":
      //   launchURL('http://shanzhu2.top/');
      //   break;
      // case "【备用1】打开官网":
      //   launchURL('http://shanzhu7.top/');
      //   break;
      // case "【备用2】打开官网":
      //   launchURL('http://shanzhu6.top/');
      //   break;
      case "退出账号":
        confirmDialog(context, text: "确定退出登录吗？", onPressed: () {
          userViewModel.loginOut();
        });
        break;
      // case "设置密码":
      //   if (!Q1Data.isLogin()) {
      //     Get.to(const LoginPage());
      //     return;
      //   }
      //   Get.to(const ForgetPwPage(title: '设置密码'));
      //   break;
      case "意见反馈":
        await Get.to(const FeedbackPage());
        break;
      case "修改密码":
        Get.toNamed(RouteConfig.forgetPwPage, arguments: true);
        break;
      // case "用户协议":
      //   await Get.to(const ProtocolPage(0));
      //   break;
      case "隐私政策":
        Get.toNamed(RouteConfig.privacyPage);
        break;
      // case "夜间模式":
      //   Store(Q1Actions.isDark()).value =
      //       !(Store(Q1Actions.isDark()).value ?? false);
      //   break;
      default:
        myToast('comingSoon'.tr);
        break;
    }
  }
}
