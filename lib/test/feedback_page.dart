import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:zhibao_flutter/dialog/select_picture_dialog.dart';
import 'package:zhibao_flutter/util/http/base_view_model.dart';
import 'package:zhibao_flutter/zone.dart';

class FeedbackPage extends StatefulWidget {
  const FeedbackPage({Key? key}) : super(key: key);

  @override
  _FeedbackPageState createState() => _FeedbackPageState();
}

class _FeedbackPageState extends State<FeedbackPage> {
  TextEditingController controller = TextEditingController();
  TextEditingController contact = TextEditingController();

  bool inAsync = false;

  List<String> imgList = ['', '', ''];

  TextStyle hintStyle = TextStyle(fontSize: 16.px);

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      inAsync: inAsync,
      backgroundColor: Colors.white,
      appBar: CommomBar(
        title: '意见反馈',
        rightDMActions: [
          // UnconstrainedBox(
          //   child: SmallButton(
          //     padding: EdgeInsets.symmetric(horizontal: 10.px),
          //     minHeight: 30.px,
          //     onPressed: () async {
          //       await Get.to(AdviceFeedbackPage([
          //         FeedBackPageModel(),
          //       ]));
          //     },
          //     child: Text(
          //       '反馈列表',
          //       style: TextStyle(color: Colors.white, fontSize: 12.px),
          //     ),
          //   ),
          // ),
          // Space(),
        ],
      ),
      body: Column(
        children: [
          Container(
            height: 30,
            alignment: Alignment.center,
            color: Colors.purple.withOpacity(0.3),
            child: Text(
              '感谢您的反馈，我们都会仔细查看哒~',
              style: TextStyle(color: Colors.white),
            ),
          ),
          Expanded(
            child: ListView(
              padding: EdgeInsets.all(20.px),
              children: [
                Container(
                  decoration: BoxDecoration(
                    color: const Color(0xffF4F4F4),
                    borderRadius: BorderRadius.all(Radius.circular(8.px)),
                  ),
                  child: TextField(
                    maxLines: 10,
                    controller: controller,
                    onChanged: (str) {
                      setState(() {});
                    },
                    decoration: InputDecoration(
                      hintText: '请输入反馈内容',
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.symmetric(
                        horizontal: 10.px,
                        vertical: 10.px,
                      ),
                    ),
                  ),
                ),
                Space(height: 10),
                Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    '${controller.text.length}/300',
                    style: TextStyle(
                      fontSize: 14,
                      color: controller.text.length > 300
                          ? Colors.red
                          : Colors.grey,
                    ),
                  ),
                ),
                // Space(height: 20),
                // Row(
                //   children: [
                //     Text('联系方式', style: hintStyle),
                //     Space(),
                //     Expanded(
                //       child: Container(
                //         decoration: BoxDecoration(
                //           color: Color(0xffF4F4F4),
                //           borderRadius: BorderRadius.all(Radius.circular(8)),
                //         ),
                //         child: TextField(
                //           maxLines: 1,
                //           controller: contact,
                //           onChanged: (str) {
                //             setState(() {});
                //           },
                //           decoration: InputDecoration(
                //             hintText: '请输入联系方式',
                //             border: InputBorder.none,
                //             contentPadding: EdgeInsets.symmetric(
                //               horizontal: 10,
                //               vertical: 5,
                //             ),
                //           ),
                //         ),
                //       ),
                //     )
                //   ],
                // ),
                Space(height: 30),
                Row(
                  children: [
                    Text('请输入上传图片 [选填]', style: hintStyle),
                  ],
                ),
                Space(height: 15),
                Wrap(
                  spacing: 10,
                  runSpacing: 10,
                  children: List.generate(imgList.length, (index) {
                    var e = imgList[index];
                    var _size = ((Get.width - 60.px) / 3) - 0.01;
                    Container _content;
                    if (strNoEmpty(e)) {
                      _content = Container(
                        width: _size,
                        height: _size,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: swImageProvider(e),
                          ),
                        ),
                      );
                    } else {
                      _content = Container(
                        width: _size,
                        height: _size,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(8.px)),
                          color: const Color(0xffF4F4F4),
                        ),
                        child: const Icon(Icons.add, color: Colors.grey),
                      );
                    }
                    return InkWell(
                      child: _content,
                      onTap: () => upload(index, e),
                    );
                  }),
                ),
                Space(height: 60),
                SmallButton(
                  padding: EdgeInsets.symmetric(vertical: 12.px),
                  child: const Text('提交'),
                  onPressed: () async {
                    await post();
                  },
                ),
                Space(height: 50),
              ],
            ),
          )
        ],
      ),
    );
  }

  void upload(index, e) {
    // myToast('敬请期待');
    selectPictureDialog(
      context,
      callback: (List<Q1FileResult> _value) {
        if (_value.isEmpty) {
          return;
        }
        Q1FileResult rsp = _value.first;
        imgList[index] = rsp.resultUrl;
        setState(() {});
      },
      isIdFileName: false,
      isCrop: false,
      dir: 'feedback',
      description: '',
    );
  }

  Future post() async {
    // myToast('敬请期待');
    // return;

    if (controller.text.isEmpty) {
      myToast('请输入内容');
      return;
    }
    if (controller.text.length > 300) {
      myToast('文字长度过大，请删除一部分后再试');
      return;
    }
    inAsync = true;
    setState(() {});
    // systemViewModel
    //     .feedBackPost(
    //   context,
    //   question: controller.text,
    //   image: imgList,
    //   contact: contact.text,
    // )
    //     .then((value) {
    await Future.delayed(const Duration(milliseconds: 800));
    inAsync = false;
    setState(() {});
    Get.back();
    myToast('反馈成功');
    // }).catchError((e) {
    //   inAsync = false;
    //   setState(() {});
    //   onError(e);
    // });
  }
}
