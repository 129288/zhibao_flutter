import 'package:flutter/material.dart';
import 'package:zhibao_flutter/test/set_logic.dart';
import 'package:zhibao_flutter/zone.dart';

class SetPage extends StatefulWidget {
  const SetPage({Key? key}) : super(key: key);

  @override
  _SetPageState createState() => _SetPageState();
}

class _SetPageState extends State<SetPage> {
  final logic = Get.put(SetLogic());

  Widget itemBuild(context, index) {
    if (index == logic.item.length) {
      return SmallButton(
        onPressed: () async {
          logic.action("退出账号");
        },
        margin: EdgeInsets.symmetric(horizontal: 20.px, vertical: 20.px),
        child: Text('退出账号'),
      );
    }
    final title = logic.item[index];
    return ClickEvent(
      onTap: () async {
        return logic.action(title);
      },
      child: Container(
        decoration: BoxDecoration(
          // color: Theme.of(context).textTheme.bodyText1?.color,
          border: Border(
              bottom: MyTheme.mainBorderSide(
                  title == logic.item[logic.item.length - 1]
                      ? Colors.transparent
                      : null)),
        ),
        child: ListTile(
          title: Text(title),
          trailing: Text(
            title == "检测版本" ? Q1DataRequest.versionStr : "",
            style: TextStyle(color: Colors.grey),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    logic.context = context;
    return MyScaffold(
      appBar: const CommomBar(title: '设置'),
      body: ListView.builder(
        itemBuilder: itemBuild,
        itemCount: logic.item.length + 1,
      ),
    );
  }
}
