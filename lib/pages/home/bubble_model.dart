/// **************************************************************************
/// ignore_for_file: non_constant_identifier_names,library_prefixes
/// **************************************************************************

class MockBubbleModel {
  final NoticesOk? notices;
  final JumpPages? jumpPages;
  final int noticeVersion;

  MockBubbleModel({
   required this.notices,
   required this.jumpPages,
   required this.noticeVersion,
  });

  factory MockBubbleModel.fromJson(Map<String, dynamic> json) =>
      _$MockBubbleModelFromJson(json);

  MockBubbleModel from(Map<String, dynamic> json) =>
      _$MockBubbleModelFromJson(json);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.notices != null) {
      data['notices'] = this.notices?.toJson();
    }
    if (this.jumpPages != null) {
      data['jumpPages'] = this.jumpPages?.toJson();
    }
    data['noticeVersion'] = this.noticeVersion;
    return data;
  }
}

MockBubbleModel _$MockBubbleModelFromJson(Map<String, dynamic> json) {
  return MockBubbleModel(
    notices: json['notices'] != null
        ? new NoticesOk.fromJson(json['notices'])
        : null,
    jumpPages: json['jumpPages'] != null
        ? new JumpPages.fromJson(json['jumpPages'])
        : null,
    noticeVersion: json['noticeVersion'],
  );
}

class NoticesOk {
  final List<NoticesModel>? notices;
  final Null noticeWindows;
  final Null activityWindows;

  NoticesOk({
    this.notices,
    this.noticeWindows,
    this.activityWindows,
  });

  factory NoticesOk.fromJson(Map<String, dynamic> json) =>
      _$NoticesFromJson(json);

  NoticesOk from(Map<String, dynamic> json) => _$NoticesFromJson(json);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.notices != null) {
      data['notices'] = this.notices?.map((v) => v.toJson()).toList();
    }
    data['noticeWindows'] = this.noticeWindows;
    data['activityWindows'] = this.activityWindows;
    return data;
  }
}

NoticesOk _$NoticesFromJson(Map<String, dynamic> json) {
  return NoticesOk(
    notices: List.from(json['notices'].map((item) {
      return new NoticesModel.fromJson(item);
    }).toList()),
    noticeWindows: json['noticeWindows'],
    activityWindows: json['activityWindows'],
  );
}

class NoticesModel {
  final int id;
  final int type;
  final String title;
  final String content;
  final String img;
  final String link;
  final String btnText;
  final int startTime;
  final int endTime;
  final String name;
  final int frequency;

  NoticesModel({
    required this.id,
    required this.type,
    required this.title,
    required this.content,
    required this.img,
    required this.link,
    required this.btnText,
    required this.startTime,
    required this.endTime,
    required this.name,
    required this.frequency,
  });

  factory NoticesModel.fromJson(Map<String, dynamic> json) =>
      _$NoticesModelFromJson(json);

  NoticesModel from(Map<String, dynamic> json) => _$NoticesModelFromJson(json);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['type'] = this.type;
    data['title'] = this.title;
    data['content'] = this.content;
    data['img'] = this.img;
    data['link'] = this.link;
    data['btnText'] = this.btnText;
    data['startTime'] = this.startTime;
    data['endTime'] = this.endTime;
    data['name'] = this.name;
    data['frequency'] = this.frequency;
    return data;
  }
}

NoticesModel _$NoticesModelFromJson(Map<String, dynamic> json) {
  return NoticesModel(
    id: json['id'],
    type: json['type'],
    title: json['title'],
    content: json['content'],
    img: json['img'],
    link: json['link'],
    btnText: json['btnText'],
    startTime: json['startTime'],
    endTime: json['endTime'],
    name: json['name'],
    frequency: json['frequency'],
  );
}

class JumpPages {
  final Null jumpPages;
  final List<HorizontalJumpPages> horizontalJumpPages;
  final List<VerticalJumpPages> verticalJumpPages;

  JumpPages({
    this.jumpPages,
    required this.horizontalJumpPages,
    required this.verticalJumpPages,
  });

  factory JumpPages.fromJson(Map<String, dynamic> json) =>
      _$JumpPagesFromJson(json);

  JumpPages from(Map<String, dynamic> json) => _$JumpPagesFromJson(json);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['jumpPages'] = this.jumpPages;
    if (this.horizontalJumpPages != null) {
      data['horizontalJumpPages'] =
          this.horizontalJumpPages.map((v) => v.toJson()).toList();
    }
    if (this.verticalJumpPages != null) {
      data['verticalJumpPages'] =
          this.verticalJumpPages.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

JumpPages _$JumpPagesFromJson(Map<String, dynamic> json) {
  return JumpPages(
    jumpPages: json['jumpPages'],
    horizontalJumpPages: List.from(json['horizontalJumpPages'].map((item) {
      return new HorizontalJumpPages.fromJson(item);
    }).toList()),
    verticalJumpPages: List.from(json['verticalJumpPages'].map((item) {
      return new VerticalJumpPages.fromJson(item);
    }).toList()),
  );
}

class HorizontalJumpPages {
  final int id;
  final int red;
  final int isNeedLogin;
  final String link;
  final String? moreLink;
  final String logo;
  final String name;
  final String desc;
  final String title;
  final int order;
  final Windows? windows;
  final int taskId;
  final Null cardCategory;
  final Null nameStyle;

  HorizontalJumpPages({
    required this.id,
    required this.red,
    required this.isNeedLogin,
    required this.link,
    required this.moreLink,
    required this.logo,
    required this.name,
    required this.desc,
    required this.title,
    required this.order,
    required this.windows,
    required this.taskId,
    this.cardCategory,
    this.nameStyle,
  });

  factory HorizontalJumpPages.fromJson(Map<String, dynamic> json) =>
      _$HorizontalJumpPagesFromJson(json);

  HorizontalJumpPages from(Map<String, dynamic> json) =>
      _$HorizontalJumpPagesFromJson(json);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['red'] = this.red;
    data['isNeedLogin'] = this.isNeedLogin;
    data['link'] = this.link;
    data['moreLink'] = this.moreLink;
    data['logo'] = this.logo;
    data['name'] = this.name;
    data['desc'] = this.desc;
    data['title'] = this.title;
    data['order'] = this.order;
    if (this.windows != null) {
      data['windows'] = this.windows?.toJson();
    }
    data['taskId'] = this.taskId;
    data['cardCategory'] = this.cardCategory;
    data['nameStyle'] = this.nameStyle;
    return data;
  }
}

HorizontalJumpPages _$HorizontalJumpPagesFromJson(Map<String, dynamic> json) {
  return HorizontalJumpPages(
    id: json['id'],
    red: json['red'],
    isNeedLogin: json['isNeedLogin'],
    link: json['link'],
    moreLink: json['moreLink'],
    logo: json['logo'],
    name: json['name'],
    desc: json['desc'],
    title: json['title'],
    order: json['order'],
    windows:
        json['windows'] != null ? new Windows.fromJson(json['windows']) : null,
    taskId: json['taskId'],
    cardCategory: json['cardCategory'],
    nameStyle: json['nameStyle'],
  );
}

class Windows {
  final Null content;
  final String? btnText;
  final String? img;

  Windows({
    this.content,
    required this.btnText,
    required this.img,
  });

  factory Windows.fromJson(Map<String, dynamic> json) =>
      _$WindowsFromJson(json);

  Windows from(Map<String, dynamic> json) => _$WindowsFromJson(json);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['content'] = this.content;
    data['btnText'] = this.btnText;
    data['img'] = this.img;
    return data;
  }
}

Windows _$WindowsFromJson(Map<String, dynamic> json) {
  return Windows(
    content: json['content'],
    btnText: json['btnText'],
    img: json['img'],
  );
}

class VerticalJumpPages {
  final int id;
  final int red;
  final int isNeedLogin;
  final String link;
  final String moreLink;
  final String logo;
  final String name;
  final String desc;
  final String title;
  final int order;
  final Windows? windows;
  final int taskId;
  final int cardCategory;
  final int nameStyle;

  VerticalJumpPages({
    required this.id,
    required this.red,
    required this.isNeedLogin,
    required this.link,
    required this.moreLink,
    required this.logo,
    required this.name,
    required this.desc,
    required this.title,
    required this.order,
    required this.windows,
    required this.taskId,
    required this.cardCategory,
    required this.nameStyle,
  });

  factory VerticalJumpPages.fromJson(Map<String, dynamic> json) =>
      _$VerticalJumpPagesFromJson(json);

  VerticalJumpPages from(Map<String, dynamic> json) =>
      _$VerticalJumpPagesFromJson(json);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['red'] = this.red;
    data['isNeedLogin'] = this.isNeedLogin;
    data['link'] = this.link;
    data['moreLink'] = this.moreLink;
    data['logo'] = this.logo;
    data['name'] = this.name;
    data['desc'] = this.desc;
    data['title'] = this.title;
    data['order'] = this.order;
    if (this.windows != null) {
      data['windows'] = this.windows?.toJson();
    }
    data['taskId'] = this.taskId;
    data['cardCategory'] = this.cardCategory;
    data['nameStyle'] = this.nameStyle;
    return data;
  }
}

VerticalJumpPages _$VerticalJumpPagesFromJson(Map<String, dynamic> json) {
  return VerticalJumpPages(
    id: json['id'],
    red: json['red'],
    isNeedLogin: json['isNeedLogin'],
    link: json['link'],
    moreLink: json['moreLink'],
    logo: json['logo'],
    name: json['name'],
    desc: json['desc'],
    title: json['title'],
    order: json['order'],
    windows:
        json['windows'] != null ? new Windows.fromJson(json['windows']) : null,
    taskId: json['taskId'],
    cardCategory: json['cardCategory'],
    nameStyle: json['nameStyle'],
  );
}
