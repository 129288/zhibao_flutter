import 'package:get/get.dart';
import 'package:zhibao_flutter/api/home/home_entity.dart';
import 'package:zhibao_flutter/api/home/home_view_model.dart';
import 'package:zhibao_flutter/util/check/check.dart';
import 'package:zhibao_flutter/widget/dialog/confirm_dialog.dart';
import 'package:zhibao_flutter/zone.dart';

class HomeLogic extends GetxController {
  RxString noticeText = "".obs;

  List<String> get tabListPreview => ['dailyTools'.tr, 'queryTools'.tr];
  List<String> get tabListDef => [
    'audioVideoEntertainment'.tr,
    'foodBeverage'.tr,
    'lifeServices'.tr,
    'transportation'.tr,
    'readingLearning'.tr,

    /// -------------
    // "商城",
    // "短剧",
    // "游戏",
    // "充话费",
    /// -------------
    // "精选好物",
    // "京东",
  ];

  List<String> get tabList =>
      Q1Data.hideMode.not() ? tabListDef : tabListPreview;

  // List<String> get tabList => Q1Data.userInfoEntity?.deleted == 1
  //     ? tabListDef
  //     : ([...tabListDef, "充话费", "游戏"]);

  List<String> tabKeyList = [
    "home_mock_shangcheng",
    "home_mock_duanju",
    "home_mock_youxi",
    "home_mock_huafei",
    // "精选好物",
    // "京东",
  ];
  RxInt currentIndex = 0.obs;

  get mockColumn {
    return [
      'audioVideoEntertainment'.tr,
      'foodBeverage'.tr,
      'lifeServices'.tr,
      'transportation'.tr,
      'readingLearning'.tr,

      /// ----------之前
      // "短剧",
      // "小说",
      // "游戏",
      // "充话费",
      // "精选好物",
      // "京东",
    ];
  }

  get mainContent {
    return {
      'dailyTools'.tr: [
        'todayInHistory'.tr,
        'hotSearchList'.tr,
        'angleDetection'.tr
      ],
      'queryTools'.tr: ["单位换算", "年龄计算器"],
      'audioVideoEntertainment'.tr:
          List.generate(10, (index) => "${'audioVideoEntertainment'.tr}$index"),
      'foodBeverage'.tr:
          List.generate(10, (index) => "${'foodBeverage'.tr}$index"),
      'lifeServices'.tr:
          List.generate(10, (index) => "${'lifeServices'.tr}$index"),
      'transportation'.tr:
          List.generate(10, (index) => "${'transportation'.tr}$index"),
      'readingLearning'.tr:
          List.generate(10, (index) => "${'readingLearning'.tr}$index"),
      'rechargePhoneBill'.tr: [
        '10PhoneBillRecharge'.tr,
        '100PhoneBillRecharge'.tr,
        '50ThreeNetworkPhoneBill'.tr,
        '500QuickCharge'.tr,
        '100RechargePhoneBill'.tr,
        'NationwidePhoneBillRecharge'.tr,
        '200TelecomPhoneBillRecharge'.tr,
        '100PhoneBillRecharge'.tr,
        '1InstantCharge10PhoneBill'.tr,
        'PrepaidPhoneBillToGetAMobilePhone'.tr,
      ],
      'shortDrama'.tr: [
        'ThreeBrigades'.tr,
        'DoYouKnowIfItShouldBeGreenFatAndRedThin'.tr,
        'LightingStore'.tr,
        'DetectiveConan'.tr,
        'PoisonOrAntidote'.tr,
        'SpoiledLove'.tr,
        'TheOnlyYouInTheWorld'.tr,
        'Unparalleled'.tr,
        'FemaleMasterIsAbove'.tr,
        'HeWhoMovesMyHeart'.tr,
      ],
      "Mall".tr: [
        'Lancome'.tr,
        'MilkVelvetBlanket'.tr,
        'CommemorativeBox'.tr,
        'CamelliaEssentialOil'.tr,
        'ThreePieceBedSet'.tr,
        'HighEndCrystalNecklace'.tr,
        'GinsengBirdsNest'.tr,
        'DisneyJuniorHighSchoolWatch'.tr,
        'WomensDayGift'.tr,
        'BestManGift'.tr,
      ],
      'game'.tr: [
        'PlayerUnknownsBattlegrounds'.tr,
        'ThunderGame'.tr,
        'ChivalrousConduct'.tr,
        'HonorOfKings'.tr,
        'Warrior'.tr,
        'GenshinImpact'.tr,
        'PlantsVsZombies'.tr,
        'SubwayParkour'.tr,
        'StarOfYuanDream'.tr,
        'EternalHell'.tr,
      ],
    };
  }

  RxList<HomeBannerEntity> bannerList = <HomeBannerEntity>[].obs;

  Worker? worker;

  @override
  void onReady() {
    super.onReady();
    getBanner();
    getNotice();
    worker = ever(AppConfig.currentLan, (callback) {
      getBanner();
      getNotice();
      update();
    });
  }

  Future getBanner() async {
    final value = await homeViewModel.getAdList(null, (v) {
      bannerList.value = v;
    });
    if (value.data == null) {
      return;
    }
    bannerList.value = value.data;
  }

  // Show the notice from api.
  Future getNotice() async {
    final value = await homeViewModel.noticeText(null);
    if (value.data == null) {
      return;
    }
    if (value.data is String && strNoEmpty(value.data)) {
      noticeText.value = value.data;
      // confirmDialog(Get.context!, text: value.data);
    }
  }

  void changeTab(int value) {
    currentIndex.value = value;
  }

  @override
  void onClose() {
    super.onClose();
    worker?.dispose();
    worker = null;
  }
}
