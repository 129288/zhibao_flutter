import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zhibao_flutter/pages/lian/lian_view.dart';
import 'package:zhibao_flutter/widget/appbar/commom_bar.dart';
import 'package:zhibao_flutter/widget/base/my_scaffold.dart';

import 'game_logic.dart';

class GamePage extends StatelessWidget {
  final logic = Get.find<GameLogic>();

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      body: CoverTabPage("assets/images/bottom/bg_stay_toned_game.png"),
    );
  }
}
