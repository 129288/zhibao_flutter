import 'package:get/get.dart';

import 'lian_logic.dart';

class LianBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => LianLogic());
  }
}
