import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:zhibao_flutter/widget/appbar/commom_bar.dart';
import 'package:zhibao_flutter/widget/base/my_scaffold.dart';
import 'package:zhibao_flutter/zone.dart';

import 'lian_logic.dart';

class LianPage extends StatelessWidget {
  final RxInt currentIndex;

  LianPage(this.currentIndex);

  final logic = Get.find<LianLogic>();

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => MyScaffold(
        checkDoubleExit: currentIndex.value == 1,
        body: CoverTabPage("assets/images/bottom/shop_cover.jpg"),
        // body: CoverTabPage("assets/images/bottom/bg_stay_toned_lian.png"),
      ),
    );
  }
}

class CoverTabPage extends StatelessWidget {
  final String value;

  CoverTabPage(this.value);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        SafeArea(
          child: Image.asset('$value',
              width: Get.width, height: Get.height, fit: BoxFit.cover),
        ),

        /// Because the `即将开放` have been pasted on the picture.
        Container(
          width: 300.px,
          height: 58.px,
          color: Color(0xff171616),
          alignment: Alignment.center,
          child: Text(
            'ComingSoon'.tr,
            style: TextStyle(
              color: Colors.white,
              fontSize: 20.px,
              fontWeight: MyFontWeight.bold,
            ),
          ),
        )
      ],
    );
  }
}
