import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zhibao_flutter/pages/other/main/main_logic.dart';
import 'package:zhibao_flutter/util/config/app_config.dart';
import 'package:zhibao_flutter/util/data/actions.dart';
import 'package:zhibao_flutter/util/data/notice.dart';
import 'package:zhibao_flutter/util/data/q1_data.dart';
import 'package:zhibao_flutter/util/float/fram_size.dart';
import 'package:zhibao_flutter/util/my_theme.dart';
import 'package:zhibao_flutter/widget/base/my_scaffold.dart';
import 'package:get/get.dart';
import 'package:zhibao_flutter/widget/fun/check_double_exit.dart';

class MainPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MainPageState();
}

class MainPageState extends State<MainPage> with AutomaticKeepAliveClientMixin {
  final logic = Get.find<MainLogic>();

  BottomNavigationBarItem itemBuild(item) {
    TabBarModel model = item;
    var _selectIcon = model.selectIcon;
    return BottomNavigationBarItem(
      icon: model.icon,
      activeIcon: _selectIcon,
      label: model.title,
    );
  }

  BottomNavigationBar bottomNavigationBar(
      Color selectedItemColor, int currentIndex) {
    return BottomNavigationBar(
      items: logic.pages().map(itemBuild).toList(),
      type: BottomNavigationBarType.fixed,
      currentIndex: currentIndex,
      backgroundColor: Colors.white,
      unselectedItemColor: MyTheme.unselectedItemColor(),
      selectedItemColor: selectedItemColor,
      selectedFontSize: 10.px,
      selectedLabelStyle: const TextStyle(fontWeight: FontWeight.w600),
      unselectedFontSize: 10.px,
      onTap: (int index) async {
        // FloatReporterMenu.repoterRemoveOverlayEntry();
        logic.tapBottomNav(index);
      },
      iconSize: 24,
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return MyScaffold(
      bottomNavigationBar: Obx(() => bottomNavigationBar(
          MyTheme.mainColor.value, logic.currentIndex.value)),
      body: PageView(
        physics: const NeverScrollableScrollPhysics(),
        controller: logic.pageController,
        children: logic.pages().map((item) {
          return item.page;
        }).toList(),
        onPageChanged: (index) {
          Notice.send(Q1Actions.toTabBarIndex(), index);
        },
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class TabBarModel {
  const TabBarModel({
    required this.title,
    required this.page,
    required this.icon,
    required this.selectIcon,
  });

  final String title;
  final Widget icon;
  final Widget selectIcon;
  final Widget page;
}
