import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:zhibao_flutter/api/commom/common_view_model.dart';
import 'package:zhibao_flutter/gen/assets.gen.dart';
import 'package:zhibao_flutter/pages/game/game_view.dart';
import 'package:zhibao_flutter/pages/home/home_view.dart';
import 'package:zhibao_flutter/pages/lian/lian_view.dart';
import 'package:zhibao_flutter/pages/mine/mine_view.dart';
import 'package:zhibao_flutter/pages/other/main/main_view.dart';
import 'package:zhibao_flutter/pages/other/web_view_page.dart';
import 'package:zhibao_flutter/util/config/page_util.dart';
import 'package:zhibao_flutter/util/data/actions.dart';
import 'package:zhibao_flutter/util/data/notice.dart';
import 'package:zhibao_flutter/util/func/log.dart';
import 'package:zhibao_flutter/zone.dart';
import 'package:get/get.dart';

class MainLogic extends GetxController {
  late RxInt currentIndex = 0.obs;
  PageController pageController = PageController();

  RxInt msgCount = 0.obs;

  int lastIndex = 0;

  @override
  void onInit() {
    super.onInit();
    Notice.addListener(Q1Actions.toTabBarIndexLast(), (_) {
      LogUtil.d("Q1Actions.toTabBarIndexLast()::lastIndex is ${lastIndex}");
      currentIndex.value = lastIndex;
      pageController.jumpToPage(currentIndex.value);
    });
    Notice.addListener(Q1Actions.toTabBarIndex(), (index) {
      if (kIsWeb && index == 2) {
        PageUtil.openNewLinkWhenWeb(AppConfig.gameLink);
        return;
      }
      currentIndex.value = index;
      pageController.jumpToPage(index);
    });

    Future.delayed(const Duration(seconds: 1)).then((value) async {
      var connectivityResult = (Connectivity().checkConnectivity());
      if (await connectivityResult == ConnectivityResult.none) {
        return;
      }
      // commonViewModel.appActive(null,
      //     idfa: Q1DataRequest.deviceId,
      //     openudid: Q1Data.loginRspEntity?.uid ?? "");
    });

    checkVersion();
  }

  Future checkVersion() async {
    await commonViewModel.checkVersion(Get.context!, false);
  }

  void tapBottomNav(int index) {
    if (!strNoEmpty(pages()[index].title)) {
      return;
    }
    if (kIsWeb && index == 2) {
      PageUtil.openNewLinkWhenWeb(AppConfig.gameLink);
      return;
    }
    lastIndex = currentIndex.value;
    LogUtil.d("【lastIndex】lastIndex new value is ${lastIndex}");
    currentIndex.value = index;
    pageController.jumpToPage(index);
  }

  Widget pageBuild(item) {
    TabBarModel model = item;
    return model.page;
  }

  List<TabBarModel> pages() {
    return [
      TabBarModel(
        title: 'home'.tr,
        icon: Image.asset('assets/images/bottom/bottom_home_c.png', width: 35),
        selectIcon:
            Image.asset('assets/images/bottom/bottom_home_s.png', width: 35),
        page: HomePage(currentIndex),
      ),
      if (Q1Data.hideMode.not())
        TabBarModel(
          title: 'taowoLife'.tr,
          icon: Image.asset(Assets.images.bottom.bottomShopC.path, width: 35),
          selectIcon:
              Image.asset(Assets.images.bottom.bottomShopS.path, width: 35),
          page: LianPage(currentIndex),
        ),
      if (Q1Data.hideMode.not())
        TabBarModel(
          title: 'game'.tr,
          icon:
              Image.asset('assets/images/bottom/bottom_game_c.png', width: 35),
          selectIcon:
              Image.asset('assets/images/bottom/bottom_game_s.png', width: 35),
          // page: GamePage(),
          page: WebViewPage(
              link: AppConfig.gameLink,
              showAppBar: false,
              mainPage: true,
              currentIndex: currentIndex),
        ),
      TabBarModel(
        title: 'my'.tr,
        icon: Image.asset('assets/images/bottom/bottom_mine_c.png', width: 35),
        selectIcon:
            Image.asset('assets/images/bottom/bottom_mine_s.png', width: 35),
        page: MinePage(currentIndex),
      ),
    ];
  }

  @override
  void onClose() {
    super.onClose();
    Notice.removeListenerByEvent(Q1Actions.toTabBarIndexLast());
    Notice.removeListenerByEvent(Q1Actions.toTabBarIndex());
  }
}
