import 'package:flutter/material.dart';
import 'package:get/get_utils/src/extensions/export.dart';

// 主页无网络空白页面
class HomeWithoutInternetPage extends StatefulWidget {
  final VoidCallback onLoad;
  final double? topMargin;

  const HomeWithoutInternetPage(this.onLoad, {this.topMargin, Key? key})
      : super(key: key);

  @override
  State<HomeWithoutInternetPage> createState() =>
      _HomeWithoutInternetPageState();
}

class _HomeWithoutInternetPageState extends State<HomeWithoutInternetPage>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.only(top: widget.topMargin ?? 150),
            child: Image.asset(
              "assets/images/ic_empty.png",
              width: 175,
              fit: BoxFit.fill,
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 35.5),
            child: Text(
              'noInternet'.tr,
              style: TextStyle(color: Color(0xFF61605F), fontSize: 16),
            ),
          ),
          InkWell(
            onTap: () {
              widget.onLoad();
            },
            child: Container(
              margin: EdgeInsets.only(top: 45),
              height: 42,
              width: 127,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25),
                gradient: LinearGradient(
                    begin: Alignment.centerRight,
                    end: Alignment.centerLeft,
                    colors: [
                      Color(0xffFF8800),
                      Color(0xffFF7F00),
                      Color(0xffFF5000),
                    ]),
              ),
              child: Text(
                'reload'.tr,
                style: TextStyle(
                  fontSize: 15,
                  color: Color(0xffffffff),
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
