import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zhibao_flutter/widget/appbar/commom_bar.dart';
import 'package:zhibao_flutter/widget/base/my_scaffold.dart';
import 'package:zhibao_flutter/zone.dart';

import 'upgrade_logic.dart';

class UpgradePage extends StatelessWidget {
  final logic = Get.find<UpgradeLogic>();

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      appBar: CommomBar(
        title: "upgrade".tr,
      ),
      body: GetBuilder<UpgradeLogic>(
        builder: (UpgradeLogic logic) {
          return ListView(
            padding: EdgeInsets.symmetric(horizontal: 14.px),
            children: [
              SizedBox(height: (27 - 13).px),
              Text(
                'upgradeVIP'.tr,
                style: TextStyle(
                  color: Color(0xff000000),
                  fontSize: 24.px,
                  fontWeight: MyFontWeight.bold,
                ),
              ),

              /// 不需要下面的筛选器那就加点边距。
              SizedBox(height: 20.px),

              // Second 2024/02/18 12:07 AM
              // 这三个直接不要
              // Second 2024/02/18 12:07 AM
              // 这三个直接不写即可
              // Padding(
              //   padding: EdgeInsets.symmetric(vertical: 20.px),
              //   child: Row(
              //     children: () {
              //       final style =
              //           TextStyle(color: Color(0xff9DA4B2), fontSize: 12.01.px);
              //       return [
              //         /// todo Need show filter icon and change effect.
              //         Expanded(
              //           child: Text("销量", style: style),
              //         ),
              //         Expanded(
              //           child: Center(
              //             child: Text("价格", style: style),
              //           ),
              //         ),
              //         Expanded(
              //           child: Align(
              //             alignment: Alignment.centerRight,
              //             child: Text("菜单", style: style),
              //           ),
              //         ),
              //       ];
              //     }(),
              //   ),
              // ),
              Wrap(
                spacing: 10.px,
                runSpacing: 20.px,
                children: List.generate(
                  AppConfig.maxLevel,
                  (index) {
                    final levelValue = index + 1;
                    final bool canUpgrade = (Q1Data.grade + 1) == levelValue;
                    final bool upgraded = (Q1Data.grade + 1) > levelValue;
                    final width = (375 - 28 - 10).px / 2;
                    return InkwellWithOutColor(
                      child: Container(
                        width: width,
                        height: width * (235 / 168),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                              BorderRadius.all(Radius.circular(12.px)),
                          boxShadow: MyTheme.shadowGrey,
                        ),
                        child: Column(
                          children: [
                            Expanded(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  ClipRRect(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(3.px)),
                                    child: ColorFiltered(
                                      colorFilter: ColorFilter.mode(
                                          (Q1Data.grade + 1) < levelValue
                                              ? Colors.grey
                                              : Colors.transparent,
                                          BlendMode.color),
                                      child: Image.asset(
                                        'assets/images/upgrade/ic_upgrade_mini_$levelValue.png',
                                        width: 75.86.px,
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: 14.px),
                                  Text(
                                    'NO$levelValue',
                                    style: TextStyle(
                                      color: Color(0xff171616),
                                      fontSize: 18.px,
                                      fontWeight: MyFontWeight.bold,
                                    ),
                                  ),
                                  Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 10.px),
                                    child: Text(
                                      levelValue == AppConfig.maxLevel
                                          ? "highestLevel".tr
                                          : 'upgradeToEarnMore'.trParams({
                                              "levelValue": "${levelValue + 1}"
                                            }),
                                      style: TextStyle(
                                        color: Color(0xff7D756C),
                                        fontSize: 12.px,
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: 8.px),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 10.px),
                              height: 39.px,
                              decoration: BoxDecoration(
                                color: (Q1Data.grade + 1) >= levelValue
                                    ? Color(0xfffcf2ec)
                                    : Colors.grey.withOpacity(0.2),
                                borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(12.px),
                                  bottomRight: Radius.circular(12.px),
                                ),
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    '$levelValue${"level".tr}',
                                    style: TextStyle(
                                      color: Color(0xff000000),
                                      fontSize: 14.px,
                                      fontWeight: MyFontWeight.medium,
                                    ),
                                  ),
                                  SmallButton(
                                    /// For localization.[Mar 2 2024]
                                    // width: 52.px,
                                    height: 24.px,

                                    color: (Q1Data.grade + 1) == levelValue
                                        ? MyTheme.themeColor()
                                        : Colors.grey,

                                    /// For localization.[Mar 2 2024]
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 10.px),
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(22.px)),
                                    useInkwell: true,
                                    onPressed: () {
                                      if (canUpgrade) {
                                        logic.toDet(index, levelValue);
                                      }
                                    },
                                    child: Text(
                                      upgraded ? "already".tr : "goUpgrade".tr,
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 12.px,
                                        fontWeight: MyFontWeight.medium,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      onTap: () {
                        logic.toDet(index, levelValue);
                      },
                    );
                  },
                ),
              ),
              SizedBox(height: 60.px),
            ],
          );
        },
      ),
    );
  }
}
