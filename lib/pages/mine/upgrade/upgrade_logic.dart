import 'package:get/get.dart';
import 'package:zhibao_flutter/api/mine/mine_view_model.dart';
import 'package:zhibao_flutter/zone.dart';

class UpgradeLogic extends GetxController {
  Future toDet(int index, int levelValue) async {
    if (levelValue > (Q1Data.grade + 1)) {
      return;
    }

    final value =
        await Get.toNamed(RouteConfig.upgradeDetPage, arguments: index + 1);
    if (value == 1) {
      await mineViewModel.getSelfInfo(null, Q1Data.loginRspEntity!.token!);
      update();
    }
  }
}
