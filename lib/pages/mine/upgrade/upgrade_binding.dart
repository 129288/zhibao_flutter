import 'package:get/get.dart';

import 'upgrade_logic.dart';

class UpgradeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => UpgradeLogic());
  }
}
