import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:zhibao_flutter/widget/appbar/commom_bar.dart';
import 'package:zhibao_flutter/widget/base/my_scaffold.dart';
import 'package:zhibao_flutter/zone.dart';

import 'invite_logic.dart';

/// 邀请好友、分享
class InvitePage extends StatelessWidget {
  final logic = Get.find<InviteLogic>();

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      appBar: CommomBar(title: "share".tr),
      body: Column(
        children: [
          Expanded(
            child: ListView(
              children: [
                SizedBox(height: (29 - 18).px),
                Center(
                  child: Container(
                    width: 346.px,
                    height: 413.px,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(
                        Radius.circular(20.px),
                      ),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'shareInviteFriends'.tr,
                          style: TextStyle(
                            color: Color(0xff000000),
                            fontSize: 16.px,
                            fontWeight: MyFontWeight.medium,
                          ),
                        ),
                        SizedBox(height: 12.px),
                        Center(
                          child: QrImageView(
                              data: "https://h5.zb360.pancakeman.xyz/#/"
                                  "${RouteConfig.registerPage}?"
                                  "${AppConfig.inviterKey}=${Q1Data.qrDataInvite}",
                              size: 167.px),
                        ),
                        SizedBox(height: 16.px),
                        Row(
                          children: [
                            SizedBox(width: 14.px),
                            Expanded(child: HorizontalLine()),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 5.px),
                              child: Text('or'),
                            ),
                            Expanded(child: HorizontalLine()),
                            SizedBox(width: 14.px),
                          ],
                        ),
                        SizedBox(height: 28.px),
                        Text(
                          'yourInviteCode'.tr,
                          style: TextStyle(
                            color: Color(0xff2A2823),
                            fontSize: 16.px,
                            fontWeight: MyFontWeight.medium,
                          ),
                        ),
                        SizedBox(height: 17.px),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            InkwellWithOutColor(
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 30.px, vertical: 12.px),
                                decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.px)),
                                  border: Border.all(
                                    color: Color(0xffE2E2E2),
                                    width: 1.px,
                                  ),
                                ),
                                child: Text(
                                  Q1Data.inviteCode,
                                  style: TextStyle(
                                    color: Color(0xff000000),
                                    fontSize: 14.px,
                                  ),
                                ),
                              ),
                              onTap: () {
                                copyText(Q1Data.inviteCode);
                              },
                            ),
                            SizedBox(width: 4.px),
                            InkwellWithOutColor(
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 30.px, vertical: 12.px),
                                decoration: BoxDecoration(
                                  color: Color(0xffFFF6EC),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.px)),
                                  border: Border.all(
                                    color: Color(0xffF37135),
                                    width: 1.px,
                                  ),
                                ),
                                child: Text(
                                  'copy'.tr,
                                  style: TextStyle(
                                    color: Color(0xffF37135),
                                    fontSize: 14.px,
                                  ),
                                ),
                              ),
                              onTap: () {
                                copyText(Q1Data.inviteCode);
                              },
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 80.px),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
