import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:zhibao_flutter/api/wallet/wallet_view_model.dart';
import 'package:zhibao_flutter/pages/mine/mine_logic.dart';
import 'package:zhibao_flutter/util/check/check.dart';
import 'package:zhibao_flutter/util/config/app_config.dart';
import 'package:zhibao_flutter/util/ui/my_toast.dart';

class TransferLogic extends GetxController {
  // TextEditingController typeEditController = TextEditingController();
  TextEditingController targetController =
      TextEditingController(text: AppConfig.mockTarget);
  TextEditingController amountController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController codeC = TextEditingController();

  FocusNode codeF = FocusNode();
  FocusNode targetFocusNode = FocusNode();
  FocusNode amountFocusNode = FocusNode();
  FocusNode passwordFocusNode = FocusNode();

  Future commit() async {
    if (!strNoEmpty(targetController.text)) {
      myToast('enterTargetMember'.tr);
      targetFocusNode.requestFocus();
    } else if (!strNoEmpty(amountController.text)) {
      myToast('enterTransferAmount'.tr);
      amountFocusNode.requestFocus();
    } else if (!strNoEmpty(codeC.text)) {
      myToast('enterVerificationCode'.tr);
      codeF.requestFocus();
    } else if (!strNoEmpty(passwordController.text)) {
      myToast('enterTransactionPassword'.tr);
      passwordFocusNode.requestFocus();
    } else {
      final value = await walletViewModel.walletTransfer(
        Get.context!,
        amount: double.parse(amountController.text),
        distPhone: targetController.text,
        password: passwordController.text,
        sms: codeC.text,
      );
      if (value.data == null) {
        return;
      }
      Get.back();
      myToast('operationSuccessful'.tr);
      final MineLogic? logic = Get.find<MineLogic>();
      if (logic != null) {
        logic.refreshMine();
      }
    }
  }
}
