import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zhibao_flutter/pages/login/login_view.dart';
import 'package:zhibao_flutter/pages/mine/bill/bill_logic.dart';
import 'package:zhibao_flutter/pages/mine/bill/bill_view.dart';
import 'package:zhibao_flutter/widget/appbar/commom_bar.dart';
import 'package:zhibao_flutter/widget/base/my_scaffold.dart';
import 'package:zhibao_flutter/widget/data/get_code_bt.dart';
import 'package:zhibao_flutter/zone.dart';

import 'transfer_logic.dart';

class TransferPage extends StatelessWidget {
  final logic = Get.find<TransferLogic>();

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      appBar: CommomBar(
        title: "transfer".tr,
        rightDMActions: [
          TextButton(
            onPressed: () {
              Get.toNamed(RouteConfig.billPage, arguments: {
                BillConfig.useParamKey: BillTypeFromEnum.transfers,
              });
            },
            child: Text('transferRecord'.tr),
          ),
          SizedBox(width: 10.px),
        ],
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 16.px),
        children: [
          SizedBox(height: (29 - 16).px),
          Container(
            padding: EdgeInsets.all(16.px),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(
                Radius.circular(10.px),
              ),
              boxShadow: MyTheme.shadowGrey,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: () {
                final style = TextStyle(
                  color: Color(0xff000000),
                  fontSize: 16.px,
                  fontWeight: MyFontWeight.medium,
                );
                return [
                  Text('currentPoints'.tr, style: style),
                  Text("${Q1Data.userInfoEntity?.balance ?? 0.0}", style: style)
                ];
              }(),
            ),
          ),
          SizedBox(height: 16.px),
          TransferInput(
              "targetMember".tr,
              "ic_transfer_target.svg",
              logic.targetController,
              logic.targetFocusNode,
              (v) => logic.amountFocusNode.requestFocus()),
          TransferInput(
            "transferAmount".tr,
            "ic_transfer_amount.svg",
            logic.amountController,
            logic.amountFocusNode,
            (v) => logic.codeF.requestFocus(),
          ),
          TextFieldOfLogin(
            "verificationCode".tr,
            "ic_register_sms_code",
            logic.codeC,
            focusNode: logic.codeF,
            keyboardType: TextInputType.number,
            inputFormatters: textFormatterNumSmsCode,
            onSubmitted: (v) {
              logic.passwordFocusNode.requestFocus();
            },
            rWidget: GetCodeBt(
              mobilePhone: TextEditingController(text: Q1Data.phone),
              sence: "transfer",
              codeFocusNode: logic.codeF,
              loginType: LoginType.none,
            ),
          ),
          SizedBox(height: 10.px),
          TransferInput(
            "transactionPassword".tr,
            "ic_transfer_password.svg",
            logic.passwordController,
            logic.passwordFocusNode,
            (v) => logic.commit(),
          ),
          InkwellWithOutColor(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                SizedBox(width: 10.px),
                Text(
                  'modifyTransactionPassword'.tr,
                  style: TextStyle(
                    color: Colors.grey,
                  ),
                ),
                SizedBox(width: 5.px),
                Icon(
                  Icons.arrow_forward_ios_rounded,
                  size: 16.px,
                  color: Colors.grey,
                ),
                SizedBox(width: 10.px),
              ],
            ),
            onTap: () {
              Get.toNamed(RouteConfig.forgetPayPwPage);
            },
          ),
          SizedBox(height: 30.px),
          SizedBox(height: (46 - 16).px),
          SmallButton(
            onPressed: () {
              logic.commit();
            },
            width: 346.px,
            height: 64.px,
            borderRadius: BorderRadius.all(
              Radius.circular(64.px / 2),
            ),
            child: Text('submit'.tr),
          )
        ],
      ),
    );
  }
}

class TransferInput extends StatefulWidget {
  final String title;
  final String icon;
  final TextEditingController controller;
  final FocusNode focusNode;
  final ValueChanged<String> onSubmitted;

  TransferInput(
      this.title, this.icon, this.controller, this.focusNode, this.onSubmitted);

  @override
  State<TransferInput> createState() => _TransferInputState();
}

class _TransferInputState extends State<TransferInput> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.title,
          style: TextStyle(
            color: Color(0xff848AA4),
            fontSize: 16.px,
            fontWeight: MyFontWeight.medium,
          ),
        ),
        SizedBox(height: 12.px),
        Container(
          height: 60.px,
          padding: EdgeInsets.symmetric(horizontal: 10.px),
          decoration: BoxDecoration(
            border: Border.all(color: Color(0xffB7BAC2), width: 1.5.px),
            borderRadius: BorderRadius.all(Radius.circular(10.px)),
          ),
          child: Row(
            children: [
              SvgPicture.asset(
                'assets/images/mine/${widget.icon}',
                width: 40.px,
              ),
              SizedBox(width: 10.px),
              Expanded(
                child: TextField(
                  controller: widget.controller,
                  obscureText: widget.title == 'transactionPassword'.tr,
                  keyboardType: widget.title == 'transferAmount'.tr
                      ? TextInputType.number
                      : TextInputType.text,
                  inputFormatters: widget.title == 'transferAmount'.tr
                      ? numAndDotFormatter
                      : [],
                  decoration: InputDecoration(
                    hintText: '${"PleaseEnter".tr}${widget.title}'.tr,
                    border: InputBorder.none,
                  ),
                  focusNode: widget.focusNode,
                  textInputAction: TextInputAction.next,
                  onSubmitted: widget.onSubmitted,
                ),
              )
            ],
          ),
        ),
        SizedBox(height: 16.px),
      ],
    );
  }
}

////////////////////////////Label
// Second 2024/02/17 6:25 PM
// 去掉
//
// Second 2024/02/17 6:25 PM
// 不要这个,
//
// Second 2024/02/17 6:25 PM
// 直接是默认账户
//
// class TransferInputLabel extends StatefulWidget {
//   final GestureTapCallback? onTap;
//   final TextEditingController? controller;
//
//   TransferInputLabel(this.onTap, this.controller);
//
//   @override
//   State<TransferInputLabel> createState() => _TransferInputLabelState();
// }
//
// class _TransferInputLabelState extends State<TransferInputLabel> {
//   @override
//   Widget build(BuildContext context) {
//     return Column(
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: [
//         Text(
//           '转账类型',
//           style: TextStyle(
//             color: Color(0xff848AA4),
//             fontSize: 16.px,
//             fontWeight: MyFontWeight.medium,
//           ),
//         ),
//         SizedBox(height: 12.px),
//         Container(
//           height: 60.px,
//           padding: EdgeInsets.symmetric(horizontal: 10.px),
//           decoration: BoxDecoration(
//             border: Border.all(color: Color(0xffB7BAC2), width: 1.5.px),
//             borderRadius: BorderRadius.all(Radius.circular(10.px)),
//           ),
//           child: Row(
//             children: [
//               SvgPicture.asset(
//                 'assets/images/mine/ic_transfer_type.svg',
//                 width: 40.px,
//               ),
//               SizedBox(width: 10.px),
//               Expanded(
//                 child: TextField(
//                   enabled: false,
//                   controller: widget.controller,
//                   onTap: widget.onTap,
//                   decoration: InputDecoration(
//                     hintText: '请选择转账类型',
//                     border: InputBorder.none,
//                   ),
//                 ),
//               ),
//               SvgPicture.asset(
//                 'assets/images/mine/ic_transfer_arrow_down.svg',
//                 width: 15.px,
//               ),
//               SizedBox(width: 6.px),
//             ],
//           ),
//         ),
//         SizedBox(height: 16.px),
//       ],
//     );
//   }
// }
