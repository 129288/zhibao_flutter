import 'package:get/get.dart';

import 'charge_logic.dart';

class ChargeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ChargeLogic());
  }
}
