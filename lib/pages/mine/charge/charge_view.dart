import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:zhibao_flutter/pages/mine/withdraw/withdraw_view.dart';
import 'package:zhibao_flutter/zone.dart';

import 'charge_logic.dart';

class ChargePage extends StatelessWidget {
  final logic = Get.find<ChargeLogic>();

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      appBar: CommomBar(title: "rechargePage".tr),
      body: Column(
        children: [
          Expanded(
            child: ListView(
              children: [
                SizedBox(height: (29 - 18).px),
                Center(
                  child: Container(
                    width: 346.px,
                    height: 390.px,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(
                        Radius.circular(20.px),
                      ),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'rechargePage'.tr,
                          style: TextStyle(
                            color: Color(0xff000000),
                            fontSize: 16.px,
                            fontWeight: MyFontWeight.medium,
                          ),
                        ),
                        SizedBox(height: 12.px),
                        Center(
                          child: QrImageView(
                              data: Q1Data.qrDataAddress, size: 167.px),
                        ),
                        SizedBox(height: 16.px),
                        InkwellWithOutColor(
                          child: Container(
                            margin: EdgeInsets.symmetric(horizontal: 20.px),
                            padding: EdgeInsets.symmetric(
                                horizontal: 30.px, vertical: 12.px),
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.px)),
                              border: Border.all(
                                color: Color(0xffE2E2E2),
                                width: 1.px,
                              ),
                            ),
                            child: Text(
                              Q1Data.address,
                              style: TextStyle(
                                color: Color(0xff000000),
                                fontSize: 14.px,
                              ),
                            ),
                          ),
                          onTap: () {
                            copyText(Q1Data.address);
                          },
                        ),
                        AddressRechargeTips(),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 80.px),
              ],
            ),
          ),
          // Second 2024/02/21 11:22 PM
          // 去分享,    按钮都可以砍掉,   进入页面就行了,
          //
          // Second 2024/02/21 11:22 PM
          // 用户自己知道截图
          //
          // q1 2024/02/21 11:22 PM
          // 好
          //
          // Second 2024/02/21 11:22 PM
          // 点击复制能复制就行了
          //
          // q1 2024/02/21 11:23 PM
          // 两个分享页面都能复制
          //

          // SmallButton(
          //   margin: EdgeInsets.only(
          //       left: 16.px, right: 16.px, bottom: 31.px, top: 15.px),
          //   width: FrameSize.winWidth() - (16.px * 2),
          //   height: 64.px,
          //   borderRadius: BorderRadius.all(Radius.circular(50.px)),
          //   onPressed: () {
          //     myToast('弹出分享');
          //   },
          //   child: Text('去分享'),
          // )
        ],
      ),
    );
  }
}

class AddressRechargeTips extends StatelessWidget {
  const AddressRechargeTips({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: FrameSize.winWidth() - 100.px,
      child: Column(
        children: [
          SizedBox(height: 15.px),
          Text(
            'transferError'.tr,
            style: TextStyle(
              color: Colors.grey,
              fontSize: 12.px,
              fontWeight: FontWeight.w600,
            ),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}
