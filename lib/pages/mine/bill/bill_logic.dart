import 'package:flutter/material.dart';
import 'package:get/get.dart';

enum BillTypeFromEnum {
  // 全部
  all,
  // 收益
  income,
  // 充值
  recharge,
  // 提现
  withdraw,
  // 转让
  transfers,
}

class BillTabModel {
  final String title;
  final BillTypeFromEnum billType;
  final int? typeForServer;

  BillTabModel(this.title, this.billType, this.typeForServer);
}

class BillLogic extends GetxController {
  List<BillTabModel> tabs = [
    BillTabModel("income".tr, BillTypeFromEnum.income, 5),
    BillTabModel("recharge".tr, BillTypeFromEnum.recharge, 2),
    BillTabModel("transferOut".tr, BillTypeFromEnum.withdraw, 3),
    BillTabModel("transfer".tr, BillTypeFromEnum.transfers, 4),
    BillTabModel("all".tr, BillTypeFromEnum.all, null),
  ];
  late TabController tabC;
}