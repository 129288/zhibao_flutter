import 'package:get/get.dart';

import 'bill_logic.dart';

class BillBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => BillLogic());
  }
}
