import 'package:flutter/material.dart';
import 'package:zhibao_flutter/zone.dart';

class LanguageSetState {
  LanguageSetState() {
    ///Initialize variables
  }
}

class LanModel {
  final Locale locale;
  final String name;

  LanModel(this.locale, this.name);
}
