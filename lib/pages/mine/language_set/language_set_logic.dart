import 'package:get/get.dart';
import 'package:zhibao_flutter/zone.dart';

import 'language_set_state.dart';

class LanguageSetLogic extends GetxController {
  final LanguageSetState state = LanguageSetState();

  @override
  void onInit() {
    super.onInit();
    AppConfig.currentLan.value = AppConfig.locale;
  }
}
