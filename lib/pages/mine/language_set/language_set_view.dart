import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:zhibao_flutter/util/config/app_config.dart';
import 'package:zhibao_flutter/util/data/language.dart';
import 'package:zhibao_flutter/widget/appbar/commom_bar.dart';
import 'package:zhibao_flutter/widget/base/my_scaffold.dart';

import 'language_set_logic.dart';

class LanguageSetPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final logic = Get.find<LanguageSetLogic>();
    final state = Get.find<LanguageSetLogic>().state;

    return MyScaffold(
      appBar: CommomBar(title: "lanSet".tr),
      body: Column(
        children: LanguageUtil.lanList.map((e) {
          return Obx(
            () => ListTile(
              title: Text(e.name),
              trailing: AppConfig.currentLan.value == e.locale
                  ? const Icon(Icons.check)
                  : null,
              onTap: () {
                final Locale lan = e.locale;
                AppConfig.currentLan.value = lan;
                Get.updateLocale(lan);
                GetStorage box = GetStorage();
                box.write("languageCode", lan.languageCode);
                box.write("countryCode", lan.countryCode);
              },
            ),
          );
        }).toList(),
      ),
    );
  }
}
