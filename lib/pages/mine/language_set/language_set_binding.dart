import 'package:get/get.dart';

import 'language_set_logic.dart';

class LanguageSetBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => LanguageSetLogic());
  }
}
