import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:zhibao_flutter/util/check/check.dart';
import 'package:zhibao_flutter/zone.dart';

class MyLoginFieldConfig {
  static final bool showRedTipWhenTextNull = true;
}

class MyLoginField extends StatefulWidget {
  final GestureTapCallback? onTap;
  final String img;
  final double? imgRightMargin;
  final Widget? innerWidget;
  final int? maxLength;
  final String hintText;
  final TextEditingController controller;
  final FocusNode? focusNode;
  final ValueChanged<String>? onChanged;
  final bool? autofocus;
  final RxString violationStr;
  final double? imgWidth;
  final List<TextInputFormatter>? inputFormatters;

  /// Use to check the value change and hide [violationStr].
  final RxString? rxMainItem;

  const MyLoginField({
    Key? key,
    this.onTap,
    this.innerWidget,
    this.onChanged,
    this.autofocus,
    required this.violationStr,
    this.imgWidth,
    this.focusNode,
    this.imgRightMargin,
    this.maxLength,
    this.inputFormatters,
    required this.rxMainItem,
    required this.img,
    required this.hintText,
    required this.controller,
  }) : super(key: key);

  @override
  State<MyLoginField> createState() => _MyLoginFieldState();
}

class _MyLoginFieldState extends State<MyLoginField> {
  bool get showRedTip =>
      (MyLoginFieldConfig.showRedTipWhenTextNull ||
          strNoEmpty(widget.controller.text)) &&
      widget.innerWidget == null;

  StreamSubscription? subscription;

  @override
  void initState() {
    super.initState();
    if (widget.rxMainItem != null) {
      subscription = widget.rxMainItem!.listen((p0) {
        if (strNoEmpty(widget.violationStr.value)) {
          widget.violationStr.value = "";
        }
      });
    }
  }

  Widget mainContent(RxString? violationStrSelf) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(
              left: 15.px,
              right: 15.px,
              bottom: violationStrSelf == null ||
                      !strNoEmpty(violationStrSelf.value) ||
                      !showRedTip
                  ? 24.px
                  : 12.px),
          child: InkWell(
            onTap: () {
              if (widget.onTap != null) {
                widget.onTap!();
              }
            },
            child: Container(
              height: 60, //.px
              padding: EdgeInsets.only(left: 27.px),
              decoration: BoxDecoration(
                color: Color(0xffffffff),
                borderRadius: BorderRadius.circular(11.px),
                border: Border.all(
                  color: violationStrSelf != null &&
                          strNoEmpty(violationStrSelf.value)
                      ? Color(0xffFF5555)
                      : Colors.transparent,
                  width: 2,
                ),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(0, 2),
                    blurRadius: 5.px,
                    color: Color(0xff7E7E7E).withOpacity(0.15),
                  ),
                ],
              ),
              child: Row(
                children: [
                  Image.asset(
                    "${widget.img}",
                    width: widget.imgWidth ?? 17.px,
                  ),
                  SizedBox(width: widget.imgRightMargin ?? 23.5.px),
                  Expanded(
                    child: widget.innerWidget == null
                        ? TextField(
                            autofocus: widget.autofocus ?? false,
                            controller: widget.controller,
                            focusNode: widget.focusNode,
                            inputFormatters: widget.inputFormatters,
                            onChanged: (v) {
                              if (widget.onChanged != null)
                                widget.onChanged!(v);

                              /// Relate to [widget.rxMainItem] and [initState()].
                              if (violationStrSelf != null) {
                                violationStrSelf.value = "";
                              }

                              if (widget.maxLength != null) {
                                setState(() {});
                              }
                            },
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: widget.hintText,
                              hintStyle: TextStyle(
                                color: Color(0xffB9B9B9),
                                fontSize: 17, //.sp
                              ),
                            ),
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 17, //.sp
                            ),
                          )
                        : widget.innerWidget!,
                  ),
                  if (widget.maxLength != null)
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 14.5.px),
                      child: Text(
                        '${widget.controller.text.length}/${widget.maxLength}',
                        style: TextStyle(
                            color: Color(widget.controller.text.length >
                                    widget.maxLength!
                                ? 0xffFF5555
                                : 0xffB9B9B9),
                            fontSize: 14.px),
                      ),
                    ),
                  if (widget.innerWidget != null)
                    Padding(
                      padding: EdgeInsets.only(right: 22.px),
                      child: Image.asset(
                        "assets/images/ic_xgzl_label_n.png",
                        width: 7.px,
                      ),
                    ),
                ],
              ),
            ),
          ),
        ),
        if (violationStrSelf != null && showRedTip)
          () {
            if (strNoEmpty(violationStrSelf.value)) {
              return Padding(
                padding: EdgeInsets.only(bottom: 12.px),
                child: Row(
                  children: [
                    SizedBox(width: (24.5 - 10).px),
                    Image.asset("assets/images/ic_register_xh_n.png",
                        width: 10.px),
                    SizedBox(width: 10.px),
                    Text(
                      violationStrSelf.value,
                      style:
                          TextStyle(color: Color(0xffFF6B6B), fontSize: 13.px),
                    )
                  ],
                ),
              );
            } else {
              return Container();
            }
          }()
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    // if (widget.violationStr == null) {
    //   return mainContent(widget.violationStr);
    // }
    return Obx(() {
      return mainContent(widget.violationStr);
    });
  }

  @override
  void dispose() {
    super.dispose();
    subscription?.cancel();
    subscription = null;
  }
}
