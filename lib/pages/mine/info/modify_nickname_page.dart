import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:zhibao_flutter/api/mine/mine_view_model.dart';
import 'package:zhibao_flutter/pages/mine/info/my_login_field.dart';
import 'package:zhibao_flutter/util/check/check.dart';
import 'package:zhibao_flutter/zone.dart';

class ModifyNickNamePage extends StatefulWidget {
  const ModifyNickNamePage({Key? key}) : super(key: key);

  @override
  State<ModifyNickNamePage> createState() => _ModifyNickNamePageState();
}

class _ModifyNickNamePageState extends State<ModifyNickNamePage> {
  TextEditingController nameC = TextEditingController();
  FocusNode nameF = FocusNode();
  RxString violationStrForNickName = "".obs;

  String initContent = "";

  String count = ""; //textfield字符计数

  @override
  void initState() {
    super.initState();

    Map ar = Get.arguments as Map;
    if (ar.containsKey("nickName")) {
      nameC.text = ar['nickName'];
      initContent = nameC.text;
    }

    nameC.addListener(() {
      count = nameC.text;
      setState(() {});
    });
  }

  Future validateAllField() async {
    if (!strNoEmpty(nameC.text)) {
      violationStrForNickName.value = 'enterNickname'.tr;
    }
  }

  Future commit() async {
    validateAllField();
    if (!strNoEmpty(nameC.text)) {
      myToast('enterNickname'.tr);
      nameF.requestFocus();
      return;
    }

    if (strNoEmpty(initContent) && nameC.text == initContent) {
      Get.back(result: nameC.text);
      return;
    }

    // final value = await mineViewModel.checkNicknameExist(context,
    //     nickname: nameC.text, callViolation: (String tip) {
    //   LogUtil.d("mineViewModel.checkNicknameExist::callViolation::$tip");
    //   violationStrForNickName.value = tip;
    //   nameF.requestFocus();
    //   myToast(tip);
    // });
    // if (value.data == null) {
    //   return;
    // }
    // if (value.data == 1) {
    //   myToast("已存在的昵称");
    //   violationStrForNickName.value = "已存在的昵称";
    //   nameF.requestFocus();
    //   return;
    // }

    Get.back(result: nameC.text);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: SafeArea(
        child: MyScaffold(
          backgroundColor: Colors.white,
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: () => Navigator.pop(context),
                    child: Padding(
                      padding: EdgeInsets.only(
                        left: 15,
                        right: 15,
                        top: 13,
                        bottom: 12,
                      ),
                      child: Image.asset(
                        "assets/images/ic_nav_arrow_n.png",
                        width: 10,
                      ),
                    ),
                  ),
                  Text(
                    'modifyNickname'.tr,
                    style: TextStyle(
                      fontSize: 18,
                      color: Color(0xff181716),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      left: 15,
                      right: 15,
                      top: 13,
                      bottom: 12,
                    ),
                    child: SizedBox(width: 10),
                  ),
                ],
              ),
              SizedBox(height: 32.5),

              /// New version.
              MyLoginField(
                violationStr: violationStrForNickName,
                rxMainItem: null,
                img: "assets/images/ic_register_nickname_n.png",
                hintText: 'enterNickname'.tr,
                controller: nameC,
                focusNode: nameF,
                autofocus: true,
              ),

              /// Old version.
              // Container(
              //   height: 60,
              //   margin: EdgeInsets.symmetric(horizontal: 30),
              //   padding: EdgeInsets.only(left: 26.5),
              //   decoration: BoxDecoration(
              //     color: Color(0xffffffff),
              //     borderRadius: BorderRadius.circular(11),
              //     boxShadow: [
              //       BoxShadow(
              //         offset: Offset(0, 2.5),
              //         blurRadius: 6,
              //         color: Color(0xffB6B6B6).withOpacity(0.3),
              //       ),
              //     ],
              //   ),
              //   child: Row(
              //     children: [
              //       Image.asset(
              //         "assets/images/ic_register_nickname_n.png",
              //         width: 17,
              //       ),
              //       SizedBox(width: 13.5),
              //       Expanded(
              //         child: Container(
              //           padding: EdgeInsets.only(right: 32.5),
              //           child: TextField(
              //             nameC: nameC,
              //             maxLength: 10,
              //             autofocus: true,
              //             decoration: InputDecoration(
              //               counterText: '',
              //               contentPadding: EdgeInsets.all(0),
              //               hintText: '请输入你的昵称',
              //               border: OutlineInputBorder(),
              //               enabledBorder: OutlineInputBorder(
              //                 borderSide: BorderSide(color: Colors.transparent),
              //               ),
              //               focusedBorder: OutlineInputBorder(
              //                 borderSide: BorderSide(color: Colors.transparent),
              //               ),
              //               hintStyle: TextStyle(
              //                 color: Color(0xffB9B9B9),
              //                 fontSize: 17,
              //               ),
              //             ),
              //           ),
              //         ),
              //       ),
              //       Padding(
              //         padding: EdgeInsets.only(
              //           right: 14.5,
              //         ),
              //         child: Text(
              //           '${nameC.text.length}/10',
              //           style: TextStyle(
              //             fontSize: 14,
              //             color: Color(0xffB9B9B9),
              //           ),
              //         ),
              //       ),
              //     ],
              //   ),
              // ),
              // SizedBox(height: 15),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(
                      left: 24,
                      top: 4.5,
                      bottom: 4,
                    ),
                    child: Image.asset(
                      "assets/images/ic_register_xh_n.png",
                      width: 10,
                    ),
                  ),
                  SizedBox(width: 9.5),
                  Text(
                    'enterNickname'.tr,
                    style: TextStyle(
                      fontSize: 13,
                      color: Color(0xffB9B9B9),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 60.5),
              Padding(
                padding: EdgeInsets.only(right: 30, bottom: 39),
                child: InkWell(
                  onTap: () {
                    commit();
                  },
                  child: Container(
                    height: 50,
                    margin: EdgeInsets.only(left: 30),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      gradient: LinearGradient(
                          begin: Alignment.centerRight,
                          end: Alignment.centerLeft,
                          colors: [
                            Color(0xffFF8800),
                            Color(0xffFF7F00),
                            Color(0xffFF5000),
                          ]),
                    ),
                    child: Text(
                      'confirm'.tr,
                      style: TextStyle(
                        fontSize: 17,
                        color: nameC.text.length == 0
                            ? Color(0xffffffff).withOpacity(0.6)
                            : Color(0xffffffff),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
