import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zhibao_flutter/pages/mine/info/change_info_page.dart';
import 'package:zhibao_flutter/util/func/smart_refresh.dart';
import 'package:zhibao_flutter/widget/bt/small_border_bt.dart';
import 'package:zhibao_flutter/zone.dart';

import 'mine_logic.dart';

class MinePage extends StatelessWidget {
  final RxInt currentIndex;

  MinePage(this.currentIndex);

  final logic = Get.find<MineLogic>();

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => MyScaffold(
        checkDoubleExit: currentIndex.value == 3,
        appBar: CommomBar(
          title: 'my'.tr,
          leading: Container(),
        ),
        body: GetBuilder<MineLogic>(
          builder: (MineLogic controller) {
            final double sizeOfGrade = 18.px * 1.5;
            final double sizeOfAvatar = 58.px;
            final double verticalPadding = 12.px;
            final TextStyle incomeStyle = TextStyle(
              color: Colors.white,
              fontSize: 14.px,
              fontWeight: MyFontWeight.medium,
            );
            return MySmartRefresh(
              controller: logic.refreshController,
              onRefresh: () async {
                return logic.refreshMine();
              },
              child: ListView(
                padding: EdgeInsets.symmetric(horizontal: 14.px),
                children: [
                  Space(height: (25 - 18).px),
                  Stack(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(bottom: 20.px),
                        child: Row(
                          children: [
                            SwAvatar(
                              Q1Data.avatar,
                              size: sizeOfAvatar,
                              onTap: () async {
                                Get.to(ChangeInfoPage());
                              },
                            ),
                            SizedBox(width: 11.px),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    strNoEmpty(Q1Data.userInfoEntity?.name)
                                        ? Q1Data.userInfoEntity!.name!
                                        : "${Q1Data.userInfoEntity?.id ?? 'UserException'.tr}",
                                    style: TextStyle(
                                      color: Color(0xff171616),
                                      fontSize: 16.px,
                                      fontWeight: MyFontWeight.bold,
                                    ),
                                  ),
                                  if (Q1Data.hideMode.not())
                                    SizedBox(height: 2.px),
                                  if (Q1Data.hideMode.not())
                                    UnconstrainedBox(
                                      child: Container(
                                        decoration: BoxDecoration(
                                          color: Color(0xffF37135),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(16.px)),
                                        ),
                                        alignment: Alignment.center,
                                        height: 20.px,
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 8.px),
                                        child: Text(
                                          // 【2024 Feb 21 付介】
                                          // 推荐人ID就是邀请码，user/info 返回的
                                          // 【2024 feb 24】
                                          // 改成手机号/邮箱ID
                                          // '手机号/邮箱ID: ${hidePhone(Q1Data.userInfoEntity?.referrerPhone ?? "")}',
                                          /// 【feb 25 2024】.
                                          "${'Referrer'.tr}: ${hideCenter(Q1Data.userInfoEntity?.referrerPhone ?? '')}",
                                          // '推荐人ID: ${Q1Data.userInfoEntity?.id ?? "用户异常"}',
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 12.px,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                    )
                                ],
                              ),
                            ),

                            /// 【2024 Feb 27】
                            /// 去升级改色橘色底白色字
                            if (Q1Data.hideMode.not())
                              SmallButton(
                                onPressed: () {
                                  logic.onItemClick('GoUpgrade'.tr);
                                },

                                /// For localization.[Mar 2 2024]
                                // width: 90.px,
                                height: 40.px,

                                /// For localization.[Mar 2 2024]
                                padding:
                                    EdgeInsets.symmetric(horizontal: 10.px),
                                child: Text(
                                  'GoUpgrade'.tr,
                                  style: TextStyle(
                                    /// 【2024 Feb 27】
                                    /// 去升级改色橘色底白色字
                                    color: Colors.white,
                                    fontWeight: MyFontWeight.semBold,
                                    fontSize: 16.px,
                                  ),
                                ),
                              )
                          ],
                        ),
                      ),
                      Positioned(
                        left: sizeOfAvatar - sizeOfGrade,
                        bottom: sizeOfGrade / 2,
                        child: Q1Data.grade == 0
                            ? Container(
                                width: sizeOfGrade,
                                height: sizeOfGrade,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  color: Color(0xffF37135),
                                  shape: BoxShape.circle,
                                ),
                                child: Text(
                                  'V${Q1Data.grade}',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 12.px,
                                    height: 1,
                                  ),
                                ),
                              )
                            : Image.asset(
                                'assets/images/upgrade/ic_upgrade_mini_${Q1Data.grade}.png',
                                width: sizeOfGrade,
                              ),
                      )
                    ],
                  ),
                  Stack(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          color: Color(0xffF37135),
                          borderRadius:
                              BorderRadius.all(Radius.circular(20.px)),
                          boxShadow: MyTheme.shadowGrey,
                        ),
                        padding: EdgeInsets.only(
                            top: 27.px,
                            bottom: 27.px - verticalPadding,
                            left: 22.px,
                            right: 22.px),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              Q1Data.hideMode.not()
                                  ? "myPoints".tr
                                  : "myExperience".tr,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20.px,
                                  fontWeight: MyFontWeight.bold),
                            ),
                            SizedBox(height: 18.px - verticalPadding),
                            Row(
                              children: [
                                Expanded(
                                  flex: 3,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        Q1Data.hideMode.not() ? ' ' : "",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 10.px),
                                      ),
                                      SizedBox(
                                        width: FrameSize.winWidth() * 0.5,
                                        child: Text(
                                          Q1Data.hideMode.not()
                                              ? '${Q1Data.userInfoEntity?.balance ?? 0.0}'
                                              : "0",
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 28.px,
                                            fontWeight: MyFontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Space(width: 7.px),
                                Expanded(
                                  flex: 4,
                                  child: Align(
                                    alignment: Alignment.centerRight,
                                    child: Container(
                                      padding: EdgeInsets.only(
                                        left: 10.px,
                                        right: 5.px,
                                        top: verticalPadding,
                                        bottom: verticalPadding,
                                      ),
                                      decoration: BoxDecoration(
                                        border: Border.all(color: Colors.white),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5.px)),
                                      ),
                                      child: Row(
                                        children: Q1Data.hideMode.not()
                                            ? [
                                                Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Padding(
                                                      padding: EdgeInsets.only(
                                                          bottom: 5.px),
                                                      child: Text(
                                                          'dailyEarnings'.tr,
                                                          style: incomeStyle),
                                                    ),
                                                    Text('totalEarnings'.tr,
                                                        style: incomeStyle)
                                                  ],
                                                ),
                                                Expanded(
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Padding(
                                                        padding:
                                                            EdgeInsets.only(
                                                                bottom: 5.px),
                                                        child: Text(
                                                          '${Q1Data.userInfoEntity?.todayEarn ?? 0.0}',
                                                          style: incomeStyle,
                                                        ),
                                                      ),
                                                      Text(
                                                        // 【Feb 21 2024】付介：这两个先隐藏掉
                                                        // [Mar 9 2024] Bug list: show it.
                                                        '${Q1Data.userInfoEntity?.totalEarn ?? 0.0}',
                                                        style: incomeStyle,
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ]
                                            : [
                                                Text(
                                                  'GainMoreExperience'.tr,
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                )
                                              ],
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                      // 【Feb 21 2024】付介：这两个先隐藏掉
                      // 【Feb 27 2024】 Show the activity level.
                      Positioned(
                        right: 0,
                        top: 22.px,
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 14.px),
                          height: 30.px,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.horizontal(
                                left: Radius.circular(20.px)),
                          ),
                          alignment: Alignment.center,
                          child: Text(
                            "${'ActivityLevel'.tr}:${Q1Data.userInfoEntity?.activityLevel ?? 0}",
                            style: TextStyle(
                              color: Color(0xffF37135),
                              fontSize: 12.px,
                              fontWeight: MyFontWeight.semBold,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 20.px),
                  if (Q1Data.hideMode.not() && false)
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SmallButton(
                          height: 48.px,
                          width: 168.px,
                          boxShadow: MyTheme.shadowGrey,
                          onPressed: () {
                            logic.onItemClick("绿色积分");
                          },
                          child: Text('绿色积分'),
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.px)),
                        ),
                        SmallBorderBt(
                          onPressed: () {
                            logic.onItemClick("数字交易");
                          },
                          height: 48.px,
                          width: 168.px,
                          boxShadow: MyTheme.shadowGrey,
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.px)),
                          child: Text(
                            '钱包地址',
                            style: TextStyle(color: MyTheme.mainColor.value),
                          ),
                        )
                      ],
                    ),
                  if (Q1Data.hideMode.not()) SizedBox(height: 18.px),
                  Container(
                    decoration: BoxDecoration(
                      boxShadow: MyTheme.shadowGrey,
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10.px)),
                    ),
                    child: Column(
                      children: (Q1Data.hideMode.not()
                              ? logic.items
                              : logic.itemsOfHide)
                          .map((e) {
                        return InkwellWithOutColor(
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                vertical: 11.px, horizontal: 10.px),
                            child: Row(
                              children: [
                                SvgPicture.asset(
                                  'assets/images/mine/${e[0]}',
                                  width: 16.px,
                                ),
                                SizedBox(width: 10.px),
                                Expanded(
                                  child: Text(
                                    e[1],
                                    style: TextStyle(
                                        color: Color(0xff000000),
                                        fontSize: 12.px),
                                  ),
                                ),
                                SvgPicture.asset(
                                  'assets/images/mine/ic_mine_arrow_right.svg',
                                  width: 12.px,
                                ),
                              ],
                            ),
                          ),
                          onTap: () {
                            logic.onItemClick(e[1]);
                          },
                        );
                      }).toList(),
                    ),
                  ),
                  Center(
                    child: InkwellWithOutColor(
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: 30.px, horizontal: 20.px),
                        child: Text(
                          'Logout'.tr,
                          style: TextStyle(
                            color: Color(0xff7D756C),
                            fontSize: 12.px,
                            decoration: TextDecoration.underline,
                          ),
                        ),
                      ),
                      onTap: () {
                        logic.logout(context);
                      },
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
