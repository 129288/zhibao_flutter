import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:zhibao_flutter/dialog/price_dialog.dart';
import 'package:zhibao_flutter/pages/login/login_view.dart';
import 'package:zhibao_flutter/widget/appbar/commom_bar.dart';
import 'package:zhibao_flutter/widget/base/my_scaffold.dart';
import 'package:zhibao_flutter/widget/data/get_code_bt.dart';
import 'package:zhibao_flutter/zone.dart';

import 'withdraw_logic.dart';

class WithdrawPage extends StatelessWidget {
  final logic = Get.find<WithdrawLogic>();

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      appBar: CommomBar(title: "withdraw".tr),
      body: Column(
        children: [
          Expanded(
            child: ListView(
              padding: EdgeInsets.symmetric(horizontal: 32.px),
              children: [
                SizedBox(height: (29 - 16).px),
                Image.asset(
                  'assets/images/mine/ic_withdraw_head.png',
                  width: 70.15.px,
                  height: 70.15.px,
                ),
                SizedBox(height: 11.px),
                Center(
                  child: Text(
                    'withdraw'.tr,
                    style: TextStyle(
                      color: Color(0xff000000),
                      fontSize: 18.px,
                      fontWeight: MyFontWeight.semBold,
                    ),
                  ),
                ),
                SizedBox(height: 52.px),
                Text(
                  'amount'.tr,
                  style: TextStyle(
                    color: Color(0xff17172C),
                    fontSize: 18.px,
                    fontWeight: MyFontWeight.semBold,
                  ),
                ),
                SizedBox(height: 4.px),
                Text(
                  '${'currentBalance'.tr}\$${logic.balance}',
                  style: TextStyle(color: Color(0xffB7BAC2), fontSize: 15.px),
                ),
                SizedBox(height: 16.px),
                InkwellWithOutColor(
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 11.px),
                    decoration: BoxDecoration(
                      border: Border(
                        bottom:
                            BorderSide(color: Color(0xffDFDFDF), width: 2.px),
                      ),
                    ),
                    alignment: Alignment.center,
                    child: Obx(() {
                      return Text(
                        '\$${logic.textValue.value}',
                        style: TextStyle(
                          color: Color(0xff17172C),
                          fontSize: 32.px,
                          fontWeight: MyFontWeight.semBold,
                        ),
                      );
                    }),
                  ),
                  onTap: () async {
                    final result = await priceDialog(logic.textValue.value);
                    if (result != null && result != '') {
                      logic.textValue.value = result;
                    }
                  },
                ),
                SizedBox(height: 20.px),
                TextFieldOfLogin(
                  "withdrawalAddress".tr,
                  "ic_phone",
                  logic.addressC,
                  focusNode: logic.addressF,
                ),
                AddressWithdrawTips(),
                SizedBox(height: 20.px),
                TextFieldOfLogin(
                  "verificationCode".tr,
                  "ic_register_sms_code",
                  logic.codeC,
                  focusNode: logic.codeF,
                  keyboardType: TextInputType.number,
                  inputFormatters: textFormatterNumSmsCode,
                  onSubmitted: (v) {
                    logic.commit(context);
                  },
                  // onChanged: (v) {
                  //   logic.refreshCommitButton();
                  // },
                  rWidget: GetCodeBt(
                    mobilePhone: TextEditingController(text: Q1Data.phone),
                    sence: "withdraw",
                    codeFocusNode: logic.codeF,
                    loginType: LoginType.none,
                  ),
                ),
                SizedBox(height: 30.px),
              ],
            ),
          ),
          SmallButton(
            width: 311.px,
            height: 64.px,
            borderRadius: BorderRadius.all(Radius.circular(64.px)),
            margin: EdgeInsets.symmetric(horizontal: 32.px, vertical: 32.px),
            onPressed: () {
              logic.commit(context);
            },
            child: Text(
              'confirm'.tr,
              style: TextStyle(
                color: Color(0xffFBFCFF),
                fontSize: 20.px,
                fontWeight: MyFontWeight.semBold,
              ),
            ),
          )
        ],
      ),
    );
  }
}

class AddressWithdrawTips extends StatelessWidget {
  const AddressWithdrawTips({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: 5.px),
        Text(
          'withdrawalFee'.tr,
          style: TextStyle(color: Colors.grey, fontSize: 12.px),
        ),
      ],
    );
  }
}
