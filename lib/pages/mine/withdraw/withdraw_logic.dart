import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:zhibao_flutter/api/wallet/wallet_view_model.dart';
import 'package:zhibao_flutter/pages/mine/mine_logic.dart';
import 'package:zhibao_flutter/zone.dart';

class WithdrawLogic extends GetxController {
  RxString textValue = "0.00".obs;

  double get balance => Q1Data.userInfoEntity?.balance ?? 0;

  double get valueDouble => double.parse(textValue.value.replaceAll(",", ""));

  TextEditingController addressC =
      TextEditingController(text: AppConfig.mockAddress);
  FocusNode addressF = FocusNode();
  TextEditingController codeC = TextEditingController();
  FocusNode codeF = FocusNode();

  Future commit(BuildContext context) async {
    if (valueDouble <= 0) {
      Get.back();
      return;
    }
    if (addressC.text.isEmpty) {
      myToast('enterAddress'.tr);
      addressF.requestFocus();
      return;
    }
    if (codeC.text.isEmpty) {
      myToast('enterVerificationCode'.tr);
      codeF.requestFocus();
      return;
    }
    if (valueDouble > balance) {
      myToast('exceedBalance'.tr);
      return;
    }

    final value = await walletViewModel.walletWithdrawApply(
      context,
      amount: valueDouble,
      address: addressC.text,
      sms: codeC.text,
    );
    if (value.data == null) {
      return;
    }
    Get.back();
    myToast('operationSuccessful'.tr);
    final MineLogic? logic = Get.find<MineLogic>();
    if (logic != null) {
      logic.refreshMine();
    }
  }
}
