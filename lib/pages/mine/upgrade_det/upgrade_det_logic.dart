import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:zhibao_flutter/api/wallet/wallet_view_model.dart';
import 'package:zhibao_flutter/pages/mine/mine_logic.dart';
import 'package:zhibao_flutter/util/ui/my_toast.dart';

class UpgradeDetLogic extends GetxController {
  int current = 0;
  Map<int, int> needsUsd = {
    1: 3,
    2: 6,
    3: 12,
    4: 24,
    5: 48,
    6: 96,
    7: 192,
    8: 384,
    9: 768,
    10: 1536,
    11: 3072,
    12: 6144,
  };

  @override
  void onInit() {
    super.onInit();
    if (Get.arguments is int) {
      current = Get.arguments as int;
      print("current: $current");
    } else {
      Get.back();
      myToast('parameterError'.tr);
    }
  }

  Future commit(BuildContext context) async {
    final value = await walletViewModel.userUpgrade(context);
    if (value.data == null) {
      return;
    }
    Get.back(result: 1);
    myToast('operationSuccessful'.tr);
    final MineLogic? logic = Get.find<MineLogic>();
    if (logic != null) {
      logic.refreshMine();
    }
  }
}
