import 'package:get/get.dart';

import 'upgrade_det_logic.dart';

class UpgradeDetBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => UpgradeDetLogic());
  }
}
