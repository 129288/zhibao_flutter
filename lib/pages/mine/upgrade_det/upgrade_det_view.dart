import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zhibao_flutter/pages/home/bubble_new_page.dart';
import 'package:zhibao_flutter/widget/appbar/commom_bar.dart';
import 'package:zhibao_flutter/widget/base/my_scaffold.dart';
import 'package:zhibao_flutter/zone.dart';

import 'upgrade_det_logic.dart';

class UpgradeDetPage extends StatelessWidget {
  final logic = Get.find<UpgradeDetLogic>();

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      appBar: CommomBar(),
      body: ListView(
        children: [
          SizedBox(height: (41 - 19).px),
          Center(
            child: Image.asset(
              'assets/images/upgrade/ic_upgrade_big_${logic.current}.png',
              width: 185.19.px,
            ),
          ),
          SizedBox(height: 18.px),
          Center(
            child: Text(
              'NO${logic.current}',
              style: TextStyle(
                color: Color(0xff171616),
                fontSize: 36.px,
                fontWeight: MyFontWeight.bold,
              ),
            ),
          ),
          SizedBox(height: 28.px),
          Stack(
            children: [
              Image.asset(
                'assets/images/upgrade/ic_upgrade_det_card.png',
                width: FrameSize.winWidth(),
              ),
              Container(
                padding:
                    EdgeInsets.symmetric(horizontal: 21.px, vertical: 19.px),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '${logic.needsUsd[logic.current]}.00 U',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 28.px,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      'upgradeToEarnMore'
                          .trParams({"levelValue": "${logic.current + 1}"}),
                      style:
                          TextStyle(color: Color(0xffF7F7F7), fontSize: 12.px),
                    )
                  ],
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 15.px, vertical: 22.px),
            padding: EdgeInsets.symmetric(vertical: 22.px),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(10.px)),
              boxShadow: MyTheme.shadowGrey,
            ),
            child: Column(
              children: [
                Center(
                  child: Container(
                    height: 36.px,
                    width: 170.px,
                    decoration: BoxDecoration(
                      color: Color(0xffF37135),
                      borderRadius: BorderRadius.all(
                        Radius.circular(22.px),
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        VerticalLine(
                          color: Colors.white.withOpacity(0.2),
                          width: 2.px,
                          height: 12.px,
                        ),
                        SizedBox(width: 6.px),
                        VerticalLine(
                          color: Colors.white.withOpacity(0.5),
                          width: 2.px,
                          height: 12.px,
                        ),
                        SizedBox(width: 6.px),
                        VerticalLine(
                          color: Colors.white.withOpacity(1),
                          width: 2.px,
                          height: 12.px,
                        ),
                        SizedBox(width: 10.px),
                        Text(
                          'wePromise'.tr,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16.px,
                            fontWeight: MyFontWeight.medium,
                          ),
                        ),
                        SizedBox(width: 10.px),
                        VerticalLine(
                          color: Colors.white.withOpacity(1),
                          width: 2.px,
                          height: 12.px,
                        ),
                        SizedBox(width: 6.px),
                        VerticalLine(
                          color: Colors.white.withOpacity(0.5),
                          width: 2.px,
                          height: 12.px,
                        ),
                        SizedBox(width: 6.px),
                        VerticalLine(
                          color: Colors.white.withOpacity(0.2),
                          width: 2.px,
                          height: 12.px,
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 24.px),
                Row(
                  children: [
                    ['ic_upgrade_det_zhengping.png', "authenticGuarantee".tr],
                    ['ic_upgrade_det_shouhou.png', "worryFreeAfterSales".tr],
                    ['ic_upgrade_det_jisu.png', "fastLogistics".tr],
                    ['ic_upgrade_det_tese.png', "specialServices".tr],
                  ].map((e) {
                    return Expanded(
                      child: Column(
                        children: [
                          Image.asset(
                            'assets/images/upgrade/${e[0]}',
                            width: 34.9.px,
                          ),
                          SizedBox(height: 10.px),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 5.px),
                            child: Text(
                              e[1],
                              style: TextStyle(
                                  color: Color(0xff171616), fontSize: 12.px),
                              textAlign: TextAlign.center,
                            ),
                          )
                        ],
                      ),
                    );
                  }).toList(),
                ),
                SizedBox(height: (31 - 22).px),
              ],
            ),
          ),
          SizedBox(height: (30 - 22).px),
          SmallButton(
            height: 64.px,
            color: (Q1Data.grade + 1) == logic.current
                ? MyTheme.themeColor()
                : Colors.grey,
            borderRadius: BorderRadius.all(Radius.circular(64.px / 2)),
            margin: EdgeInsets.symmetric(horizontal: 16.px),
            onPressed: () {
              if ((Q1Data.grade + 1) == logic.current) {
                logic.commit(context);
              }
            },
            child: Text('upgradeNow'.tr),
          ),
          InkwellWithOutColor(
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 20.px),
              child: Text(
                'viewMyMatrix'.tr,
                style: TextStyle(color: MyTheme.themeColor(), fontSize: 16.px),
                textAlign: TextAlign.center,
              ),
            ),
            onTap: () {
              Get.to(BubbleNewPage(logic.current));
            },
          ),
          SizedBox(height: 28.px),
        ],
      ),
    );
  }
}
