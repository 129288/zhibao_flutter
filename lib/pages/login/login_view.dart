import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:zhibao_flutter/api/commom/common_entity.dart';
import 'package:zhibao_flutter/pages/login/login_widget.dart';
import 'package:zhibao_flutter/util/config/sp_key.dart';
import 'package:zhibao_flutter/widget/bt/Inkwell_without_color.dart';
import 'package:zhibao_flutter/widget/input/text_field_of_login.dart';
import 'package:zhibao_flutter/zone.dart';

import 'login_logic.dart';

class LoginPagePayPw extends StatelessWidget {
  final LoginInput logic;
  final ValueChanged<String>? onSubmitted;

  LoginPagePayPw(this.logic, this.onSubmitted);

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => TextFieldOfLogin(
        'transactionPassword'.tr,
        "ic_lock_new",
        logic.pwController,
        focusNode: logic.pwFocusNode,
        obscureText: !logic.showPw.value,
        textInputAction: onSubmitted != null ? TextInputAction.next : null,
        onSubmitted: (v) {
          if (onSubmitted != null) {
            onSubmitted!(v);
          } else {
            logic.commit(context);
          }
        },
        onChanged: (v) {
          logic.onChangePw(v);
        },
        rWidget: Obx(() {
          return InkwellWithOutColor(
            onTap: () {
              logic.showPw.value = !logic.showPw.value;
            },
            child: SvgPicture.asset(
              "assets/images/login/ic_eye_${logic.isInputPw.value ? "theme" : "grey"}_${!logic.showPw.value ? "close" : "open"}.svg",
              width: 24.px,
              height: 24.px,
            ),
          );
        }),
      ),
    );
  }
}

class LoginPagePw extends StatelessWidget {
  final LoginInput logic;
  final ValueChanged<String>? onSubmitted;

  LoginPagePw(this.logic, this.onSubmitted);

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => TextFieldOfLogin(
        'password'.tr,
        "ic_lock_new",
        logic.pwController,
        focusNode: logic.pwFocusNode,
        obscureText: !logic.showPw.value,
        textInputAction: onSubmitted != null ? TextInputAction.next : null,
        onSubmitted: (v) {
          if (onSubmitted != null) {
            onSubmitted!(v);
          } else {
            logic.commit(context);
          }
        },
        onChanged: (v) {
          logic.onChangePw(v);
        },
        rWidget: Obx(() {
          return InkwellWithOutColor(
            onTap: () {
              logic.showPw.value = !logic.showPw.value;
            },
            child: SvgPicture.asset(
              "assets/images/login/ic_eye_${logic.isInputPw.value ? "theme" : "grey"}_${!logic.showPw.value ? "close" : "open"}.svg",
              width: 24.px,
              height: 24.px,
            ),
          );
        }),
      ),
    );
  }
}

class LoginPagePhone extends StatefulWidget {
  final LoginInput logic;

  LoginPagePhone(this.logic);

  @override
  State<LoginPagePhone> createState() => _LoginPagePhoneState();
}

enum LoginType { none, phone, email }

class _LoginPagePhoneState extends State<LoginPagePhone> {
  @override
  Widget build(BuildContext context) {
    final bool phone = widget.logic.loginType.value == LoginType.phone;
    final iconImage = phone ? "ic_phone" : "ic_email";
    return Column(
      children: [
        ToggleButtons(
          splashColor: Colors.transparent,
          isSelected: [phone, !phone],
          onPressed: (index) {
            widget.logic.loginType.value =
                index == 0 ? LoginType.phone : LoginType.email;
            widget.logic.phoneFocusNode.unfocus();
            widget.logic.emailFocusNode.unfocus();
            setState(() {});

            /// Refresh the commit button.
            widget.logic.refreshCommitButton();
          },
          children: ["ic_phone", "ic_email"].map((e) {
            return SvgPicture.asset(
              "assets/images/login/$e.svg",
              width: 24.px,
              height: 24.px,
            );
          }).toList(),
        ),
        SizedBox(height: 10.px),
        TextFieldOfLogin(
          phone ? "mobilePhone".tr : "email".tr,
          iconImage,
          phone ? widget.logic.phoneController : widget.logic.emailController,
          headWidget: phone
              ? InkwellWithOutColor(
                  child: Row(
                    children: [
                      SizedBox(width: 10.px),
                      Obx(
                        () => Text(
                          widget.logic.countryCode.value,
                          style: TextStyle(
                            color: Color(0xff171616),
                            fontSize: 16.px,
                          ),
                        ),
                      ),
                      SizedBox(width: 15.px),
                      Container(
                        width: 1.px,
                        height: 24.px,
                        color: Color(0xffD8D8D8),
                      ),
                      SizedBox(width: 10.px),
                    ],
                  ),
                  onTap: () {
                    showAreaCodeDialog(
                      context,
                      (AreaCode? v) {
                        if (v?.phoneCode is String) {
                          widget.logic.countryCode.value = v!.phoneCode;
                          GetStorage()
                              .write(SpKey.countryCodeForPhone, v.phoneCode);
                        }
                      },
                    );
                  },
                )
              : null,
          focusNode:
              phone ? widget.logic.phoneFocusNode : widget.logic.emailFocusNode,
          textInputAction: TextInputAction.next,
          inputFormatters:
              AppConfig.allowAccountNotPhone ? [] : numAndPhoneFormatter,
          onSubmitted: (v) {
            widget.logic.pwFocusNode.requestFocus();
          },
          keyboardType:
              phone ? TextInputType.phone : TextInputType.emailAddress,
          onChanged: (v) {
            widget.logic.onChangePhone(v);
          },
          rWidget: Obx(() {
            if (widget.logic.isFinishPhone.value) {
              return InkwellWithOutColor(
                  onTap: () {
                    unFocus(context);
                  },
                  child: SvgPicture.asset(
                    "assets/images/login/ic_mark_checked.svg",
                    width: 34.px,
                    height: 34.px,
                  ));
            } else {
              return Container();
            }
          }),
        ),
      ],
    );
  }
}

class LogoShow extends StatelessWidget {
  const LogoShow({super.key});

  @override
  Widget build(BuildContext context) {
    final sizeOfLogo = 76.px;
    final br = BorderRadius.all(Radius.circular(16.8.px));
    return ClipRRect(
      child: Image.asset(
        "assets/images/login/ic_launcher.png",
        width: sizeOfLogo,
        height: sizeOfLogo,
      ),

      /// Use the svg will not display anything about the image.
      // child: SvgPicture.asset(
      //   // "assets/images/login/ic_phone.svg",
      //   "assets/images/login/ic_logo.svg",
      //   width: sizeOfLogo,
      //   height: sizeOfLogo,
      // ),
      borderRadius: br,
    );
  }
}

class LoginPage extends StatelessWidget {
  final logic = Get.find<LoginLogic>();

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      checkDoubleExit: true,
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 32.px),
        children: [
          SizedBox(height: FrameSize.padTopH() + 50.px),
          Align(
            alignment: Alignment.centerLeft,
            child: LogoShow(),
          ),
          SizedBox(height: 18.px),
          Text(
            'login'.tr,
            style: TextStyle(
              color: Color(0xff171616),
              fontSize: 24.px,
              fontWeight: MyFontWeight.bold,
            ),
          ),
          SizedBox(height: 53.px),
          LoginPagePhone(logic),
          Space(height: 20.px),
          LoginPagePw(logic, null),
          Align(
            alignment: Alignment.centerRight,
            child: ClickEvent(
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 14.px),
                child: Text(
                  'forgotPassword'.tr,
                  style: TextStyle(color: Color(0xff848AA4), fontSize: 14.px),
                ),
              ),
              onTap: () {
                logic.toForgetPw();
              },
            ),
          ),
          Space(height: (24 - 14).px),
          Obx(
            () => SmallButton(
              height: 64.px,
              borderRadius: BorderRadius.all(Radius.circular(999)),
              onPressed: () {
                logic.commit(context);
              },
              enabled: logic.loginBtAvalable.value,
              child: Text('login'.tr),
            ),
          ),
          Center(
            child: ClickEvent(
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 24.px),
                child: Text(
                  'noAccountRegister'.tr,
                  style: TextStyle(color: Color(0xff182046), fontSize: 16.px),
                ),
              ),
              onTap: () {
                logic.toRegister();
              },
            ),
          ),
          TextButton(
            onPressed: () {
              Get.toNamed(RouteConfig.privacyPage);
            },
            child: Text('privacyPolicyQuote'.tr),
          )
        ],
      ),
    );
  }
}
