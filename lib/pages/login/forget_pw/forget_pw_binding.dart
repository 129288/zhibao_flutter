import 'package:get/get.dart';

import 'forget_pw_logic.dart';

class ForgetPwBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ForgetPwLogic());
  }
}
