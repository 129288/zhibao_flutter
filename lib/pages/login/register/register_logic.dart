import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zhibao_flutter/api/user/user_view_model.dart';
import 'package:zhibao_flutter/pages/login/login_logic.dart';

import '../../../zone.dart';

abstract mixin class RegisterInput implements LoginInputMajor, LoginInput {
  RxBool isInputPwAgain = false.obs;
  RxBool showPwAgain = false.obs;

  @override
  void refreshCommitButton() {
    loginBtAvalable.value = strNoEmpty(useAccountController.text) &&
        strNoEmpty(pwController.text) &&
        strNoEmpty(pwAgainController.text) &&
        strNoEmpty(codeController.text);
  }

  void onChangePwAgain(String v) {
    isInputPwAgain.value = strNoEmpty(v);
    refreshCommitButton();
  }
}

class RegisterLogic extends GetxController with LoginInput, RegisterInput {
  @override
  void onInit() {
    super.onInit();
    getInviteCodeParam();
  }

  Future getInviteCodeParam() async {
    // Only support web.
    if (!kIsWeb) {
      return;
    }
    if (Get.parameters.containsKey(AppConfig.inviterKey) &&
        Get.parameters[AppConfig.inviterKey] != null) {
      String inviteFromParam = Get.parameters[AppConfig.inviterKey].toString();
      inviteController.text = inviteFromParam;
    }
    /*
    *  example:
    *  [TZ] RegisterLogic::Get.arguments::null
    *  [TZ] RegisterLogic::Get.parameters::{i: 12231}
    * */
    LogUtil.d("RegisterLogic::Get.arguments::${Get.arguments}");
    LogUtil.d("RegisterLogic::Get.parameters::${Get.parameters}");
    LogUtil.d("---------");
  }

  @override
  Future commit(BuildContext context) async {
    if (!registerCheckPassed(true)) {
      return;
    }

    final value = await userViewModel.register(
      context,
      account: useAccount,
      password: pwController.text,
      checkCode: codeController.text,
      inviteCode: strNoEmpty(inviteController.text)
          ? inviteController.text
          : "", // 取消默认填写邀请码1234
    );
    if (value.data == null) {
      return;
    }
    Get.back();
    myToast("操作成功");
  }
}
