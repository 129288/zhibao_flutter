import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:zhibao_flutter/pages/login/login_logic.dart';
import 'package:zhibao_flutter/pages/login/login_view.dart';
import 'package:zhibao_flutter/pages/login/register/register_logic.dart';
import 'package:zhibao_flutter/widget/data/get_code_bt.dart';
import 'package:zhibao_flutter/zone.dart';

class RegisterPagePayPwAgain extends StatelessWidget {
  final RegisterInput logic;

  RegisterPagePayPwAgain(this.logic);

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => TextFieldOfLogin(
        'confirmTransactionPassword'.tr,
        "ic_lock_new",
        logic.pwAgainController,
        focusNode: logic.pwAgainFocusNode,
        obscureText: !logic.showPwAgain.value,
        textInputAction: TextInputAction.next,
        onSubmitted: (v) {
          logic.codeFocusNode.requestFocus();
        },
        onChanged: (v) {
          logic.onChangePwAgain(v);
        },
        rWidget: Obx(() {
          return InkwellWithOutColor(
            onTap: () {
              logic.showPwAgain.value = !logic.showPwAgain.value;
            },
            child: SvgPicture.asset(
              "assets/images/login/ic_eye_${logic.isInputPwAgain.value ? "theme" : "grey"}_${!logic.showPwAgain.value ? "close" : "open"}.svg",
              width: 24.px,
              height: 24.px,
            ),
          );
        }),
      ),
    );
  }
}

class RegisterPagePwAgain extends StatelessWidget {
  final RegisterInput logic;

  RegisterPagePwAgain(this.logic);

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => TextFieldOfLogin(
        'confirmPassword'.tr,
        "ic_lock_new",
        logic.pwAgainController,
        focusNode: logic.pwAgainFocusNode,
        obscureText: !logic.showPwAgain.value,
        textInputAction: TextInputAction.next,
        onSubmitted: (v) {
          logic.codeFocusNode.requestFocus();
        },
        onChanged: (v) {
          logic.onChangePwAgain(v);
        },
        rWidget: Obx(() {
          return InkwellWithOutColor(
            onTap: () {
              logic.showPwAgain.value = !logic.showPwAgain.value;
            },
            child: SvgPicture.asset(
              "assets/images/login/ic_eye_${logic.isInputPwAgain.value ? "theme" : "grey"}_${!logic.showPwAgain.value ? "close" : "open"}.svg",
              width: 24.px,
              height: 24.px,
            ),
          );
        }),
      ),
    );
  }
}

class RegisterSmsCode extends StatelessWidget {
  final RegisterInput logic;
  final String sence;

  RegisterSmsCode(this.logic, this.sence);

  @override
  Widget build(BuildContext context) {
    return TextFieldOfLogin('verificationCode'.tr, "ic_register_sms_code", logic.codeController,
        focusNode: logic.codeFocusNode,
        keyboardType: TextInputType.number,
        inputFormatters: textFormatterNumSmsCode, onSubmitted: (v) {
      logic.commit(context);
    }, onChanged: (v) {
      logic.refreshCommitButton();
    },
        rWidget: Obx(
          () => GetCodeBt(
            mobilePhone: logic.loginType.value == LoginType.phone
                ? logic.phoneController
                : logic.emailController,
            sence: sence,
            codeFocusNode: logic.codeFocusNode,
            loginType: logic.loginType.value,
          ),
        ));
  }
}

class RegisterCommit extends StatelessWidget {
  final String title;
  final LoginInput logic;

  RegisterCommit(this.title, this.logic);

  @override
  Widget build(BuildContext context) {
    final height = 64.px;
    return Obx(
      () => SmallButton(
        height: height,
        borderRadius: BorderRadius.all(Radius.circular(height / 2)),
        onPressed: () {
          logic.commit(context);
        },
        enabled: logic.loginBtAvalable.value,
        child: Text(title),
      ),
    );
  }
}

class RegisterPage extends StatelessWidget {
  final logic = Get.find<RegisterLogic>();

  @override
  Widget build(BuildContext context) {
    // [Mar 9 2024]
    // 如果在这里不调用的话那么在地址栏修改然后回车就不会刷新。
    logic.getInviteCodeParam();
    return MyScaffold(
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 32.px),
        children: [
          SizedBox(height: FrameSize.padTopH() + 50.px),
          Align(
            alignment: Alignment.centerLeft,
            child: LogoShow(),
          ),
          SizedBox(height: 18.px),
          Text(
            'register'.tr,
            style: TextStyle(
              color: Color(0xff171616),
              fontSize: 24.px,
              fontWeight: MyFontWeight.bold,
            ),
          ),
          SizedBox(height: 53.px),
          LoginPagePhone(logic),
          Space(height: 16.px),
          LoginPagePw(logic, (v) {
            logic.pwAgainFocusNode.requestFocus();
          }),
          Space(height: 16.px),
          RegisterPagePwAgain(logic),
          Space(height: 16.px),
          RegisterSmsCode(logic, "register"),
          Space(height: 16.px),
          TextFieldOfLogin(
            "invitationCodeMandatory".tr,
            "ic_register_sms_code",
            logic.inviteController,
          ),
          Space(height: 30.px),
          RegisterCommit('complete'.tr, logic),
          Space(height: 60.px),
        ],
      ),
    );
  }
}
