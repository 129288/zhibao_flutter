import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:zhibao_flutter/api/user/user_view_model.dart';
import 'package:zhibao_flutter/pages/login/login_logic.dart';
import 'package:zhibao_flutter/pages/login/register/register_logic.dart';

import '../../../zone.dart';

class ForgetPayPwLogic extends GetxController with LoginInput, RegisterInput {
  @override
  Future commit(BuildContext context) async {
    if (!registerCheckPassed(true, isPayPassword: true)) {
      return;
    }
    final value = await userViewModel.userUpdatePayPassword(
      context,
      checkCode: codeController.text,
      account: useAccount,
      password: pwController.text,
    );
    if (value.data == null) {
      return;
    }
    myToast("操作成功");
    Get.back();
  }
}
