import 'package:get/get.dart';

import 'forget_pay_pw_logic.dart';

class ForgetPayPwBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ForgetPayPwLogic());
  }
}
