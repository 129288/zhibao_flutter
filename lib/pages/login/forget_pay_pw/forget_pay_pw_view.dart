import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zhibao_flutter/pages/login/login_view.dart';
import 'package:zhibao_flutter/pages/login/register/register_view.dart';
import 'package:zhibao_flutter/widget/appbar/commom_bar.dart';
import 'package:zhibao_flutter/widget/base/my_scaffold.dart';
import 'package:zhibao_flutter/zone.dart';

import 'forget_pay_pw_logic.dart';

class ForgetPayPwPage extends StatelessWidget {
  final logic = Get.find<ForgetPayPwLogic>();

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      appBar: CommomBar(title: "setTransactionPassword".tr),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 28.px),
        children: [
          SizedBox(height: 50.px),
          Container(
            padding: EdgeInsets.all(18.px),
            decoration: BoxDecoration(
              color: MyTheme.mainColor.value.withOpacity(0.1),
              shape: BoxShape.circle,
            ),
            child: SvgPicture.asset(
                'assets/images/login/ic_forget_pw_avatar.svg',
                width: 76.px,
                height: 76.px),
          ),
          SizedBox(height: 28.px),
          Text(
            'setTransactionPassword'.tr,
            style: TextStyle(
              color: Color(0xff171616),
              fontSize: 18.px,
              fontWeight: MyFontWeight.medium,
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 4.px),
            child: Column(
              children: [
                SizedBox(height: 16.px),
                LoginPagePhone(logic),
                Space(height: 16.px),
                LoginPagePayPw(logic, (v) {
                  logic.pwAgainFocusNode.requestFocus();
                }),
                Space(height: 16.px),
                RegisterPagePayPwAgain(logic),
                Space(height: 16.px),
                RegisterSmsCode(logic, "payPassword"),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 30.px),
                  width: FrameSize.winWidth(),
                  child: RegisterCommit("continue".tr, logic),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
