import 'package:flutter/material.dart';
import 'package:zhibao_flutter/util/float/fram_size.dart';
import 'package:zhibao_flutter/widget/scrol/my_behavior.dart';
import 'package:get/get.dart';

class ScrollDialog extends StatefulWidget {
  final Widget? child;
  final bool barrierDismissible;
  final WillPopCallback? onWillPop;
  final bool isDecoration;
  final double? width;
  final double? height;

  ScrollDialog({
    this.child,
    this.barrierDismissible = false,
    this.isDecoration = false,
    this.onWillPop,
    this.height,
    this.width,
  }) : assert(child != null);

  @override
  _ScrollDialogState createState() => _ScrollDialogState();
}

class _ScrollDialogState extends State<ScrollDialog> {
  GlobalKey key = GlobalKey();

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(milliseconds: 600)).then((value) {
      if (mounted) setState(() {});
    });
  }

  double getHeight() {
    RenderBox? box = key.currentContext?.findRenderObject() as RenderBox?;
    double? size;
    var _fullHeight = FrameSize.winHeight() - FrameSize.statusBarHeight();
    var _boxHeight = box?.size.height;
    if (_boxHeight == null) {
      size = _fullHeight;
    } else {
      size = _boxHeight > _fullHeight ? _boxHeight : _fullHeight;
    }
    return size;
  }

  @override
  Widget build(BuildContext context) {
    var _content = new Material(
      type: MaterialType.transparency,
      child: new ScrollConfiguration(
        behavior: new MyBehavior(),
        child: new SingleChildScrollView(
          child: new Container(
            height: getHeight(),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                widget.isDecoration
                    ? new Container(
                        key: key,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(12)),
                        ),
                        padding:
                            EdgeInsets.symmetric(horizontal: 25, vertical: 24),
                        width: FrameSize.winWidth() - 60,
                        child: widget.child,
                      )
                    : new Container(
                        key: key,
                        child: widget.child,
                      ),
              ],
            ),
          ),
        ),
      ),
    );
    if (widget.onWillPop != null) {
      return WillPopScope(
        onWillPop: widget.onWillPop,
        child: _content,
      );
    } else if (widget.barrierDismissible) {
      return new GestureDetector(
        child: _content,
        onTap: () => Get.back(),
      );
    } else {
      return _content;
    }
  }
}
