// import 'package:discuz_q_flutter/framework/export.dart';
// import 'package:extended_image/extended_image.dart';
// import 'package:flutter/material.dart';
//
// class MyPhotoView extends StatefulWidget {
//   final String imageTestUrl;
//
//   const MyPhotoView(this.imageTestUrl, {Key? key}) : super(key: key);
//
//   @override
//   State<MyPhotoView> createState() => _MyPhotoViewState();
// }
//
// class _MyPhotoViewState extends State<MyPhotoView> {
//   final GlobalKey<ExtendedImageGestureState> gestureKey =
//       GlobalKey<ExtendedImageGestureState>();
//
//   GestureConfig gestureConfig(ExtendedImageState state) {
//     return GestureConfig(
//       minScale: 0.9,
//       animationMinScale: 0.7,
//       maxScale: 4.0,
//       animationMaxScale: 4.5,
//       speed: 1.0,
//       inertialSpeed: 100.0,
//       initialScale: 1.0,
//       inPageView: false,
//       initialAlignment: InitialAlignment.center,
//       reverseMousePointerScrollDirection: true,
//       gestureDetailsIsChanged: (GestureDetails? details) {
//         //print(details?.totalScale);
//       },
//     );
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Material(
//       child: Column(
//         children: <Widget>[
//           CommonBar(
//             rightDMActions: [
//               IconButton(
//                 padding: EdgeInsets.symmetric(horizontal: 10.px),
//                 icon: Image.asset(
//                   'assets/images/main/ic_reset.png',
//                   color: MyTheme.themeColor(),
//                 ),
//                 onPressed: () {
//                   gestureKey.currentState!.reset();
//                   //you can also change zoom manual
//                   //gestureKey.currentState.gestureDetails=GestureDetails();
//                 },
//               )
//             ],
//           ),
//           Expanded(
//             child: Hero(
//               tag: widget.imageTestUrl,
//               child: GestureDetector(
//                 child: () {
//                   if (widget.imageTestUrl.contains('asset')) {
//                     return ExtendedImage.asset(
//                       widget.imageTestUrl,
//                       fit: BoxFit.contain,
//                       mode: ExtendedImageMode.gesture,
//                       extendedImageGestureKey: gestureKey,
//                       initGestureConfigHandler: gestureConfig,
//                     );
//                   }
//                   return ExtendedImage.network(
//                     widget.imageTestUrl,
//                     fit: BoxFit.contain,
//                     mode: ExtendedImageMode.gesture,
//                     extendedImageGestureKey: gestureKey,
//                     initGestureConfigHandler: gestureConfig,
//                   );
//                 }(),
//                 onTap: () {
//                   Get.back();
//                 },
//               ),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
