import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:zhibao_flutter/util/my_theme.dart';

class CommomBar extends StatelessWidget implements PreferredSizeWidget {
  const CommomBar({
    this.title = '',
    this.showBackIcon = true,
    this.rightDMActions,
    this.backgroundColor,
    this.mainColor,
    this.titleW,
    this.bottom,
    this.leading,
    this.isCenterTitle = true,
    this.brightness,
    this.automaticallyImplyLeading = true,
    this.icons,
    this.elevation = 0,
    this.titleStyle,
    this.systemOverlayStyle,
  });

  final String title;
  final bool showBackIcon;
  final List<Widget>? rightDMActions;
  final Color? backgroundColor;
  final Color? mainColor;
  final Widget? titleW;
  final PreferredSizeWidget? bottom;
  final Widget? leading;
  final bool isCenterTitle;
  final Brightness? brightness;
  final SystemUiOverlayStyle? systemOverlayStyle;
  final bool automaticallyImplyLeading;
  final IconData? icons;
  final double elevation;
  final TextStyle? titleStyle;

  @override
  Size get preferredSize => Size(100, bottom != null ? 100 : 48);

  @override
  Widget build(BuildContext context) {
    final Color? mainColorValue = mainColor ?? null;
    return AppBar(
      title: titleW ??
          Text(
            title,
            style: titleStyle ?? MyTheme.appBarTitleStyle(mainColorValue),
          ),
      backgroundColor: backgroundColor,
      elevation: elevation,
      // ignore: deprecated_member_use
      // brightness: brightness ?? Brightness.light,
      systemOverlayStyle: systemOverlayStyle,
      leading: leading ??
          (showBackIcon
              ? Navigator.canPop(context)
                  ? InkWell(
                      onTap: () {
                        FocusScope.of(context).requestFocus(FocusNode());
                        Navigator.maybePop(context);
                      },
                      child: Container(
                        width: (40),
                        height: (40),
                        color: Colors.transparent,
                        alignment: Alignment.center,
                        child: icons != null
                            ? Icon(icons, color: mainColorValue)
                            : Image(
                                width: (10),
                                height: (19),
                                fit: BoxFit.cover,
                                color: mainColorValue,
                                image: AssetImage(
                                    "assets/images/main/ic_nav_arrow1_n.png"),
                              ),
                      ),
                    )
                  : null
              : null),
      centerTitle: isCenterTitle,
      bottom: bottom,
      automaticallyImplyLeading: automaticallyImplyLeading,
      actions: rightDMActions ?? [const Center()],
    );
  }
}
