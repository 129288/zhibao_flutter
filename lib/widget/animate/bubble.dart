// import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:zhibao_flutter/api/wallet/wallet_entity.dart';
import 'package:zhibao_flutter/zone.dart';

class Bobbles extends StatefulWidget {
  final int index;
  final List<TeamStructureEntity> teamStructureList;

  Bobbles({
    required this.index,
    required this.teamStructureList,
  });

  @override
  _BobblesState createState() => _BobblesState();
}

class _BobblesState extends State<Bobbles>
    with TickerProviderStateMixin, AutomaticKeepAliveClientMixin {
  late Animation<RelativeRect> animation;
  late Animation<RelativeRect> animationTop;
  late AnimationController controller;
  late AnimationController controllerTop;

  int seconds = 0;

  @override
  initState() {
    super.initState();
    controller = AnimationController(
      lowerBound: 0.0,
      duration: Duration(milliseconds: 800 + (widget.index * 100)),
      vsync: this,
    );
    controllerTop = AnimationController(
      lowerBound: 0.0,
      duration: Duration(milliseconds: 200),
      vsync: this,
    );

    RelativeRectTween rectTween = RelativeRectTween(
      begin: RelativeRect.fromLTRB(0.0, 0.0, 0.0, 10.0),
      end: RelativeRect.fromLTRB(0.0, 10.0, 0.0, 0.0),
    );

    animation = rectTween.animate(controller);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    animation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        controller?.reverse();
      } else if (status == AnimationStatus.dismissed) {
        controller?.forward();
      }
    });
    controller?.forward();

    List positionList = [
      [-10, -(FrameSize.winHeight() / 2.1)],

      /// Location 1
      [-(FrameSize.winWidth() / 2), -(FrameSize.winHeight() / 2.9)],

      /// Location 2
      [(FrameSize.winWidth() / 1.8), -(FrameSize.winHeight() / 2.6)],

      /// Location 3
      [-(FrameSize.winWidth() / 1.6), -70],

      /// Location 4
      [-(FrameSize.winWidth() / 1.9), FrameSize.winHeight() / 5],

      /// Location 5
      [(FrameSize.winWidth() / 1.6), -80],

      /// Location 6
      [(FrameSize.winWidth() / 1.9), FrameSize.winHeight() / 3.7],
    ];

    // List positionList = [
    //   [0, -(FrameSize.winHeight() / 2.4)],
    //   [(FrameSize.winWidth() / 1.5), 30],
    //   [-(FrameSize.winWidth() / 1.2), 0],
    //   [-(FrameSize.winWidth() / 3), -(FrameSize.winHeight() / 2.7)],
    //   [-(FrameSize.winWidth() / 2), FrameSize.winHeight() / 3.9],
    //   [(FrameSize.winWidth() / 2), -(FrameSize.winHeight() / 2.8)],
    //   [100, -(FrameSize.winHeight() / 2.5)],
    //   [(FrameSize.winWidth() / 1.3), -50],
    //   [(FrameSize.winWidth() / 1.6), -120],
    //   [0, (FrameSize.winHeight() / 2.8)],
    //   [-(FrameSize.winWidth() / 1.2), 80],
    //   [-(FrameSize.winWidth() / 1.3), -90],
    //   [-(FrameSize.winWidth() / 1.6), -170],
    //   [-(FrameSize.winWidth() / 3), FrameSize.winHeight() / 2.8],
    //   [80, FrameSize.winHeight() / 2.7],
    //   [(FrameSize.winWidth() / 1.3), -(FrameSize.winHeight() / 3.9)],
    //   [(FrameSize.winWidth() / 1.3), FrameSize.winHeight() / 3],
    //   [(FrameSize.winWidth() / 1.6), FrameSize.winHeight() / 3.2],
    //   [-(FrameSize.winWidth() / 1.5), FrameSize.winHeight() / 6],
    // ];

    List resultList = positionList[widget.index];
    RelativeRectTween rectTweenTop = RelativeRectTween(
      begin: RelativeRect.fromLTRB(
          resultList[0].toDouble(), resultList[1].toDouble(), 0.0, 0.0),
      end: RelativeRect.fromLTRB(
          resultList[0].toDouble(), -FrameSize.winHeight(), 0.0, 0.0),
    );

    animationTop = rectTweenTop.animate(controllerTop);
    final TeamStructureEntity? model =
        widget.index > (widget.teamStructureList.length - 1)
            ? null
            : widget.teamStructureList[widget.index];
    final double size = model == null ? 75 : 125;
    final bool notSelf = model?.userId != Q1Data.id;
    var bubble = Container(
      width: size,
      height: size,
      child: Stack(
        children: <Widget>[
          PositionedTransition(
            rect: animation,
            child: Column(
              children: [
                Center(
                  child: Image.asset(
                    'assets/images/ic/ic_float_diamond.png',
                    width: 40,
                    height: 40,
                    color: model == null ? Colors.grey : null,
                  ),
                ),
                Space(height: 5),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 0.px),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: model == null
                        ? [
                            Text(
                              'ToBeJoined'.tr,
                              style: TextStyle(color: Colors.white),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            )
                          ]
                        : [
                            /// 自己不显示位置
                            if (notSelf)
                              Text(
                                "${'Position'.tr}: ${model.num ?? ''}",
                                style: TextStyle(color: Colors.white),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            Text(
                              "${'User'.tr}: ${!notSelf ? 'self'.tr : end4Phone(model.phone)}",
                              style: TextStyle(color: Colors.white),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                            Text(
                              "${"referrer".tr}: ${end4Phone(model.referrerPhone ?? '0000')}",
                              style: TextStyle(color: Colors.white),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
    return PositionedTransition(
      rect: animationTop,
      child: UnconstrainedBox(
        child: GestureDetector(
          child: bubble,
          onTap: () => handle(),
        ),
      ),
    );
  }

  void handle() async {
    return;
    // if (!Q1Data.isLogin()) {
    //   Get.to(new LoginPage());
    //   return;
    // }
    // final player = AudioCache();
    // player.play('file/media.mp3');

    controller.stop();
    controllerTop.forward();
  }

  @override
  dispose() {
    controller?.dispose();
    controllerTop?.dispose();
    super.dispose();
  }

  @override
  bool get wantKeepAlive => true;
}

//    List positionOldList = [
//      [0, -(FrameSize.winHeight() / 2.8)],
//      [-(FrameSize.winWidth() / 1.2), 0],
//      [-(FrameSize.winWidth() / 3), -70],
//      [-(FrameSize.winWidth() / 2), FrameSize.winHeight() / 3.9],
//      [30, 0],
//      [-(FrameSize.winWidth() / 2.3), 20],
//      [130, 50],
//      [(FrameSize.winWidth() / 1.5), 30],
//      [(FrameSize.winWidth() / 1.3), -50],
//      [(FrameSize.winWidth() / 1.6), -120],
//      [0, (FrameSize.winHeight() / 2.8)],
//      [-(FrameSize.winWidth() / 1.2), 80],
//      [-(FrameSize.winWidth() / 3), -170],
//      [-(FrameSize.winWidth() / 2), FrameSize.winHeight() / 2.8],
//      [10, 90],
//      [-(FrameSize.winWidth() / 2.3), FrameSize.winHeight() / 6],
//      [130, 150],
//      [(FrameSize.winWidth() / 1.3), 80],
//      [(FrameSize.winWidth() / 1.3), FrameSize.winHeight() / 3],
//      [(FrameSize.winWidth() / 1.6), FrameSize.winHeight() / 3.2],
//    ];
