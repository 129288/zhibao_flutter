import 'package:flutter/material.dart';
import 'package:zhibao_flutter/util/data/q1_data.dart';
import 'package:zhibao_flutter/util/ui/my_toast.dart';

class LoginInkWell extends InkWell {
  LoginInkWell({GestureTapCallback? onTap, required Widget child, Key? key})
      : super(
            key: key,
            child: child,
            onTap: () {
              if (onTap != null) {
                if (!Q1Data.isAgree.value) {
                  myToast("请先阅读且同意协议");
                  return;
                }
                onTap();
              }
            });
}
