import 'package:flutter/material.dart';
import 'package:zhibao_flutter/zone.dart';

class MyGradientButton extends StatelessWidget {
  final VoidCallback onTap;
  final RxBool enable;
  final String text;
  final TextStyle? style;
  final EdgeInsetsGeometry? margin;
  final double? height;
  final double? width;
  final BorderRadiusGeometry? borderRadius;

  MyGradientButton({
    required this.onTap,
    required this.enable,
    required this.text,
    this.style,
    this.margin,
    this.height,
    this.width,
    this.borderRadius,
  });

  @override
  Widget build(BuildContext context) {
    return ClickEvent(
      onTap: () {
        onTap();
      },
      child: Obx(
        () {
          return Container(
            width: width,
            height: height ?? 50,
            margin: margin ?? EdgeInsets.only(left: 27.5 - 20),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              borderRadius: borderRadius ?? BorderRadius.circular(25),
              gradient: LinearGradient(
                begin: Alignment.centerRight,
                end: Alignment.centerLeft,
                colors: enable.value
                    ? MyTheme.gradientColors
                    : MyTheme.gradientDisableColors,
              ),
            ),
            child: Text(
              text,
              style: style ??
                  TextStyle(
                    fontSize: 17,
                    color: Color(0xffffffff),
                    fontWeight: FontWeight.bold,
                  ),
            ),
          );
        },
      ),
    );
  }
}
