import 'package:flutter/material.dart';

class InkwellWithOutColor extends StatelessWidget {
  final Widget child;
  final GestureTapCallback? onTap;

  InkwellWithOutColor({
    required this.child,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: child,
      focusColor: Colors.transparent,
      highlightColor: Colors.transparent,
      splashColor: Colors.transparent,
    );
  }
}
