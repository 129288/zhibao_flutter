import 'package:flutter/material.dart';
import 'package:zhibao_flutter/widget/bt/small_button.dart';
import 'package:zhibao_flutter/zone.dart';

class SmallBorderBt extends StatelessWidget {
  final VoidCallback onPressed;
  final Widget child;
  final double? width;
  final double? height;
  final BorderRadius? borderRadius;
  final List<BoxShadow>? boxShadow;
  SmallBorderBt({
    required this.onPressed,
    required this.child,
    this.width,
    this.height,
    this.borderRadius,
    this.boxShadow,
  });

  @override
  Widget build(BuildContext context) {
    return SmallButton(
      onPressed: onPressed,
      child: child,
      color: Colors.white,
      width: width,
      height: height,
      borderRadius: borderRadius,
      boxShadow: boxShadow,
      padding: width != null && height != null ? EdgeInsets.all(0) : null,
      border: Border.all(
        color: MyTheme.mainColor.value,
        width: 1.px,
      ),
    );
  }
}
