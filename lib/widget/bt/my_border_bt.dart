import 'package:flutter/material.dart';
import 'package:zhibao_flutter/zone.dart';

class MyBorderBt extends SmallButton {
  MyBorderBt()
      : super(
          width: 127.px,
          height: 42.px,
          padding: EdgeInsets.all(0),
          color: Colors.white,
          border: Border.all(color: Color(0xffDDDDDD), width: 1),
          onPressed: () {
            Get.back();
          },
          child: Text(
            '取消',
            style: TextStyle(color: Color(0xffDDDDDD), fontSize: 17.px),
          ),
        );
}