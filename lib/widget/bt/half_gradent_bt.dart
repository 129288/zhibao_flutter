import 'package:flutter/material.dart';
import 'package:zhibao_flutter/zone.dart';

class HalfGradentBt extends SmallButton {
  HalfGradentBt({required VoidCallback onPressed})
      : super(
          onPressed: onPressed,
          width: 127.px,
          height: 42.px,
          padding: EdgeInsets.all(0),
          gradient: MyTheme.mainButtonGradientReverse,
          child: Text(
            '保存',
            style: TextStyle(
              color: Color(0xffFFFFFF),
              fontSize: 17.px,
            ),
          ),
        );
}
