import 'package:flutter/material.dart';

class MyErrorWidget extends StatelessWidget {
  const MyErrorWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Error'),
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: const [
            Icon(
              Icons.error_outline_outlined,
              color: Colors.red,
              size: 100,
            ),
            Text(
              'Oops... something went wrong',
            ),
          ],
        ),
      ),
    );
  }
}
