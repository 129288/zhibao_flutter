import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zhibao_flutter/util/float/fram_size.dart';
import 'package:zhibao_flutter/zone.dart';

class LoadingView extends StatelessWidget {
  final double height;
  final Color? color;

  const LoadingView({
    Key? key,
    this.height = 300,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: FrameSize.winWidth(),
      height: height,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          const CupertinoActivityIndicator(),
          Space(height: 5),
          Text(
            'loading'.tr,
            style: TextStyle(color: color ?? Colors.grey, fontSize: 12),
          ),
        ],
      ),
    );
  }
}
