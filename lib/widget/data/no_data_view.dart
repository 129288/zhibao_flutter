import 'package:flutter/material.dart';
import 'package:zhibao_flutter/util/config/app_config.dart';
import 'package:zhibao_flutter/util/float/fram_size.dart';
import 'package:zhibao_flutter/zone.dart';

class NoDataView extends StatefulWidget {
  final String? label;
  final VoidCallback? onRefresh;
  final bool isTeam;
  final Function? onPressed;

  NoDataView({
    this.label,
    this.onRefresh,
    this.isTeam = true,
    this.onPressed,
  });

  NoDataViewState createState() => NoDataViewState();
}

class NoDataViewState extends State<NoDataView> {
  bool isWay = false;

  @override
  Widget build(BuildContext context) {
    var label = widget.label ?? '暂无数据';
    return Container(
      height: 180.px,
      width: FrameSize.winWidth(),
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Space(height: mainSpace * 1.5),
          new Image.asset(
            'assets/images/${widget.isTeam ? 'ic_empty' : 'ic_no_data'}.png',
            height: 100.px,
          ),
          new Space(height: mainSpace * 1.5),
          // new Text(
          //   label,
          //   style: TextStyle(color: Colors.grey, fontSize: 14.0.px),
          // ),
          // new Space(height: mainSpace * 2.5),
          new Visibility(
            visible: widget.onRefresh != null,
            child: isWay
                ? new CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(MyTheme.themeColor()),
                  )
                : new Container(),
          ),
          // new Visibility(
          //   visible: widget.onRefresh != null,
          //   child: SmallButton(
          //     padding: EdgeInsets.symmetric(horizontal: 10),
          //     onPressed: () => handle(),
          //     child: new Text('重新加载'),
          //   ),
          // )
        ],
      ),
    );
  }

  handle() {
    widget.onRefresh!();

    if (mounted) setState(() => isWay = true);
    new Future.delayed(new Duration(seconds: 2), () {}).then((v) {
      if (mounted) setState(() => isWay = false);
    });
  }
}

class NoDataWidgetOfQianbaozaie extends StatelessWidget {
  final bool miniHeight;

  const NoDataWidgetOfQianbaozaie({this.miniHeight = false, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin:
      !miniHeight ? EdgeInsets.zero : EdgeInsets.symmetric(vertical: 27),
      height: !miniHeight
          ? ((FrameSize.winHeightDynamic(context) - FrameSize.topBarHeight()) *
          0.7)
          : null,
      width: FrameSize.winWidthDynamic(context),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            'assets/images/qianbaozai/ic_kby_zwsj.png',
            width: 191.65,
            height: 95,
          ),
          Space(height: 21),
          Text(
            '暂无数据',
            style: TextStyle(color: Color(0xff9E9E9E), fontSize: 16),
          ),
        ],
      ),
    );
  }
}
