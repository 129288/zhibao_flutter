import 'package:flutter/material.dart';
import 'package:zhibao_flutter/zone.dart';

const String launchImg = "assets/images/ic_launcher.png";

class LogoWidget extends StatelessWidget {
  final double? size;
  final Color? color;

  const LogoWidget({
    this.size,
    this.color,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = this.size ?? 30.px;
    return Container(
      width: size,
      height: size,
      padding: EdgeInsets.all(3.px),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey.withOpacity(0.2)),
        shape: BoxShape.circle,
      ),
      child: Image.asset(launchImg, width: size, height: size, color: color),
    );
  }
}
