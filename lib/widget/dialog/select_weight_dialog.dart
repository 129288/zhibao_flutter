import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zhibao_flutter/util/check/check.dart';
import 'package:zhibao_flutter/util/float/fram_size.dart';
import 'package:zhibao_flutter/util/my_theme.dart';
import 'package:zhibao_flutter/zone.dart';
import 'package:get/get.dart';

Future selectWeightDialog(context, {final String? init}) {
  LogUtil.d("selectWeightDialog::init::$init");
  return showModalBottomSheet(
    context: context,
    backgroundColor: Colors.transparent,
    builder: (context) {
      return Material(
        type: MaterialType.transparency,
        child: SelectWeightDialog(init),
      );
    },
  );
}

class SelectWeightDialog extends StatefulWidget {
  final String? init;

  const SelectWeightDialog(this.init, {Key? key}) : super(key: key);

  @override
  _SelectWeightDialogState createState() => _SelectWeightDialogState();
}

class _SelectWeightDialogState extends State<SelectWeightDialog> {
  FixedExtentScrollController controller = FixedExtentScrollController();

  String currentValue = "";

  List<String> itemList = List.generate(75 - 35 + 1, (index) {
    final int okInt = index + 35;
    if (okInt == 75) {
      return "75kg及以上";
    }
    return "${okInt}kg";
  });

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(milliseconds: 0)).then((value) => initData());
  }

  void initData() async {
    if (!strNoEmpty(widget.init)) {
      currentValue = itemList[0];
      return;
    }
    currentValue = widget.init!;

    final int getIndex = itemList.indexWhere((element) {
      return element == currentValue ||

          /// [selectWeightDialog] Origin compare to fix can't auto scroll.
          element.replaceAll('kg', '').replaceAll('及以上', '') ==
              currentValue.replaceAll('kg', '').replaceAll('及以上', '');
    });
    LogUtil.d(
        "${this.toString()}::getIndex::$getIndex,currentValue::$currentValue");
    await controller.animateToItem(getIndex,
        duration: Duration(milliseconds: 50), curve: Curves.bounceIn);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 274.5 + FrameSize.padBotH(),
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  child: Container(
                    width: 45,
                    padding: EdgeInsets.symmetric(vertical: 15),
                    alignment: Alignment.center,
                    child: Text(
                      '取消',
                      style: TextStyle(
                        color: Color(0xffFF5001),
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  onTap: () => Get.back(),
                ),
                Text(
                  '请选择体重',
                  style: TextStyle(fontSize: 17, fontWeight: FontWeight.w500),
                ),
                InkWell(
                  child: Container(
                    width: 45,
                    padding: EdgeInsets.symmetric(vertical: 15),
                    alignment: Alignment.center,
                    child: Text(
                      '确定',
                      style: TextStyle(
                        color: Color(0xffFF5001),
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  onTap: () {
                    print("currentValue：：$currentValue");
                    Get.back(result: currentValue);
                  },
                ),
              ],
            ),
          ),
          Expanded(
            child: CupertinoPicker.builder(
              itemExtent: 45,
              childCount: itemList.length,
              scrollController: controller,
              backgroundColor: Colors.white,
              selectionOverlay: Container(),
              onSelectedItemChanged: (index) {
                currentValue = itemList[index];
                setState(() {});
              },
              itemBuilder: (context, index) {
                final String value = itemList[index];
                return Container(
                  height: 45,
                  alignment: Alignment.center,
                  child: Text(
                    value,
                    style: TextStyle(
                      color: Color(0xff30302F),
                      fontSize: 17,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
