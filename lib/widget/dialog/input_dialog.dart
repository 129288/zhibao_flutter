import 'package:flutter/material.dart';
import 'package:zhibao_flutter/util/check/regular.dart';
import 'package:zhibao_flutter/util/float/fram_size.dart';
import 'package:zhibao_flutter/util/my_theme.dart';
import 'package:zhibao_flutter/util/ui/my_toast.dart';
import 'package:zhibao_flutter/util/ui/ui.dart';
import 'package:zhibao_flutter/widget/bt/small_button.dart';
import 'package:get/get.dart';

Future inputDialog(
  context, {
  final String? title,
  final String? tips,
  final String? initText,
  final String? hintText,
  final int? maxLength,
  final TextInputType? keyboardType,
}) {
  return showDialog(
    context: context,
    builder: (context) {
      return InputDialogPage(
          title, tips, hintText, maxLength, initText, keyboardType);
    },
  );
}

class InputDialogPage extends StatefulWidget {
  final String? title;
  final String? tips;
  final String? hintText;
  final String? initText;
  final int? maxLength;
  final TextInputType? keyboardType;

  InputDialogPage(
    this.title,
    this.tips,
    this.hintText,
    this.maxLength,
    this.initText,
    this.keyboardType,
  );

  @override
  _InputDialogPageState createState() => _InputDialogPageState();
}

class _InputDialogPageState extends State<InputDialogPage> {
  TextStyle style = TextStyle(color: Color(0xff121212), fontSize: 16);

  late TextEditingController controller;
  late TextEditingController controllerSecond;

  @override
  void initState() {
    super.initState();
    controller = TextEditingController(text: widget.initText);
    controllerSecond = TextEditingController(text: "");
  }

  @override
  Widget build(BuildContext context) {
    int _maxLength = widget.maxLength ?? 10;
    return Material(
      type: MaterialType.transparency,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: FrameSize.winKeyHeight(context)),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(12)),
            ),
            padding: EdgeInsets.symmetric(horizontal: 25, vertical: 24),
            width: Get.width - 60,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  widget.title ?? '设置提U信息～',
                  style: TextStyle(
                    color: Color(0xff121212),
                    fontSize: 20,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                if (strNoEmpty(widget.tips))
                  Text(
                    widget.tips ?? '',
                    style: TextStyle(color: Color(0xff909090), fontSize: 14),
                  ),
                Space(height: 20),
                Container(
                  decoration: BoxDecoration(
                    color: Color(0xffF4F4F4),
                    borderRadius: BorderRadius.all(Radius.circular(6)),
                  ),
                  child: TextField(
                    controller: controller,
                    keyboardType: widget.keyboardType,
                    onChanged: (text) {
                      setState(() {});
                    },
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                      border: InputBorder.none,
                      hintText: widget.hintText ?? '提U账号',
                    ),
                  ),
                ),
                Space(),
                Container(
                  decoration: BoxDecoration(
                    color: Color(0xffF4F4F4),
                    borderRadius: BorderRadius.all(Radius.circular(6)),
                  ),
                  child: TextField(
                    controller: controllerSecond,
                    onChanged: (text) {
                      setState(() {});
                    },
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                      border: InputBorder.none,
                      hintText: widget.hintText ?? '提U名字',
                    ),
                  ),
                ),
                Space(height: 5),
                Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    '${controller.text.length}/$_maxLength',
                    style: TextStyle(
                        color: controller.text.length > _maxLength
                            ? Colors.red
                            : Color(0xff909090),
                        fontSize: 14),
                  ),
                ),
                Space(height: 20),
                Row(
                  children: [
                    SmallButton(
                      minWidth: (Get.width - 130) / 2,
                      padding: EdgeInsets.symmetric(horizontal: 0),
                      margin: EdgeInsets.all(0),
                      child: Text('取消'),
                      onPressed: () => Get.back(),
                      color: Color(0xffAEAEAE),
                    ),
                    Space(width: 20),
                    SmallButton(
                      minWidth: (Get.width - 130) / 2,
                      padding: EdgeInsets.symmetric(horizontal: 0),
                      margin: EdgeInsets.all(0),
                      color: MyTheme.themeColor(),
                      child: Text('确定'),
                      onPressed: () => handle(),
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  void handle() {
    if (!strNoEmpty(controller.text)) {
      myFailToast('输入不能为空哦');
      return;
    } else if (controller.text.length > (widget.maxLength ?? 10)) {
      myFailToast('输入过长，请缩减后再试');
      return;
    }
    if (!strNoEmpty(controllerSecond.text)) {
      myFailToast('输入不能为空哦');
      return;
    } else if (controllerSecond.text.length > (widget.maxLength ?? 10)) {
      myFailToast('输入过长，请缩减后再试');
      return;
    }
    Get.back(result: controller.text + "," + controllerSecond.text);
  }
}
