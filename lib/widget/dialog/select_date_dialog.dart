import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zhibao_flutter/util/check/check.dart';
import 'package:zhibao_flutter/util/my_theme.dart';
import 'package:get/get.dart';

Future selectDateDialog(context, {final DateTime? initDate}) {
  return showModalBottomSheet(
    context: context,
    backgroundColor: Colors.transparent,
    builder: (context) {
      return Material(
        type: MaterialType.transparency,
        child: SelectDateDialog(initDate),
      );
    },
  );
}

class SelectDateDialog extends StatefulWidget {
  final DateTime? initDate;

  const SelectDateDialog(this.initDate, {Key? key}) : super(key: key);

  @override
  _SelectDateDialogState createState() => _SelectDateDialogState();
}

class _SelectDateDialogState extends State<SelectDateDialog> {
  FixedExtentScrollController controller = FixedExtentScrollController();
  FixedExtentScrollController yearController = FixedExtentScrollController();
  FixedExtentScrollController monthController = FixedExtentScrollController();

  int currentYear = DateTime.now().year;
  int currentMouth = 1;
  int currentDay = 1;

  List years = List.generate(30, (index) {
    return DateTime.now().year - index;
  });
  List? mouths;

  int? dayNum;

  @override
  void initState() {
    super.initState();
    mouths = List.generate(12, (index) {
      return DateTime(currentYear).month + index;
    });
    dayNum = MyDate.getDays(currentYear)[currentMouth - 1];
    Future.delayed(Duration(milliseconds: 0)).then((value) => initDate());
  }

  void initDate() async {
    if (widget.initDate == null) {
      return;
    }
    currentYear = widget.initDate!.year;
    currentMouth = widget.initDate!.month;
    currentDay = widget.initDate!.day;

    await yearController.animateToItem(years.indexOf(currentYear),
        duration: Duration(milliseconds: 50), curve: Curves.bounceIn);
    await monthController.animateToItem(mouths!.indexOf(currentMouth),
        duration: Duration(milliseconds: 50), curve: Curves.bounceIn);
    await controller.animateToItem(currentDay - 1,
        duration: Duration(milliseconds: 50), curve: Curves.bounceIn);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.vertical(top: Radius.circular(8)),
      ),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 5),
            decoration: BoxDecoration(border: MyTheme.mainBorder()),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  child: Container(
                    width: 60,
                    padding: EdgeInsets.symmetric(vertical: 15),
                    alignment: Alignment.center,
                    child: Text(
                      '取消',
                      style: TextStyle(
                        color: Color(0xff121212),
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  onTap: () => Get.back(),
                ),
                Text(
                  '请选择日期',
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                ),
                InkWell(
                  child: Container(
                    width: 60,
                    padding: EdgeInsets.symmetric(vertical: 15),
                    alignment: Alignment.center,
                    child: Text(
                      '确定',
                      style: TextStyle(
                        color: Color(0xffFF2E18),
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  onTap: () => Get.back(
                    result:
                        '${DateTime(currentYear, currentMouth, currentDay).millisecondsSinceEpoch ~/ 1000}',
                    // '$currentYear-${'${doubleNum(currentMouth)}-${doubleNum(currentDay)}'}',
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Row(
              children: [
                Expanded(
                  child: CupertinoPicker.builder(
                    itemExtent: 60,
                    childCount: years.length,
                    scrollController: yearController,
                    onSelectedItemChanged: (d) {
                      currentYear = years[d];
                      dayNum = MyDate.getDays(currentYear)[currentMouth - 1];
                      if (controller.offset > 0) controller.jumpTo(0);
                      if (monthController.offset > 0) monthController.jumpTo(0);
                      setState(() {});
                    },
                    itemBuilder: (context, index) {
                      return Container(
                        height: 60,
                        alignment: Alignment.center,
                        child: Text('${years[index]}年'),
                      );
                    },
                  ),
                ),
                Expanded(
                  child: CupertinoPicker.builder(
                    itemExtent: 60,
                    childCount: mouths!.length,
                    scrollController: monthController,
                    onSelectedItemChanged: (d) {
                      currentMouth = mouths![d];
                      dayNum = MyDate.getDays(currentYear)[currentMouth - 1];
                      if (controller.offset > 0) controller.jumpTo(0);
                      setState(() {});
                    },
                    itemBuilder: (context, index) {
                      return Container(
                        height: 60,
                        alignment: Alignment.center,
                        child: Text('${mouths![index]}月'),
                      );
                    },
                  ),
                ),
                Expanded(
                  child: CupertinoPicker.builder(
                    itemExtent: 60,
                    onSelectedItemChanged: (d) {
                      currentDay = d + 1;
                    },
                    scrollController: controller,
                    childCount: dayNum,
                    itemBuilder: (context, index) {
                      return Container(
                        height: 60,
                        alignment: Alignment.center,
                        child: Text('${index + 1}日'),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
