import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zhibao_flutter/util/float/fram_size.dart';
import 'package:zhibao_flutter/util/my_theme.dart';
import 'package:zhibao_flutter/util/ui/ui.dart';
import 'package:zhibao_flutter/widget/bt/small_button.dart';
import 'package:zhibao_flutter/widget/flutter/scroll_dialog.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:get/get.dart';

bool _canShow = true;

Future confirmDialog(
  context, {
  String? text,
  VoidCallback? onPressed,
  VoidCallback? onCancel,
  String? cancelText,
  String? okText,
  Widget? head,
  int type = 1,
  TextAlign textAlign = TextAlign.center,
  bool? isHtml,
  Widget? child,
  bool? barrierDismissible,
}) async {
  if (!_canShow) {
    return;
  }
  return showDialog(
    context: context,
    builder: (context) {
      return SetPwHud(
        text,
        onPressed,
        cancelText,
        okText,
        head,
        type,
        textAlign,
        onCancel,
        isHtml: isHtml,
        child: child,
        barrierDismissible: barrierDismissible,
      );
    },
  ).then<void>((value) {
    _canShow = false;
    Future.delayed(const Duration(milliseconds: 200)).then((value) {
      _canShow = true;
    });
  });
}

class SetPwHud extends StatefulWidget {
  final String? text;
  final String? cancelText;
  final String? okText;
  final VoidCallback? onPressed;
  final VoidCallback? onCancel;
  final Widget? head;
  final int? type;
  final TextAlign? textAlign;
  final bool? isHtml;
  final Widget? child;
  final bool? barrierDismissible;

  SetPwHud(this.text, this.onPressed, this.cancelText, this.okText, this.head,
      this.type, this.textAlign, this.onCancel,
      {this.isHtml = false, this.child, this.barrierDismissible});

  @override
  _SetPwHudState createState() => _SetPwHudState();
}

class _SetPwHudState extends State<SetPwHud> {
  @override
  Widget build(BuildContext context) {
    return ScrollDialog(
      barrierDismissible: widget.barrierDismissible ?? false,
      child: Material(
        type: MaterialType.transparency,
        child: Stack(
          children: [
            SizedBox(
              width: FrameSize.winWidth(),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                    ),
                    padding: EdgeInsets.only(
                        left: 23.px, right: 23.px, bottom: 39.px, top: 45.px),
                    width: FrameSize.winWidth() - (22.5 * 2),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Center(
                          child: widget.head ?? Container(),
                        ),
                        widget.child != null
                            ? SizedBox(
                                width: FrameSize.winWidth(),
                                child: widget.child,
                              )
                            : widget.isHtml ?? false
                                ? Html(
                                    data: '${widget.text}',
                                    style: MyTheme.mainHtmlStyle(),
                                  )
                                : Padding(
                                    padding: EdgeInsets.symmetric(
                                      horizontal: (34 - 23).px,
                                    ),
                                    child: Text(
                                      widget.text ?? 'pleaseEnterContent'.tr,
                                      style: TextStyle(
                                        color: Color(0xff30302F),
                                        fontSize: 16.px,
                                        fontWeight: FontWeight.w600,
                                      ),
                                      textAlign: widget.textAlign,
                                    ),
                                  ),
                        // new Space(height: _isAd ? 20 : 40),
                        Space(height: 30),
                        widget.type == 1
                            ? Row(
                                children: [
                                  Expanded(
                                    child: SmallButton(
                                      minWidth:
                                          (FrameSize.winWidthDynamic(context) -
                                                  130) /
                                              2,
                                      color: Colors.white,
                                      border: Border.all(
                                        color: Color(0xffDDDDDD),
                                      ),
                                      margin: EdgeInsets.zero,
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 0),
                                      child: Text(
                                        widget.cancelText ?? 'cancel'.tr,
                                        style: const TextStyle(
                                            color: Color(0xff828282),
                                            fontSize: 17,
                                            fontWeight: FontWeight.w700),
                                      ),
                                      onPressed: () async {
                                        Get.back();
                                        if (widget.onCancel != null) {
                                          widget.onCancel!();
                                        }
                                      },
                                    ),
                                  ),
                                  Space(width: 30),
                                  Expanded(
                                    child: SmallButton(
                                      minWidth:
                                          (FrameSize.winWidthDynamic(context) -
                                                  130) /
                                              2,
                                      margin: EdgeInsets.zero,
                                      gradient: LinearGradient(
                                        colors: [
                                          Color(0xffFF8800),
                                          Color(0xffFF7F00),
                                        ],
                                      ),
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 0),
                                      child: Text(
                                        widget.okText ?? 'confirm'.tr,
                                        style: const TextStyle(
                                          color: Colors.white,
                                          fontSize: 17,
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                      onPressed: () async {
                                        Get.back();
                                        if (widget.onPressed != null) {
                                          widget.onPressed!();
                                        }
                                      },
                                    ),
                                  ),
                                ],
                              )
                            : SmallButton(
                                minWidth:
                                    (FrameSize.winWidthDynamic(context) - 130) /
                                        2,
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 0),
                                child: Text(widget.okText ?? 'gotIt'.tr),
                                onPressed: () async {
                                  Get.back();
                                },
                              ),
                      ],
                    ),
                  ),
                  // new Space(height: _isAd && Platform.isAndroid ? 200 : 0),
                ],
              ),
            ),
            // new Positioned(
            //   bottom: 0,
            //   child: _isAd && Platform.isAndroid
            //       ? new FullDoseBannerView(horizontal: 0)
            //       : new Container(),
            // ),
            // new Positioned(
            //   top: 0,
            //   child: _isAd && Platform.isAndroid
            //       ? new FullDoseFeedView(horizontal: 0)
            //       : new Container(),
            // ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    // if (widget.isAd) adViewModel.removeBannerAd();
  }
}
