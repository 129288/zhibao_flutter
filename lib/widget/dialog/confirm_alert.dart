import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zhibao_flutter/util/check/check.dart';

bool _canShow = true;

typedef VoidCallbackConfirm = void Function(bool isOk);

void confirmAlert<T>(
  BuildContext context,
  VoidCallbackConfirm callBack, {
  int? type,
  String? tips,
  String? okBtn,
  String? cancelBtn,
  TextStyle? okBtnStyle,
  TextStyle? cancelBtnStyle,
  TextStyle? style,
  bool isWarm = false,
  String? warmStr,
}) {
  if (!_canShow) {
    return;
  }
  showDialog<T>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      if (!strNoEmpty(okBtn)) okBtn = '确定';
      if (!strNoEmpty(cancelBtn)) cancelBtn = '取消';
      if (!strNoEmpty(warmStr)) warmStr = '温馨提示';
      return CupertinoAlertDialog(
        title: isWarm
            ? Padding(
                padding: const EdgeInsets.only(bottom: 20.0, top: 0),
                child: Text(
                  '$warmStr',
                  style: const TextStyle(
                      color: Color(0xff343243),
                      fontSize: 19.0,
                      fontWeight: FontWeight.normal),
                ),
              )
            : Padding(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: Text(
                  '$tips',
                  style: const TextStyle(
                      color: Color(0xff343243),
                      fontSize: 15.0,
                      fontWeight: FontWeight.normal),
                ),
              ),
        content: isWarm
            ? Padding(
                padding: const EdgeInsets.only(bottom: 10.0),
                child: Text(
                  '$tips',
                  style: const TextStyle(color: Color(0xff888697)),
                ),
              )
            : Container(),
        actions: <Widget>[
          CupertinoDialogAction(
            child: Text(
              '$cancelBtn',
              style: cancelBtnStyle ??
                  const TextStyle(fontWeight: FontWeight.w400),
            ),
            isDefaultAction: true,
            onPressed: () {
              Navigator.pop(context);
              callBack(false);
            },
          ),
          CupertinoDialogAction(
            child: Text('$okBtn', style: okBtnStyle),
            isDestructiveAction: true,
            onPressed: () {
              Navigator.pop(context);
              callBack(true);
            },
          ),
        ],
      );
    },
  ).then<void>((value) {
    _canShow = false;
    Future.delayed(const Duration(milliseconds: 200)).then((value) {
      _canShow = true;
    });
  });
}

//example:
//
//confirmAlert(
//context,
//(v) {},
//tips: '当前账号未登录，请登录',
//okBtn: '立即登录',
//okBtnStyle: TextStyle(color: Colors.deepPurple),
//cancelBtnStyle: TextStyle(color: Colors.grey,fontWeight: FontWeight.w400),
//);
