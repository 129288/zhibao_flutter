import 'package:flutter/material.dart';
import 'package:zhibao_flutter/util/check/regular.dart';
import 'package:zhibao_flutter/util/float/fram_size.dart';
import 'package:zhibao_flutter/util/my_theme.dart';
import 'package:zhibao_flutter/util/ui/my_toast.dart';
import 'package:zhibao_flutter/util/ui/ui.dart';
import 'package:zhibao_flutter/widget/bt/small_button.dart';
import 'package:get/get.dart';

Future inputComoLlegarDialog(context, String initAccount, String initName) {
  return showDialog(
    context: context,
    builder: (context) {
      return InputComoLlegarDialogPage(initAccount, initName);
    },
  );
}

class InputComoLlegarDialogPage extends StatefulWidget {
  final String initAccount;
  final String initName;

  InputComoLlegarDialogPage(this.initAccount, this.initName);

  @override
  _InputComoLlegarDialogPageState createState() =>
      _InputComoLlegarDialogPageState();
}

class _InputComoLlegarDialogPageState extends State<InputComoLlegarDialogPage> {
  TextStyle style = TextStyle(color: Color(0xff121212), fontSize: 16);

  late TextEditingController controller;
  late TextEditingController controllerSecond;

  @override
  void initState() {
    super.initState();
    controller = TextEditingController(text: widget.initAccount);
    controllerSecond = TextEditingController(text: widget.initName);
  }

  @override
  Widget build(BuildContext context) {
    TextStyle hintStyle = TextStyle(color: Color(0xffB9B9B9), fontSize: 15);
    return Material(
      type: MaterialType.transparency,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: FrameSize.winKeyHeight(context)),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(12)),
            ),
            padding: EdgeInsets.symmetric(horizontal: 32, vertical: 39),
            width: Get.width - 60,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Text(
                    "设置提现支付宝账号",
                    style: TextStyle(
                      color: Color(0xff181716),
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                Space(height: 30),
                Container(
                  height: 60,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(11)),
                    boxShadow: [
                      BoxShadow(
                          color: Color(0xffB6B6B6).withOpacity(0.3),
                          blurRadius: 10),
                    ],
                  ),
                  child: TextField(
                    controller: controller,
                    onChanged: (text) {
                      setState(() {});
                    },
                    autofocus: true,
                    decoration: InputDecoration(
                      // 【UI问题】绑定支付宝弹框UI问题（详情见图）
                      contentPadding: EdgeInsets.symmetric(horizontal: 21),
                      border: InputBorder.none,
                      hintText: '请输入支付宝账号',
                      hintStyle: hintStyle,
                    ),
                  ),
                ),
                Space(height: 15),
                Container(
                  height: 60,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(11)),
                    boxShadow: [
                      BoxShadow(
                          color: Color(0xffB6B6B6).withOpacity(0.3),
                          blurRadius: 10),
                    ],
                  ),
                  child: TextField(
                    controller: controllerSecond,
                    onChanged: (text) {
                      setState(() {});
                    },
                    decoration: InputDecoration(
                      // 【UI问题】绑定支付宝弹框UI问题（详情见图）
                      contentPadding: EdgeInsets.symmetric(horizontal: 21),
                      border: InputBorder.none,
                      hintText: "请输入支付宝用户名称",
                      hintStyle: hintStyle,
                    ),
                  ),
                ),
                Space(height: 21),
                SizedBox(
                  height: 33,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 4),
                        child: Image.asset(
                            'assets/images/qianbaozai/ic_qianbaozai_xh_n.png',
                            width: 9),
                      ),
                      Space(width: 4),
                      Expanded(
                        child: Text(
                          '请谨慎填写，如账户与您的姓名不匹配则打款\n无法到账',
                          style: TextStyle(
                            color: Color(0xffFF5E5E),
                            fontSize: 12,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Space(height: 15),
                SmallButton(
                  minWidth: (Get.width - 64),
                  padding: EdgeInsets.symmetric(horizontal: 0),
                  margin: EdgeInsets.all(0),
                  gradient: MyTheme.mainButtonGradient,
                  child: Text('确定'),
                  onPressed: () => handle(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void handle() {
    if (!strNoEmpty(controller.text)) {
      myFailToast('支付宝账号不能为空');
      return;
    }
    if (!strNoEmpty(controllerSecond.text)) {
      myFailToast('支付宝用户名称不能为空');
      return;
    }
    Get.back(result: controller.text + "," + controllerSecond.text);
  }
}
