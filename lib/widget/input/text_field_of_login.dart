import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:zhibao_flutter/widget/bt/Inkwell_without_color.dart';
import 'package:zhibao_flutter/zone.dart';

class TextFieldOfLogin extends StatefulWidget {
  final String title;
  final String icon;
  final TextEditingController controller;
  final TextInputType? keyboardType;
  final TextInputAction? textInputAction;
  final ValueChanged<String>? onChanged;
  final Widget? rWidget;
  final FocusNode? focusNode;
  final ValueChanged<String>? onSubmitted;
  final List<TextInputFormatter>? inputFormatters;
  final bool obscureText;
  final Widget? headWidget;

  TextFieldOfLogin(
    this.title,
    this.icon,
    this.controller, {
    this.textInputAction,
    this.keyboardType,
    this.focusNode,
    this.onChanged,
    this.rWidget,
    this.onSubmitted,
    this.inputFormatters,
    this.obscureText = false,
    this.headWidget,
  });

  @override
  State<TextFieldOfLogin> createState() => _TextFieldOfLoginState();
}

class _TextFieldOfLoginState extends State<TextFieldOfLogin> {
  late FocusNode focusNode;

  RxBool select = false.obs;

  @override
  void initState() {
    super.initState();
    focusNode = widget.focusNode ?? FocusNode();
    focusNode.addListener(() {
      select.value = focusNode.hasFocus;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.title,
          style: TextStyle(
            color: Color(0xff848AA4),
            fontSize: 16.px,
            fontWeight: MyFontWeight.medium,
          ),
        ),
        SizedBox(height: 12.px),
        InkwellWithOutColor(
          child: Obx(
            () => Container(
              height: 60.px,
              padding: EdgeInsets.symmetric(horizontal: 10.px),
              decoration: BoxDecoration(
                border: Border.all(
                    color: Color(select.value ? 0xffF37135 : 0xffB7BAC2),
                    width: 1.5.px),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.px),
                ),
              ),
              child: Row(
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10.px),
                    padding: EdgeInsets.all(9.px),
                    decoration: BoxDecoration(
                      color: Color(0xffF37135).withOpacity(0.1),
                      borderRadius: BorderRadius.all(
                        Radius.circular(8.px),
                      ),
                    ),
                    child: SvgPicture.asset(
                      "assets/images/login/${widget.icon}.svg",
                      width: 21.19.px,
                    ),
                  ),
                  SizedBox(width: 5.px),
                  widget.headWidget ?? Container(),
                  SizedBox(width: 5.px),
                  Expanded(
                    child: TextField(
                      controller: widget.controller,
                      focusNode: focusNode,
                      keyboardType: widget.keyboardType,
                      textInputAction: widget.textInputAction,
                      inputFormatters: widget.inputFormatters,
                      obscureText: widget.obscureText,
                      onChanged: widget.onChanged,
                      onSubmitted: widget.onSubmitted,
                      decoration: InputDecoration(
                        isCollapsed: true,
                        hintText: '${'PleaseEnter'.tr}${widget.title}',
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                  SizedBox(width: 10.px),
                  if (widget.rWidget != null) widget.rWidget!,
                  SizedBox(width: 4.px),
                ],
              ),
            ),
          ),
          onTap: () {
            focusNode.requestFocus();
          },
        ),
      ],
    );
  }
}
