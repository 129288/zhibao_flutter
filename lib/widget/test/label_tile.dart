import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zhibao_flutter/zone.dart';
// import 'package:flutter_svg/flutter_svg.dart';

class LabelTile extends StatelessWidget {
  final String? title;
  final String? subTitle;
  final String? bottomTitle;
  final String? rString;
  final TextStyle? rStyle;
  final VoidCallback? onPressed;
  final bool? isBorder;
  final Widget? rightW;
  final double? vertical;
  final Color? iconColor;
  final Color? color;
  final int? type;
  final String? icon;

  const LabelTile({
    this.title,
    this.bottomTitle,
    this.subTitle,
    this.onPressed,
    this.isBorder = true,
    this.rightW,
    this.rString,
    this.rStyle,
    this.vertical = 0,
    this.type = 0,
    this.iconColor,
    this.icon,
    this.color,
  });

  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all(color ?? Colors.white),
        // disabledColor: Colors.white,
        padding: MaterialStateProperty.all(
          EdgeInsets.symmetric(
              horizontal: type == 0 ? 20.0 : 0, vertical: vertical!),
        ),
      ),
      child: Container(
        decoration: BoxDecoration(
          border: isBorder! ? MyTheme.mainBorder() : null,
        ),
        padding:
            EdgeInsets.symmetric(vertical: bottomTitle != null ? 15 : 20.0),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: subTitle != null
                    ? <Widget>[
                        Text(
                          '$title',
                          style: const TextStyle(
                              color: Color(0xff333333), fontSize: 14.0),
                        ),
                        Space(height: mainSpace / 3),
                        Text(
                          '$subTitle',
                          style: const TextStyle(
                              color: Color(0xff999999), fontSize: 12),
                        ),
                      ]
                    : [
                        Visibility(
                          visible: icon != null,
                          child: Container(
                            margin: const EdgeInsets.only(right: 10),
                            child:
                                // icon.endsWith('svg')
                                //     ? SvgPicture.asset(
                                //         icon,
                                //         width: 25,
                                //         height: 25,
                                //       )
                                //     :
                                Image.asset(
                              icon ?? '',
                              width: 25,
                              height: 25,
                            ),
                          ),
                        ),
                        bottomTitle != null
                            ? Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    '$title',
                                    style: const TextStyle(
                                        color: Color(0xff333333),
                                        fontSize: 17.0),
                                  ),
                                  Visibility(
                                    visible: bottomTitle != null,
                                    child: Padding(
                                      padding: EdgeInsets.only(top: 2),
                                      child: Text(
                                        '$bottomTitle',
                                        style: TextStyle(color: Colors.grey),
                                      ),
                                    ),
                                  )
                                ],
                              )
                            : Expanded(
                                child: Text('$title',
                                    style: const TextStyle(
                                        color: Color(0xff333333),
                                        fontSize: 17.0),
                                    overflow: TextOverflow.ellipsis),
                              ),
                      ],
              ),
            ),
            rString != null
                ? Text(
                    '$rString',
                    style: rStyle ?? TextStyle(color: Colors.grey),
                  )
                : Container(),
            rightW ??
                Icon(CupertinoIcons.right_chevron,
                    color: iconColor ?? Colors.grey, size: 18),
          ],
        ),
      ),
      onPressed: onPressed,
    );
  }
}
