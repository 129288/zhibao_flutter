import 'package:flutter/material.dart';
import 'package:zhibao_flutter/util/check/check.dart';
import 'package:zhibao_flutter/util/config/app_config.dart';
import 'package:zhibao_flutter/widget/image/sw_image.dart';

class SwAvatar extends StatelessWidget {
  final String? image;
  final double size;
  final ClickEventCallback? onTap;
  final bool isBorder;
  final double widthOfBorder;

  /// When no avatar use male def img.
  final bool isMaleDef;

  const SwAvatar(
    this.image, {
    this.size = 30,
    this.onTap,
    this.isBorder = false,
    this.isMaleDef = false,
    this.widthOfBorder = 1,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget result;
    Widget defAvatar = SwImage(
      AppConfig.defPic,
      width: size,
      height: size,
      onTap: onTap,
      fit: BoxFit.cover,
      borderRadius: BorderRadius.all(Radius.circular(size / 2)),
    );
    if (!strNoEmpty(image)) {
      result = defAvatar;
      return result;
    }
    result = SwImage(
      image,
      width: size,
      height: size,
      onTap: onTap,
      fit: BoxFit.cover,
      borderRadius: BorderRadius.all(Radius.circular(size / 2)),
      placeholder: (context, url) {
        return defAvatar;
      },
      errorWidget: (context, url, e) {
        return defAvatar;
      },
    );
    if (isBorder) {
      return Container(
        width: size,
        height: size,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(size / 2)),
          border: Border.all(color: Colors.white, width: widthOfBorder),
        ),
        child: result,
      );
    }
    return result;
  }
}
