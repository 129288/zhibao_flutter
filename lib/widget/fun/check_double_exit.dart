import 'package:flutter/material.dart';

import '../../zone.dart';

class CheckDoubleExit extends StatefulWidget {
  final Widget child;

  const CheckDoubleExit({required this.child, super.key});

  @override
  State<CheckDoubleExit> createState() => _CheckDoubleExitState();
}

class _CheckDoubleExitState extends State<CheckDoubleExit> {
  bool nextKickBackExitApp = false;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: widget.child,
      onWillPop: () {
        return onWillPop();
      },
    );
  }

  Future<bool> onWillPop() async {
    if (nextKickBackExitApp) {
      return Future<bool>.value(true);
    } else {
      myToast('exitApp'.tr);
      nextKickBackExitApp = true;
      Future.delayed(
        const Duration(seconds: 2),
        () => nextKickBackExitApp = false,
      );
      return Future<bool>.value(false);
    }
  }
}
