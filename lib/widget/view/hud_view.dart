import 'dart:async';

import 'package:flutter/material.dart';
import 'package:zhibao_flutter/util/http/http.dart';
import 'package:get/get.dart';

class HudView {
  static bool isLoading = false;
  static bool isAutoShow = false;
  static BuildContext? _context;
  static Timer? _timer;
  static String icon = 'assets/images/main/loading.gif';

  /*
  * 定时关闭
  *
  * */
  static void timerTread([int s = Http.RECEIVE_TIMEOUT_SECOND]) {
    if (_timer != null && _timer!.isActive) {
      _timer?.cancel();
      _timer = null;
    }
    int index = 0;
    _timer = Timer.periodic(const Duration(seconds: 1), (Timer timer) {
      index++;
      if (index >= s) {
        index = 0;
        timer.cancel();
        dismiss();
      }
    });
  }

  /*
  * 显示
  *
  * */
  static void show(BuildContext? context,
      {String? msg,
      RxString? rxValue,
      int second = Http.RECEIVE_TIMEOUT_SECOND}) {
    msg ??= "loading".tr;
    if (context == null) {
      return;
    }

    dismiss();

    if (isLoading) {
      return;
    }
    isLoading = true;
    isAutoShow = false;
    _context = context;

    timerTread(second);

    /// Avoid getx no args.
    final oldArguments = Get.arguments;

    Get.dialog(
      barrierColor: Colors.transparent,
      barrierDismissible: false,
      WillPopScope(
        child: Container(
          alignment: Alignment.center,
          color: Colors.transparent,
          child: Material(
            type: MaterialType.transparency,
            textStyle: const TextStyle(color: Color(0xFF212121)),
            child: Container(
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(15.0)),
                color: Color.fromRGBO(0, 0, 0, 0.4),
              ),
              padding:
                  const EdgeInsets.symmetric(vertical: 10.0, horizontal: 4),
              width: 100.0,
              height: 100.0,
              alignment: Alignment.center,
              child: Wrap(
                crossAxisAlignment: WrapCrossAlignment.center,
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    alignment: Alignment.center,
                    child: Image.asset(
                      icon,
                      width: 50,
                      height: 50,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Center(
                      child: rxValue != null
                          ? Obx(
                              () => Text(
                                rxValue.value,
                                style: const TextStyle(color: Colors.white),
                              ),
                            )
                          : Text(
                              msg,
                              style: const TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 14),
                              maxLines: 3,
                            ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        onWillPop: () async {
          return false;
        },
      ),
      arguments: oldArguments,
    );
  }

  /*
  * 消散
  *
  * */
  static void dismiss() {
    if (isLoading && _context != null) {
      try {
        Navigator.of(_context!).pop();
      } catch (e) {
        Get.back();
      }
      _context = null;
    }
    isLoading = false;
    isAutoShow = false;
  }
}
