import 'package:card_swiper/card_swiper.dart';
import 'package:flutter/material.dart';

class MyIndicator extends SwiperPlugin {
  final Color? activeColor;
  final Color? color;
  final double activeSize;
  final double size;
  final double space;
  final Key? key;

  const MyIndicator(
      {this.activeColor,
      this.color,
      this.key,
      this.size = 10.0,
      this.activeSize = 10.0,
      this.space = 3.0});

  @override
  Widget build(BuildContext context, SwiperPluginConfig config) {
    if (config.itemCount > 20) {
      print("这谁顶的住鸭，放那么多");
    }
    Color? activeColor = this.activeColor;
    Color? color = this.color;

    if (activeColor == null || color == null) {
      ThemeData themeData = Theme.of(context);
      activeColor = this.activeColor ?? themeData.primaryColor;
      color = this.color ?? themeData.scaffoldBackgroundColor;
    }

    if (config.layout == SwiperLayout.DEFAULT) {
      return PageIndicator(
          count: config.itemCount,
          controller: PageController(
              initialPage: config.pageController?.initialPage ?? 0),
          layout: PageIndicatorLayout.NONE,
          size: size,
          activeColor: activeColor,
          color: color,
          space: space);
    }

    List<Widget> list = [];

    int itemCount = config.itemCount;
    int activeIndex = config.activeIndex;

    for (int i = 0; i < itemCount; ++i) {
      bool active = i == activeIndex;
      list.add(
        Container(
          key: Key("页码$i"),
          margin: EdgeInsets.all(space),
          child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
              color: active ? activeColor : color,
              width: 6.0,
              height: 4.0),
        ),
      );
    }

    if (config.scrollDirection == Axis.vertical) {
      return new Column(
          key: key, mainAxisSize: MainAxisSize.min, children: list);
    } else {
      return new Row(key: key, mainAxisSize: MainAxisSize.min, children: list);
    }
  }
}
