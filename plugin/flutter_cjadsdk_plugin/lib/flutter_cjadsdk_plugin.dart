import 'dart:convert';

import 'package:flutter/services.dart';

/// 媒体测试ID【appKey】
const String configId = "c8711d2bf65d8408";

/// 开屏测试广告位ID
const String splashId = "efcaf370aa056256";

/// 激励视频测试广告位ID
const String rewardVideoId = "61bf850b1c1463a8";

/// 全屏视频测试广告位ID
const String fullscreenVideoId = "d1e828bceca7f32a";

/// 插屏测试广告位ID
const String interstitialId = "a1f2c152a48d078e";

const String shortVideoId = "738ceaa888e5cf9c";

class FlutterCjadsdkPlugin {
  /// 通道名，需和iOS、android端保持一致
  static const MethodChannel _channel = MethodChannel('flutter_cjadsdk_plugin');

  static void init(VoidCallback onSuccess) {
    /// 设置原生调用Flutter时的回调
    _channel.setMethodCallHandler((call) async {
      print("flutter收到新事件" + call.method);
      print("flutter收到新事件的参数" + call.arguments);
      // Map<dynamic, dynamic> map = jsonDecode(call.arguments);
      // print("setMethodCallHandler::param::"+map["requestId"]);


      switch (call.method) {
        case "setupSuccess":
          onSuccess();
          break;
        //  return _updateNumber(call.arguments); /// 把结果返回给原生端
        default:
          break;
      }
    });
  }

  /// sdk初始化方法
  static Future<String?> adSdkSetup() async {
    final String? version =
        await _channel.invokeMethod('cjSdkSetup', {"configId": configId});
    return version;
  }

  /// 开屏
  static Future<String> getSplashAdMethod() async {
    // ignore: avoid_print
    print("进来了5555");
    final String state = await _channel
        .invokeMethod('cjLoadAndShowSplashMethod', {"advertId": splashId});

    return state;
  }

  /// 激励视频
  static Future<Map> getRewardVideoMethod() async {
    // ignore: avoid_print
    final Map result = await _channel.invokeMethod('cjLoadAndShowRewardVideoMethod',
        {"advertId": rewardVideoId, "userId": "test231231"});
    // ignore: avoid_print
    return result;
  }

    static Future<Map> preLoadReward() async {
    // ignore: avoid_print
    final Map result = await _channel.invokeMethod('preReward', {"advertId": rewardVideoId});
    // ignore: avoid_print
    return result;
  }

  /// 全屏视频
  static Future<int> getFullscreenVideoMethod() async {
    final int result = await _channel.invokeMethod(
        'cjLoadAndShowFullscreenVideoMethod', {"advertId": fullscreenVideoId});
    return result;
  }

  /// 信息流
  static Future<int> getNativeAdMethod() async {
    final int result =
        await _channel.invokeMethod('getNativeAdMethod', {"advertId": "暂不支持"});
    return result;
  }

  /// Banner
  static Future<int> getBannerAdMethod() async {
    final int result =
        await _channel.invokeMethod('getBannerAdMethod', {"advertId": "暂不支持"});

    /// 接收一个数组或者字典作为参数传递给原生端
    return result;
  }

  /// 插屏
  static Future<int> getInterstitialAdMethod() async {
    final int result = await _channel
        .invokeMethod('cjLoadAndShowInterstitialAdMethod', {"advertId": interstitialId});
        // .invokeMethod('cjLoadAndInterstitialAdShowMethod', {"advertId": interstitialId});

    /// 接收一个数组或者字典作为参数传递给原生端
    return result;
  }

  /// 短视频
  static Future<int> getShortVideoAdMethod() async {
    final int result = await _channel
        .invokeMethod('getShortVideoAdMethod', {"advertId": shortVideoId});

    /// 接收一个数组或者字典作为参数传递给原生端
    return result;
  }
}
