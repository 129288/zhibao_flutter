#import <Flutter/Flutter.h>
#import <CJMobileAd/CJMobileAd.h>
#import "ShortViewController.h"

@interface FlutterCjadsdkPlugin : NSObject<FlutterPlugin, CJSplashAdDelegate, CJRewardVideoAdDelegate, CJFullscreenVideoAdDelegate, CJInterstitialAdDelegate>

@property (nonatomic, strong) FlutterMethodChannel *methodChannel;

@end
