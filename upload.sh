echo "正在上传安装包到蒲公英...";

response=$(curl --progress-bar -F "file=@./build/app/outputs/flutter-apk/app-release.apk" \
-F "uKey=4c78368b35f9020b702e41820213afc3" \
-F "_api_key=faa532c3b3ef58631ffe949aa879caa1" \
https://www.pgyer.com/apiv1/app/upload)

appShortcutUrl=$(echo "$response" | grep -o '"appShortcutUrl":"[^"]*' | sed 's/"appShortcutUrl":"//')

# 如果$appShortcutUrl为空,表示上传失败
if [ -z "$appShortcutUrl" ]; then
  echo "上传失败"
  echo "Response:"
  echo $response
  exit 1
fi

echo "Response:"
# the json example:
#{"code":0,"message":"","data":{"appKey":"e91fdfcc3741fb8ad1d0842534ea71af","userKey":"4c78368b35f9020b702e41820213afc3","appType":"2","appIsLastest":"1","appFileSize":"56051252","appName":"\u8054\u5b9d","appVersion":"1.0.9","appVersionNo":"109","appBuildVersion":"4","appIdentifier":"com.zhibao360.zhibaoapp","appIcon":"39b79427d013e3b796a3540356523ba0","appDescription":"","appUpdateDescription":"","appScreenshots":"","appShortcutUrl":"00SZLe","appCreated":"2024-03-17 18:44:40","appUpdated":"2024-03-17 18:44:40","appQRCodeURL":"https:\/\/pgyer.com\/app\/qrcodeHistory\/32bd5e53e2ed6784715bdf43e48dcc44f2444cc99c3c11a0fd7a9ae158188646"}}
echo "$response"
echo ""
echo "Short URL: $appShortcutUrl"

version=$(grep "version:" pubspec.yaml | awk '{print $2}' | sed 's/["'\'']//g')
echo "IDO168-App-"$version"发布啦。
-------------------
【蒲公英-链接内永远保持最新版-每天只有1千次下载次数】
https://www.pgyer.com/"$appShortcutUrl"

【蓝揍网盘-如果里面没有当前版本包就是忘记上传了-无限次下载机会】
https://wwf.lanzoul.com/s/lianbao";
echo "";
