# zhibao_flutter

A new Flutter project.

# 国际化
@q1 http://3.25.77.194:9999/  接口地址,你先用着
文档地址 http://3.25.77.194:9999/swagger-ui/
header头: Accept-Language
zh-CN   en-US  vi-VN  th-TH

# 新需求文档【webview-话费】
api文档：https://www.showdoc.com.cn/2309474775904780
文档密码：654321

http://qbb.jmall.club/agent.php/Login/login.html
18221344567
123456

# 新需求文档【广告】【Mar 25 2024】
https://document.wxcjgg.cn    对接文档和sdk
https://www.yuque.com/r/notes/share/f8b18347-2014-41b2-bb29-4a0b7df643ad   测试id集成链接
https://www.pgyer.com/QAos     广告位展现形式demo的下载链接

# 蒲公英账号
账号100321999@qq.com
密码Nizhenda888

# 正式广告id
联宝，广告位正式id——
appkey：c8711d2bf65d8408
开屏：efcaf370aa056256
信息流：29fbc2a919e05d5e
插屏：a1f2c152a48d078e
辛苦注意查收~
发版前替换成正式id，避免上线后没有收益~

IDO168：
等待添加，如果需要添加广告则重新申请。


# 测试信息
账号13244766725
123456

测试邀请码：ycSD0n
国际版邀请码：YVQpnu

# Relate links.

Api doc:
http://api.ab360.pancakeman.xyz/swagger-ui/

Major doc:
https://u6jica9n9g.feishu.cn/docx/WUJcdry8lox2llxtPYjceteJnig

Schedule management - copy:
https://u6jica9n9g.feishu.cn/sheets/CWXks8pzqhgUl7topEgcQaepnge

Schedule management:
https://jf0ntdzk8d.feishu.cn/sheets/BQa8sDy58h8mKAteCRgcc0bhnhh?from=from_copylink
https://jf0ntdzk8d.feishu.cn/sheets/BQa8sDy58h8mKAteCRgcc0bhnhh?table=tblDOsLesE7dV1rH&view=vew1Kd7wVv

Extra work content:
https://u6jica9n9g.feishu.cn/docx/FUjcdyIbeol3TKxklt9cibNsnrb?from=from_copylink

designer:
https://lanhuapp.com/web/#/item/project/stage?tid=f7f7a880-dda4-4940-acac-7445baf65953&pid=9a2d4ae0-0778-4f58-abef-673485b31516

New doc:
"http://qbb.jmall.club/agent.php/Login/login.html
18221344567
123456

# 话费对接： 【Mar 9 2024，暂时暂停开发，还在进一步交流】
"优先级高需求：充话费页面，充值产品填充，产品包括：充值数额50，100，200，支
持运营商：电信，移动，电信，充值和运营商两两组合形成一个产品，实际折扣价是浮动的，
全部产品设置成95折，使用0-12小时回调一次
对接api文档：https://www.showdoc.com.cn/2309474775904780
文档密码：654321
充值系统登录信息：http://qbb.jmall.club/agent.php/Login/login.html
18221344567
123456"

# 广告
https://document.wxcjgg.cn 对接文档和sdk
https://www.yuque.com/r/notes/share/f8b18347-2014-41b2-bb29-4a0b7df643ad 测试id集成链接
https://www.pgyer.com/QAos 广告位展现形式demo的下载链接

## Running environment.

```
 q1@q1deMacBook-Pro  ~/Downloads/zhibao_flutter  flutter doctor -v
[!] Flutter (Channel [user-branch], 3.16.3, on macOS 13.0 22A5331f darwin-x64, locale en-CN)
    ! Flutter version 3.16.3 on channel [user-branch] at /opt/fvm/versions/3.16.3
      Currently on an unknown channel. Run `flutter channel` to switch to an official channel.
      If that doesn't fix the issue, reinstall Flutter by following instructions at https://flutter.dev/docs/get-started/install.
    ! Upstream repository unknown source is not a standard remote.
      Set environment variable "FLUTTER_GIT_URL" to unknown source to dismiss this error.
    • Framework revision b0366e0a3f (9 weeks ago), 2023-12-05 19:46:39 -0800
    • Engine revision 54a7145303
    • Dart version 3.2.3
    • DevTools version 2.28.4
    • Pub download mirror https://pub-web.flutter-io.cn
    • Flutter download mirror https://storage.flutter-io.cn
    • If those were intentional, you can disregard the above warnings; however it is recommended to use "git" directly to perform update checks and upgrades.

[✓] Android toolchain - develop for Android devices (Android SDK version 34.0.0)
    • Android SDK at /Users/q1/android-sdk-macosx
    • Platform android-34, build-tools 34.0.0
    • Java binary at: /Applications/Android Studio.app/Contents/jbr/Contents/Home/bin/java
    • Java version OpenJDK Runtime Environment (build 17.0.7+0-17.0.7b1000.6-10550314)
    • All Android licenses accepted.

[✓] Xcode - develop for iOS and macOS (Xcode 14.2)
    • Xcode at /Applications/Xcode.app/Contents/Developer
    • Build 14C18
    • CocoaPods version 1.11.3

[✓] Chrome - develop for the web
    • Chrome at /Applications/Google Chrome.app/Contents/MacOS/Google Chrome

[✓] Android Studio (version 2023.1)
    • Android Studio at /Applications/Android Studio.app/Contents
    • Flutter plugin can be installed from:
      🔨 https://plugins.jetbrains.com/plugin/9212-flutter
    • Dart plugin can be installed from:
      🔨 https://plugins.jetbrains.com/plugin/6351-dart
    • Java version OpenJDK Runtime Environment (build 17.0.7+0-17.0.7b1000.6-10550314)

[✓] VS Code (version 1.85.1)
    • VS Code at /Applications/Visual Studio Code.app/Contents
    • Flutter extension version 3.80.0

[✓] Connected device (3 available)
    • Mi9 Pro 5G (mobile) • 488d005c • android-arm64  • Android 11 (API 30)
    • macOS (desktop)     • macos    • darwin-x64     • macOS 13.0 22A5331f darwin-x64
    • Chrome (web)        • chrome   • web-javascript • Google Chrome 121.0.6167.139

[✓] Network resources
    • All expected network resources are available.

! Doctor found issues in 1 category.
```

# 签名
Your keystore contains 1 entry

Alias name: zhibao
Creation date: Mar 7, 2024
Entry type: PrivateKeyEntry
Certificate chain length: 1
Certificate[1]:
Owner: C=zhibao, ST=zhibao, L=zhibao, O=zhibao, OU=zhibao, CN=zhibao
Issuer: C=zhibao, ST=zhibao, L=zhibao, O=zhibao, OU=zhibao, CN=zhibao
Serial number: 1
Valid from: Thu Mar 07 00:40:59 CST 2024 until: Mon Mar 01 00:40:59 CST 2049
Certificate fingerprints:
SHA1: 5A:43:78:9D:30:0F:17:47:BA:B6:06:0B:82:C9:50:24:67:42:F5:95
SHA256: 91:19:6B:B0:74:ED:5E:C0:62:E7:48:68:D5:D6:F5:68:BD:ED:4E:2E:CE:E0:7E:3E:4F:D7:56:D7:11:28:4B:D9
Signature algorithm name: SHA256withRSA
Subject Public Key Algorithm: 2048-bit RSA key
Version: 1

# 完整签名信息：
APK signature verification result:
Signature verification succeeded
Valid APK signature v1 found
Signer CERT.RSA (META-INF/CERT.SF)
Type: X.509
Version: 1
Serial number: 0x1
Subject: C=zhibao, ST=zhibao, L=zhibao, O=zhibao, OU=zhibao, CN=zhibao
Valid from: Thu Mar 07 00:40:59 CST 2024
Valid until: Mon Mar 01 00:40:59 CST 2049
Public key type: RSA
Exponent: 65537
Modulus size (bits): 2048
Modulus: 22320952387702569961143245284418187406238567247409385028699664622395848710775882871334666550884732316980008275569654521823613798529607461065655508999371154040850256983900750149729373143803481767315512432305388114305585661966988229079415047220237066371979042753647030236819983757587317517016114080064026259137704193784984674403976244457309816934081523063376343781296890753032993488340013845193364989331583509837097279069713989872534001922757759572068812558073705956051444778376403647481621002980314313043395879694450142477078647041716484302433536556553934489105904449729362749017437637576182021120974549689577090830063
Signature type: SHA256withRSA
Signature OID: 1.2.840.113549.1.1.11
MD5 Fingerprint: 92 2B 41 AA D6 97 95 BE A6 FA 7C 01 1C 0A 7D 94
SHA-1 Fingerprint: 5A 43 78 9D 30 0F 17 47 BA B6 06 0B 82 C9 50 24 67 42 F5 95
SHA-256 Fingerprint: 91 19 6B B0 74 ED 5E C0 62 E7 48 68 D5 D6 F5 68 BD ED 4E 2E CE E0 7E 3E 4F D7 56 D7 11 28 4B D9
Valid APK signature v2 found
Signer 1
Type: X.509
Version: 1
Serial number: 0x1
Subject: C=zhibao, ST=zhibao, L=zhibao, O=zhibao, OU=zhibao, CN=zhibao
Valid from: Thu Mar 07 00:40:59 CST 2024
Valid until: Mon Mar 01 00:40:59 CST 2049
Public key type: RSA
Exponent: 65537
Modulus size (bits): 2048
Modulus: 22320952387702569961143245284418187406238567247409385028699664622395848710775882871334666550884732316980008275569654521823613798529607461065655508999371154040850256983900750149729373143803481767315512432305388114305585661966988229079415047220237066371979042753647030236819983757587317517016114080064026259137704193784984674403976244457309816934081523063376343781296890753032993488340013845193364989331583509837097279069713989872534001922757759572068812558073705956051444778376403647481621002980314313043395879694450142477078647041716484302433536556553934489105904449729362749017437637576182021120974549689577090830063
Signature type: SHA256withRSA
Signature OID: 1.2.840.113549.1.1.11
MD5 Fingerprint: 92 2B 41 AA D6 97 95 BE A6 FA 7C 01 1C 0A 7D 94
SHA-1 Fingerprint: 5A 43 78 9D 30 0F 17 47 BA B6 06 0B 82 C9 50 24 67 42 F5 95
SHA-256 Fingerprint: 91 19 6B B0 74 ED 5E C0 62 E7 48 68 D5 D6 F5 68 BD ED 4E 2E CE E0 7E 3E 4F D7 56 D7 11 28 4B D9 